from data import *
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import time
import datetime
import os


class analysis():
    def __init__(self):
        self.data = data()
        self.sales = self.data.get_daily_sales()
        self.monthly_sales = self.data.get_monthly_sales()
        #self.clearErrorLog()

    def daily_sales_regression(self):
        #https: // docs.scipy.org / doc / scipy - 0.14.0 / reference / generated / scipy.stats.linregress.html
        x = []
        y = []
        for sale in self.sales:
            # the x value is unix time and so i subtract the value of the ealiest record so that the earliest record is 0 so the intercept can be at this time rather than 1970
            x.append(int(int(self.sales[sale]["timestamp"]) - 1499800439))
            y.append(float(self.sales[sale]["day_total"]))
        print(x)
        print(y)
        print("\n")
        slope, intercept, r_value, p_value, std_err = stats.linregress(x, y)
        print("r-squared:", r_value**2)
        print("intercept:", intercept)
        print("slope:", slope)
        print("r value:", r_value)
        print("p value:", p_value)
        print("Standard Error:", std_err)


    def plotGraph(self, x, y, x_label, y_label, graph_title, file_name, regression=0, rotate_x_label=1):
        count = 0
        min_x = max_x = min_y = max_y = 0
        length = len(x)
        while count < length:
            if count == 0 or min_x > x[count]:
                min_x = x[count]
            if count == 0 or max_x < x[count]:
                max_x = x[count]
            if count == 0 or min_y > y[count]:
                min_y = y[count]
            if count == 0 or max_y < y[count]:
                max_y = y[count]
            count += 1

        fig = plt.figure(figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k')

        if regression == 1:
            fit = np.polyfit(x, y, 1)
            fit_fn = np.poly1d(fit)
            plt.plot(x, y, 'ro', x, fit_fn(x), '--k')
        else:
            plt.plot(x, y, 'go')

        plt.xlim(min_x, max_x)
        plt.ylim(0, max_y)
        if rotate_x_label == 1:
            plt.xticks(rotation=90)
        plt.ylabel(y_label, fontsize=16)
        plt.xlabel(x_label, fontsize=16)
        plt.title(graph_title, fontsize=18)
        plt.grid(True)

        try:
            savefig('/var/www/html/analysis/figs/' + str(file_name) + '.png')
        except:
            savefig('figs/' + str(file_name) + '.png')
        plt.clf()
        plt.close(fig)

    def plot_line_graph(self, x, y, x_label, y_label, graph_title, file_name, rotate_x_label=1):
        count = 0
        min_x = max_x = min_y = max_y = 0
        length = len(x)
        while count < length:
            if count == 0 or min_x > x[count]:
                min_x = x[count]
            if count == 0 or max_x < x[count]:
                max_x = x[count]
            if count == 0 or min_y > y[count]:
                min_y = y[count]
            if count == 0 or max_y < y[count]:
                max_y = y[count]
            count += 1

        fig = plt.figure(figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k')

        plt.plot(x, y, 'black')

        plt.xlim(min_x, max_x)
        plt.ylim(0, max_y)
        if rotate_x_label == 1:
            plt.xticks(rotation=90)
        plt.ylabel(y_label, fontsize=16)
        plt.xlabel(x_label, fontsize=16)
        plt.title(graph_title, fontsize=18)
        plt.grid(True)

        try:
            savefig('/var/www/html/analysis/figs/' + str(file_name) + '.png')
        except:
            savefig('figs/' + str(file_name) + '.png')

        print("Made last 31 days graph")
        #plt.clf(fig)
        plt.close(fig)

    def graph_specific_months_daily_sales(self, month=1, year=2018):
        x = []
        regression_x = []
        y = []
        regression_y = []
        volume_y = []
        for sale in self.sales:
            if str(month) == str(self.sales[sale]["month"]) and str(year) == str(self.sales[sale]["year"]):
                x.append(self.sales[sale]["date"])
                regression_x.append(int(int(self.sales[sale]["timestamp"]) - 1499800439))
                y.append(float(self.sales[sale]["day_total"]))
                regression_y.append(float(self.sales[sale]["day_total"]))
                volume_y.append(int(self.sales[sale]["orders"]))
        if len(x) > 0:
            # scatter for the daily sales
            graph_title = str(month)+"/"+str(year)+" Daily Sales"
            graph_file = str(year)+"_"+str(month)+"_Daily_Sales"
            self.plotGraph(x, y, "Date", "Sale Value (£)", graph_title, graph_file, 0)

            # regression daily sales
            graph_title2 = graph_title+" Regression"
            graph_file2 = graph_file+"_Regression"
            self.plotGraph(regression_x, regression_y, "Date", "Sale Value (£)", graph_title2, graph_file2, 1)

            # daily sales volume graph
            graph_title3 = graph_title+" Volume"
            graph_file3 = graph_file+"_Volume"
            self.plotGraph(x, volume_y, "Date", "Sale Volume", graph_title3, graph_file3)

    def graph_last_31_days(self):
        x = []
        y = []
        sales_31 = self.data.get_daily_sales_31()
        for day in sales_31:
            x.append(sales_31[day]["date"])
            y.append(float(sales_31[day]["day_total"]))
        if len(x) > 0:
            # scatter for the daily sales
            graph_title = "Last 31 Days Daily Sales"
            graph_file = "last_31_days_sales"
            self.plot_line_graph(x, y, "Date", "Sale Value (£)", graph_title, graph_file)


    def get_specific_years_monthly_sales(self, year):
        x = []
        y = []
        volume = []
        for month in self.monthly_sales:
            if int(year) == int(self.monthly_sales[month]["year"]):
                x.append(int(self.monthly_sales[month]["month"]))
                y.append(float(self.monthly_sales[month]["total"]))
                volume.append(int(self.monthly_sales[month]["orders"]))
        if len(x) > 0:
            print("Made monthly sales graph for: " + str(year))

            # scatter for the monthly sales
            graph_title = str(year)+" Monthly Sales"
            graph_file = str(year)+"_Monthly_Sales"
            self.plotGraph(x, y, "Month", "Sale Value (£)", graph_title, graph_file, 0, 0)

            # scatter for the monthly sales
            graph_title = str(year)+" Monthly Sales Regression"
            graph_file = str(year)+"_Monthly_Sales_Regression"
            self.plotGraph(x, y, "Month", "Sale Value (£)", graph_title, graph_file, 1, 0)

            # scatter for the monthly sales volume
            graph_title = str(year) + " Monthly Sales Volume"
            graph_file = str(year) + "_Monthly_Sales_Volume"
            self.plotGraph(x, volume, "Month", "Sale Volume", graph_title, graph_file, 0, 0)
        else:
            print("No sales for " + year)

    def graph_months_daily_sales(self):
        how_many_months = 60
        year = 2017
        count = 0
        while count < how_many_months:
            month = (count % 12) + 1
            if count > 1 and month == 1:
                year += 1
            self.graph_specific_months_daily_sales(month, year)
            month += 1
            count += 1

    def graph_years_monthly_sales(self):
        how_many_years = 2
        year = 2017
        count = 0
        while count < how_many_years:
            self.get_specific_years_monthly_sales(year)
            year += 1
            count += 1

    def clearErrorLog(self):
        path = "error_log.html"
        file = open(path, "w")
        file.write("")
        file.close()

    def writeErrorLog(self):
        path = "error_log.html"
        file = open(path, "w")
        file.write("fail")
        file.close()
