from api_caller import *

class data(api_caller):
    def get_a_product(self,product):
        call = 'get_product'
        params = {}
        params["id"] = product
        product = self.get_caller(call,params)
        print(product)

    def get_all_parts(self):
        call = 'get_parts'
        parts = self.get_caller(call)
        print(parts)

    def get_daily_sales(self):
        call = 'get_daily_sales'
        sales = self.get_caller(call)
        return sales["response"]

    def get_daily_sales_31(self):
        call = 'get_daily_sales_31'
        sales = self.get_caller(call)
        return sales["response"]
        
    def get_monthly_sales(self):
        call = 'get_monthly_sales'
        monthly = self.get_caller(call)
        return monthly["response"]
        
    def get_weekly_sales(self):
        call = 'get_weekly_sales'
        parts = self.get_caller(call)
        print(parts)        
        
    def get_sales(self):
        call = 'get_sales'
        sales = self.get_caller(call)
        return sales["response"]

    def customer_sales(self):
        call = 'customer_sales'
        cs = self.get_caller(call)
        return cs["response"]

    def part_sales(self):
        call = 'part_sales'
        cs = self.get_caller(call)
        return cs["response"]

    def order_part(self):
        call = 'order_part'
        cs = self.get_caller(call)
        return cs["response"]

    def timed_sales(self):
        call = 'timed_sales'
        cs = self.get_caller(call)
        return cs["response"]

