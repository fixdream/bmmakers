from kmeans import *

class customer_clustering():

    def __init__(self):
        self.data = data()
        self.timed_sales = self.data.timed_sales()
        self.customer_sales = self.data.customer_sales()

        self.km = kmeans()

    def kmean_sales_over_time(self):
        x = []
        y = []
        for sale in self.timed_sales:
            x.append(int(self.timed_sales[sale]['date_int']))
            y.append(float(self.timed_sales[sale]['amount_paid']))
        k = 3;
        #x = [1, 5, 1.5, 8, 1, 9]
        #y = [2, 8, 1.8, 8, 0.6, 11]
        self.km.kmeans_flat(x, y, k, 'customer_sales_kmeans')
        print("customer_sales_kmeans made graph")

    def kmean_customer_sales(self):
        x = []
        y = []
        customers = []
        customer_count = 0
        for sale in self.customer_sales:
            x.append(self.customer_sales[sale]['part_id'])
            if not self.customer_sales[sale]['name'] in customers:
                customer_count += 1
                customers.append(str(self.customer_sales[sale]['name']))
            y.append(customers.index(str(self.customer_sales[sale]['name'])))
        k = 2
        print(x)
        print(y)
        print(customers)
        print(customer_count)
        self.km.kmeans_flat(x, y, k, 'kmean_sales_over_time')
        print("kmean_sales_over_time made graph")



cc = customer_clustering();

cc.kmean_customer_sales();
#cc.kmean_sales_over_time();

#print(cc.customer_sales)
#print(cc.timed_sales)





