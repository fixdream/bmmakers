from kmeans import *
from api_caller import *
class part_clustering(api_caller):

    def __init__(self):
        self.data = data()
        self.parts = self.data.order_part()
        self.km = kmeans()

    def kmean_part_sales(self):
        x = []
        y = []
        xname = []
        yname = []
        count = 0
        for part in self.parts:
            if count % 2 == 0:
                x.append(int(self.parts[part]['part_id']))
                xname.append(self.parts[part]['title'])
            else:
                y.append(int(self.parts[part]['part_id']))
                yname.append(self.parts[part]['title'])
            count += 1
        k = 2
        mat = self.km.kmeans_flat(x, y, k, 'kmean_part_sales')
        print("made part sales graph")
        # kmeans_sets = json.dumps(mat)
        # kmeans_sets = kmeans_sets.replace(" ", "")
        # print(kmeans_sets)

        call = 'send_kmeans_multipart_groups'
        params = {}
        params["mat"] = mat
        self.get_caller(call, params)




pc = part_clustering();

pc.kmean_part_sales();

