import urllib
import time
import hashlib
import json
from collections import OrderedDict



class api_caller:
    try:
        settings = json.load(open('settings.json'))
    except:
        settings = json.load(open('/var/www/html/analysis/settings.json'))
    api_settings = settings["api"][0]
    url = api_settings[api_settings["url"]]
    key = api_settings["key"]
    salt = api_settings["salt"]
    account = api_settings["account"]
    
    def get_caller(self,call, params = {}):
        params["account"] = self.account
        params["type"] = call

        time22 = str(time.time())
        params["time"] = time22
        h = time22+self.key+self.salt
        b = h.encode()
        hash_object = hashlib.sha1(b)
        hash22 = hash_object.hexdigest()
        params["hash"] = hash22
        request = json.dumps(params, ensure_ascii=False)
        URL = self.url+request
        URL = URL.replace(" ", "")

        response = urllib.request.urlopen(URL)
        data = response.read()

        try:
            response = json.loads(data.decode('utf-8'), object_pairs_hook=OrderedDict)

            if response["response"] == "none":
                print("There was a problem, Response: ")
            return response
        except:
            print("There was a problem" + str(data))



