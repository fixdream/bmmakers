from data import *
from scipy import stats
import numpy as np
import matplotlib.pyplot as plt
from pylab import *
import time
from sklearn.cluster import *



class kmeans():

    def kmeans_flat(self, x, y, k, name = 0):
        mat = np.column_stack((x, y))
        kmeans = MiniBatchKMeans(n_clusters=k)
        kmeans.fit(mat)
        centroids = kmeans.cluster_centers_
        labels = kmeans.labels_
        colours = ["g.", "r.", "y.", "b."]
        fig = plt.figure(figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k')
        plt.xlim(min(x), max(x))
        plt.ylim(min(y), max(y))
        z = []
        for i in range(len(x)):
            plt.plot(mat[i][0], mat[i][1], colours[labels[i]], markersize=10)
            z.append([int(mat[i][0]), int(mat[i][1]), int(labels[i])])
        plt.scatter(centroids [:, 0], centroids[:, 1], marker="x", s=300, linewidths=5, zorder=10)
        if name != 0:
            try:
                plt.savefig('/var/www/html/analysis/figs/' + str(name) + '.png')
            except:
                plt.savefig('figs/' + str(name) + '.png')
        else:
            plt.show()
        plt.close(fig)
        return z

    def kmeans_1_dm(self, x, k, name = 0):
        kmeans = MiniBatchKMeans(n_clusters=k)
        x = np.asarray(x)
        kmeans.fit(x.reshape(1, -1))
        # centroids = kmeans.cluster_centers_
        labels = kmeans.labels_
        colours = ["b.", "r.", "y.", "g."]
        fig = plt.figure(figsize=(12, 12), dpi=80, facecolor='w', edgecolor='k')
        plt.xlim(min(x), max(x))
        for i in range(len(x)):
            plt.plot(mat[i][0], mat[i][1], colours[labels[i]], markersize=10)
        #plt.scatter(centroids [:, 0], centroids[:, 1], marker="x", s=150, linewidths=5, zorder=10)
        if name != 0:
            try:
                plt.savefig('/var/www/html/analysis/figs/' + str(name) + '.png')
            except:
                plt.savefig('figs/' + str(name) + '.png')
        else:
            plt.show()
        plt.close(fig)