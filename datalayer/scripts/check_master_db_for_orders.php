<?php

if (is_file('/var/www/html/datalayer/classes/MySQL.php')) {
    require('/var/www/html/datalayer/classes/MySQL.php');
} else {
    require('../classes/MySQL.php');
}
$mysql_live = new MySQL(1);
$mysql_local = new MySQL(2);


// local
$sql = "SELECT `id` FROM sales order by `id` DESC LIMIT 1";
$result = mysqli_query($mysql_local->link, $sql);
$rowLocal = mysqli_fetch_all($result, MYSQLI_ASSOC);

//live
$sql = "SELECT `id` FROM sales order by `id` DESC LIMIT 1";
$result = mysqli_query($mysql_live->link, $sql);
$rowLive = mysqli_fetch_all($result, MYSQLI_ASSOC);

if ($rowLocal[0]["id"] != $rowLive[0]["id"]) {

    // get live sales
    $sql = "SELECT * FROM sales WHERE `id` > " . $rowLocal[0]["id"];
    $result = mysqli_query($mysql_live->link, $sql);
    $new_orders_array = mysqli_fetch_all($result, MYSQLI_ASSOC);
    //get live sales quanities etc
    $sql = "SELECT * FROM sales_products_quantities WHERE `sale_id` > " . $rowLocal[0]["id"];
    $result = mysqli_query($mysql_live->link, $sql);
    $new_orders_qs_array = mysqli_fetch_all($result, MYSQLI_ASSOC);

    $mysql_live->close();

    foreach ($new_orders_array AS $new_orders) {
        $sql = "
        INSERT INTO `sales`
        (`id`,
        `sales_platform`,
        `transaction_id`,
        `amount_paid`,
        `currency`,
        `timestamp`,
        `date`,
        `time`,
        `address`,
        `postcode`,
        `phone`,
        `email`, 
        `name`,
        `country`,
        `shop_id`,
        `dispatched`,
        `dispatched_date`,
        `ebay_dispatched`,
        `salesRecordNumber`,
        `resend`,
        `hide`,
        `notes`) 
        VALUES(
        " . $new_orders['id'] . ",
        '" . $new_orders['sales_platform'] . "',
        '" . $new_orders['transaction_id'] . "',
        '" . $new_orders['amount_paid'] . "',
        '" . $new_orders['currency'] . "',
        '" . $new_orders['timestamp'] . "',
        '" . $new_orders['date'] . "',
        '" . $new_orders['time'] . "',
        '" . $new_orders['address'] . "',
        '" . $new_orders['postcode'] . "',
        '" . $new_orders['phone'] . "',
        '" . $new_orders['email'] . "',
        '" . $new_orders['name'] . "',
        '" . $new_orders['country'] . "',
        '" . $new_orders['shop_id'] . "',
        '" . $new_orders['dispatched'] . "',
        '" . $new_orders['dispatched_date'] . "',
        '" . $new_orders['ebay_dispatched'] . "',
        '" . $new_orders['salesRecordNumber'] . "',
        '" . $new_orders['resend'] . "',
        '" . $new_orders['hide'] . "',
        '" . $new_orders['notes'] . "'
        )";

        $result = mysqli_query($mysql_local->link, $sql);
    }

    foreach ($new_orders_qs_array AS $new_orders_qs) {

        $sql = "
        INSERT INTO `sales_products_quantities`
        (
        `id`,
        `sale_id`,
        `sku`,
        `other_identifier`,
        `quantity`,
        `title`,
        `variation_serial`, 
        `sales_products_quantitiescol`,
        `unit_price`
        )VALUES(
        " . $new_orders_qs['id'] . ",
        '" . $new_orders_qs['sale_id'] . "',
        '" . $new_orders_qs['sku'] . "',
        '" . $new_orders_qs['other_identifier'] . "',
        '" . $new_orders_qs['quantity'] . "',
        '" . $new_orders_qs['title'] . "',
        '" . $new_orders_qs['variation_serial'] . "',
        '" . $new_orders_qs['sales_products_quantitiescol'] . "',
        '" . $new_orders_qs['unit_price'] . "'
        )
       ";

        $result = mysqli_query($mysql_local->link, $sql);
    }
} else {
    echo "up to date";
}
//if ($result->num_rows === 0) {
//    echo 'no_results';
//} else {
//
//    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
//    foreach ($rows AS $row) {
//        $remote_image = $row["image_url"];
//        $suffix = explode(".", $remote_image);
//        $new_image = "../product_images/" . $row["id"] . "." . end($suffix);
//        if (!file_exists($new_image)) {
//            file_put_contents($new_image, file_get_contents($remote_image));
//        }
//    }
//}