<?php

if (is_file('/var/www/html/datalayer/classes/MySQL.php')) {
    require('/var/www/html/datalayer/classes/MySQL.php');
} else {
    require('../classes/MySQL.php');
}
$mysql = new MySQL;



$sql = "SELECT * FROM product_group";
$result = mysqli_query($mysql->link, $sql);
if ($result->num_rows === 0) {
    echo 'no_results';
} else {

    $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
    foreach ($rows AS $row) {
        $remote_image = $row["image_url"];
        $suffix = explode(".", $remote_image);
        $new_image = "../product_images/" . $row["id"] . "." . end($suffix);
        if (!file_exists($new_image)) {
            file_put_contents($new_image, file_get_contents($remote_image));
        }
    }
}