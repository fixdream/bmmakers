<?php
ini_set('max_execution_time',3000);

/// this script is for exporting products to websites

// sql for exproting products: "SELECT `id`, `sku`, `price`, `title`, `description`, `group` FROM bmmanager.products;"
// sql to get product group: "SELECT `id`, `Title` as 'title' FROM bmmanager.product_group;"
// sql tor get images: "SELECT `id` AS 'group', `image_url` as 'link' FROM bmmanager.product_group;"

// this bit updates the product vars table

require_once("../classes/MySQL.php");
$mysql2 = new MySQL();
$mysql = $mysql2->link;

function get_products($mysql) {
    $sql = "SELECT * FROM bmmanager.products order by id desc";
    $result = mysqli_query($mysql, $sql);
    if ($result->num_rows === 0) {
        return $this->messages['no_results'];
    } else {
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }
}

function insert_var($product, $product_group, $var, $value, $mysql) {
    $sql = "INSERT INTO `product_variations`
        (`product`, `product_group`, `var_name`, `var_value`) 
        VALUES 
        ('$product', '$product_group', '$var', '$value')";
    echo $sql . "<br>";
    mysqli_query($mysql, $sql);
}

$products = get_products($mysql);
foreach ($products AS $product) {
    print("<br>");


    if ($product["productscol"] != "") {
        insert_var($product["id"], $product["group"], "productscol", $product["productscol"], $mysql);
    }
    if ($product["Thickness"] != "") {
        insert_var($product["id"], $product["group"], "Thickness", $product["Thickness"], $mysql);
    }
    if ($product["Size"] != "") {
        insert_var($product["id"], $product["group"], "Size", $product["Size"], $mysql);
    }
    if ($product["Colour"] != "") {
        insert_var($product["id"], $product["group"], "Colour", $product["Colour"], $mysql);
    }
    if ($product["Pack Size"] != "") {
        insert_var($product["id"], $product["group"], "Pack Size", $product["Pack Size"], $mysql);
    }
    if ($product["Material"] != "") {
        insert_var($product["id"], $product["group"], "Material", $product["Material"], $mysql);
    }
    if ($product["Ply Thickness"] != "") {
        insert_var($product["id"], $product["group"], "Ply Thickness", $product["Ply Thickness"], $mysql);
    }
    if ($product["Size / Shape"] != "") {
        insert_var($product["id"], $product["group"], "Size / Shape", $product["Size / Shape"], $mysql);
    }
    if ($product["with 3pin usb plug"] != "") {
        insert_var($product["id"], $product["group"], "with 3pin usb plug", $product["with 3pin usb plug"], $mysql);
    }
    if ($product["Orientation"] != "") {
        insert_var($product["id"], $product["group"], "Orientation", $product["Orientation"], $mysql);
    }
    if ($product["Width"] != "") {
        insert_var($product["id"], $product["group"], "Width", $product["Width"], $mysql);
    }
    if ($product["Sizes"] != "") {
        insert_var($product["id"], $product["group"], "Sizes", $product["Sizes"], $mysql);
    }
    if ($product["Double or Single Holes"] != "") {
        insert_var($product["id"], $product["group"], "Double or Single Holes", $product["Double or Single Holes"], $mysql);
    }
    if ($product["With Circles"] != "") {
        insert_var($product["id"], $product["group"], "With Circles", $product["With Circles"], $mysql);
    }
    
    print_r($product);
    print("<br>");
}