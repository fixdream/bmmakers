<?php

// unit testing 
$salt = 'jjsks8iu3434hjwherrehjbf98erer983434rwer34rr34';

// function makes GIT calls to the datalayer 
function send($account, $key, $salt, $type, $params = array(), $timeChange = 0) {
    $url = "http://ec2-18-219-129-231.us-east-2.compute.amazonaws.com/datalayer/api/index.php?";
    $request = $url . "request=";
    $params["time"] = (time() + $timeChange);
    $h = $key . $params["time"];
    $params["hash"] = sha1($params["time"] . $key . $salt);
    $params["account"] = $account;
    $params["type"] = $type;
    $request = $request . json_encode($params);
    $response = file_get_contents($request, FILE_USE_INCLUDE_PATH);
    $response = json_decode($response, true);
    return $response;
}

// use this function when you are checking for no error
function noError($response) {
    if ($response["error"] != "none") {
        echo "fail\n";
        print_r($response);
        echo "\n";
    } else {
        echo "pass\n";
    }
}

// use this function for test where you want there to be an error in the response
function yesError($response) {
    if ($response["error"] == "none") {
        echo "fail\n";
        print_r($response);
        echo "\n";
    } else {
        echo "pass\n";
    }
}

echo "\nBegining Datalayer Unit Tests \n\n";

// ############ Interface API Calls to DataLayer ############################

// interface credentials
$account = 'interface';
$key = 'hg23ye2bebd2gve232u334656866b54Oh3hj646h58563h346yu5g46';


echo "Test the average_sale_price function: "; 
$type = "average_sale_price";
$response = send($account, $key, $salt, $type);
noError($response);

echo "Test the get_sales function: ";
$type = "get_sales";
$response = send($account, $key, $salt, $type);
noError($response);

echo "\nTest the get_product funtion: ";
$params = array("id" => 445);
$type = "get_product";
$response = send($account, $key, $salt, $type, $params);
noError($response);

echo "\nTest invalid salt: ";
$type = "get_sales";
$response = send($account, $key, "iuhweiushfuiewh", $type);
yesError($response);

echo "\nTest invalid key: "; 
$type = "get_sales";
$response = send($account, "iaushgfiuaerhgfuehgfuheuigfh", $salt, $type);
yesError($response);

echo "\nTest invalid account: "; 
$type = "get_sales";
$response = send("hssddshjjjjjk", $key, $salt, $type);
yesError($response);

echo "\nTest invalid time: "; 
$type = "get_sales";
$params = array();
$response = send($account, $key, $salt, $type, $params, 800);
yesError($response);

echo "\nTest 5 second valid time: "; 
$type = "get_sales";
$params = array();
$response = send($account, $key, $salt, $type, $params, 5);
noError($response);
