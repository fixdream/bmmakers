CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `bmmanager`@`%` 
    SQL SECURITY DEFINER
VIEW `orders_to_dispatch` AS
    SELECT 
        `sales`.`id` AS `id`,
        `sales`.`sales_platform` AS `sales_platform`,
        `sales`.`transaction_id` AS `transaction_id`,
        `sales`.`amount_paid` AS `amount_paid`,
        `sales`.`currency` AS `currency`,
        `sales`.`date` AS `date`,
        `sales`.`time` AS `time`,
        `sales`.`address` AS `address`,
        `sales`.`postcode` AS `postcode`,
        `sales`.`phone` AS `phone`,
        `sales`.`email` AS `email`,
        `sales`.`name` AS `name`,
        `sales`.`country` AS `country`,
        `sales`.`shop_id` AS `shop_id`,
        `sales`.`dispatched` AS `dispatched`,
        `sales`.`salesRecordNumber` AS `salesRecordNumber`,
        `sales`.`resend` AS `resend`
    FROM
        `sales`
    WHERE
        (`sales`.`dispatched` = 0)