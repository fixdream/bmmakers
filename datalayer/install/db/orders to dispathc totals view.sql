CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `bmmanager`@`%` 
    SQL SECURITY DEFINER
VIEW `orders_to_dispatch_totals` AS
    SELECT 
        COUNT(`sales`.`id`) AS `count`,
        TRUNCATE(SUM(`sales`.`amount_paid`), 2) AS `total_value`
    FROM
        `sales`
    WHERE
        (`sales`.`dispatched` = 0)