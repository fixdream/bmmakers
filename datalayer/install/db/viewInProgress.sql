SELECT m.name AS 'Material Name', m.supplier AS 'Supplier', mp.name AS 'Material Process', mm.name AS 'machine_material'
FROM bmmanager.materials m
LEFT JOIN material_processes mp ON mp.material = m.id
LEFT JOIN material_processes_to_machine_materials m2m ON mp.id = m2m.material_processes_id
LEFT JOIN machine_materials mm ON m2m.machine_material_id = mm.id