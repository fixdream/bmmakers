CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `bmmanager`@`%` 
    SQL SECURITY DEFINER
VIEW `sales_average_value` AS
    SELECT 
        TRUNCATE(AVG(`sales`.`amount_paid`), 2) AS `avg`
    FROM
        `sales`