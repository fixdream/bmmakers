CREATE 
    ALGORITHM = UNDEFINED 
    DEFINER = `bmmanager`@`%` 
    SQL SECURITY DEFINER
VIEW `parts_to_products` AS
    SELECT 
        `p`.`id` AS `product_id`,
        `p`.`title` AS `product_title`,
        `p`.`description` AS `product_description`,
        `p`.`ean` AS `EAN`,
        `p`.`sku` AS `SKU`,
        `p`.`price` AS `price`,
        `p`.`quantity` AS `quantity`,
        `p`.`sold` AS `sold`,
        `p`.`watchers` AS `watchers`,
        `p`.`productscol` AS `productscol`,
        `p`.`Thickness` AS `thickness`,
        `p`.`Size` AS `size`,
        `p`.`Colour` AS `colour`,
        `p`.`Pack Size` AS `pack_size`,
        `p`.`Material` AS `material`,
        `p`.`With/Without Holes` AS `with_holes`,
        `p`.`With Circles` AS `circles`,
        `p`.`Size / Shape` AS `shape`,
        `p`.`with 3pin usb plug` AS `plug`,
        `p`.`Sizes` AS `sizes`,
        `p`.`Double or Single Holes` AS `Double or Single holes`,
        `p`.`group` AS `group`,
        `p`.`ebay_shop` AS `ebay_shop`,
        `pp`.`part_quantity` AS `part_quantity`,
        `pa`.`id` AS `part_id`,
        `pa`.`name` AS `part_name`,
        `pa`.`material thickness` AS `material_thickness`,
        `pa`.`process` AS `process`,
        `pa`.`material_id` AS `material_id`
    FROM
        ((`products` `p`
        LEFT JOIN `product_parts` `pp` ON ((`pp`.`product_id` = `p`.`id`)))
        LEFT JOIN `parts` `pa` ON ((`pa`.`id` = `pp`.`part_id`)))