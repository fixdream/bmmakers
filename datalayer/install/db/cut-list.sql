SELECT p.id AS 'part_id', SUM(spq.quantity)
FROM bmmanager.parts p
LEFT JOIN product_parts pp ON p.id = pp.product_id
LEFT JOIN products pr ON pr.id = pp.part_id
LEFT JOIN sales_products_quantities spq ON spq.sku = pr.sku
LEFT JOIN sales s ON s.id = spq.sale_id
#WHERE s.dispatched != 1
#GROUP BY p.id
