CREATE DATABASE  IF NOT EXISTS `bmmanager` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bmmanager`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 188.121.57.65    Database: bmmanager
-- ------------------------------------------------------
-- Server version	5.5.43-37.2-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `sales_with_products_and_quantities`
--

DROP TABLE IF EXISTS `sales_with_products_and_quantities`;
/*!50001 DROP VIEW IF EXISTS `sales_with_products_and_quantities`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `sales_with_products_and_quantities` AS SELECT 
 1 AS `id`,
 1 AS `sales_platform`,
 1 AS `transaction_id`,
 1 AS `amount_paid`,
 1 AS `currency`,
 1 AS `date`,
 1 AS `time`,
 1 AS `address`,
 1 AS `postcode`,
 1 AS `email`,
 1 AS `name`,
 1 AS `country`,
 1 AS `shop_id`,
 1 AS `dispatched`,
 1 AS `salesRecordNumber`,
 1 AS `sku`,
 1 AS `other_identifier`,
 1 AS `quantity`,
 1 AS `title`,
 1 AS `variation_serial`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `sales_with_products_and_quantities`
--

/*!50001 DROP VIEW IF EXISTS `sales_with_products_and_quantities`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`bmmanager`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `sales_with_products_and_quantities` AS select `s`.`id` AS `id`,`s`.`sales_platform` AS `sales_platform`,`s`.`transaction_id` AS `transaction_id`,`s`.`amount_paid` AS `amount_paid`,`s`.`currency` AS `currency`,`s`.`date` AS `date`,`s`.`time` AS `time`,`s`.`address` AS `address`,`s`.`postcode` AS `postcode`,`s`.`email` AS `email`,`s`.`name` AS `name`,`s`.`country` AS `country`,`s`.`shop_id` AS `shop_id`,`s`.`dispatched` AS `dispatched`,`s`.`salesRecordNumber` AS `salesRecordNumber`,`spq`.`sku` AS `sku`,`spq`.`other_identifier` AS `other_identifier`,`spq`.`quantity` AS `quantity`,`spq`.`title` AS `title`,`spq`.`variation_serial` AS `variation_serial` from (`sales` `s` left join `sales_products_quantities` `spq` on((`s`.`id` = `spq`.`sale_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-07-18  0:36:08
