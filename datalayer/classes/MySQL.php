<?php

class MySQL {

    public $link, $messages;

    function __construct($db_overide = 0) {

        if (is_file('/var/www/html/datalayer/settings.php')) {
            require('/var/www/html/datalayer/settings.php');
        } else {
            require('../settings.php');
        }

        if (is_file("/var/www/html/datalayer/messages/" . $lang . ".php")) {
            require("/var/www/html/datalayer/messages/" . $lang . ".php");
        } else {
            require('../messages/' . $lang . '.php');
        }

        $this->messages = $messages;

        //standard
        if ($db == "live") {
            $this->link = mysqli_connect("bm.cemv2wj4o3gb.eu-west-2.rds.amazonaws.com", "bmmanager", "Thedreamcatcher234!!", "bmmanager");
        } elseif ($db == "dev") {
            $this->link = mysqli_connect("127.0.0.1", "root", "", "bmmanager");
        }

        //over ride
        if ($db_overide != 0) {
            if ($db_overide == 1) {
                $this->link = mysqli_connect("bm.cemv2wj4o3gb.eu-west-2.rds.amazonaws.com", "bmmanager", "Thedreamcatcher234!!", "bmmanager");
            } elseif ($db_overide == 2) {
                $this->link = mysqli_connect("127.0.0.1", "root", "", "bmmanager");
            }
        }

        if (!$this->link->set_charset("utf8")) {
            printf("Error loading character set utf8: %s\n", $this->link->error);
            exit();
        }

        if (!$this->link) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
    }

    public function close() {
        mysqli_close($this->link);
    }

}
