<?php

if (is_file('/var/www/html/datalayer/classes/MySQL.php')) {
    require_once('/var/www/html/datalayer/classes/MySQL.php');
} else {
    require_once('MySQL.php');
}

/*
  Adding a new api function checklist
 * 1. add new case for the getResponse - which is here 
 * 2. add new function in the api functions class
 * 3. add function name to the permissions table in the data base: INSERT INTO `bmmanager`.`api_permissions` (`id`, `function`) VALUES (NULL, 'get_pending_orders_parts_list');
 * 4. add a permission to and account in the accounts to permissions table: INSERT INTO  `bmmanager`.`api_accounts_to_permissions` (`api_account` ,`api_permission`)VALUES ('1',  '4');

 */

class api {

    private $request, $requestType, $account, $hash, $requestID, $apiUserID, $error, $response, $time, $salt;
    public $link, $messages, $thirdparty;

    public function __construct() {

// get connection to the db 
        $sql = new MySQL();
        $this->link = $sql->link;
        $this->messages = $sql->messages;
        $this->salt = 'jjsks8iu3434hjwherrehjbf98erer983434rwer34rr34';
        $this->error = "none";

        if (!($this->get_request())) {
            $this->error = $this->messages['no_request'];
            $this->respond();
        }

// check request type and set
        if (!(isset($this->request->type))) {
            $this->error = $this->messages['no_type'];
            $this->respond();
        }
        $this->requestType = $this->request->type;

// check time
        if (!(isset($this->request->time))) {
            $this->error = $this->messages['no_time'];
            $this->respond();
        }
        $this->time = $this->request->time;

// check account type and set
        if (!(isset($this->request->account))) {
            $this->error = $this->messages['no_account'];
            $this->respond();
        }
        $this->account = $this->request->account;

// check key type and set
        if (!(isset($this->request->hash))) {
            $this->error = $this->messages['no_hash'];
            $this->respond();
        }
        $this->hash = $this->request->hash;

// check the credentials of the user
        if (!($this->authenticate())) {
            $this->respond();
        };

// check that the user has permission for the function they are requesting
        $this->getApiUserID();

// get the users third party status
        $this->getApiThirdparty();


        if (!($this->getRequestID())) {
            $this->error = $this->messages['invalid_function'];
            $this->respond();
        };

        if (!($this->checkPermission())) {
            $this->error = $this->messages['no_permission'];
            $this->respond();
        };

// ###### Respond #########
        $this->getResponse($this->requestType);
        $this->respond();
    }

    public function get_request() {
        if (isset($_POST['request'])) {
            $this->request = $this->clean($_POST["request"]);
            return true;
        } else if (isset($_GET['request'])) {
            $this->request = $this->clean($_GET["request"]);
            return true;
        } else {
            return false;
        }
    }

    public function getRequestID() {
        $sql = "SELECT `id` FROM bmmanager.`api_permissions` WHERE `function` = '" . trim(strtolower($this->requestType)) . "'";
        $result = mysqli_query($this->link, $sql);
        $return = mysqli_fetch_array($result);
        if ($result->num_rows === 0) {
            return false;
        } else {
            $this->requestID = $return[0];
            return true;
        }
    }

    public function getApiUserID() {
        $sql = "SELECT `id` FROM bmmanager.`api_accounts` WHERE `account_name` = '" . $this->account . "'";
        $result = mysqli_query($this->link, $sql);
        $return = mysqli_fetch_array($result);
        $this->apiUserID = $return[0];
    }

    public function getApiThirdparty() {
        $sql = "SELECT `third_party` FROM bmmanager.`api_accounts` WHERE `account_name` = '" . $this->account . "'";
        $result = mysqli_query($this->link, $sql);
        $return = mysqli_fetch_array($result);
        $this->thirdparty = $return[0];
    }

    public function authenticate() {
        $sql = "SELECT * FROM bmmanager.`api_accounts` WHERE `account_name` = '" . $this->account . "'";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            $this->error = $this->messages['no_results'];
            return false;
        } else {
            $result = mysqli_fetch_all($result);
            $key = $result[0][1];
            $dif = abs(time() - $this->time);
            if ($dif > 100) {
                $this->error = $this->messages['out_of_time'];
                return false;
            }
            $h = $key . $this->time;
            $reHash = sha1($this->time . $key . $this->salt);
            if ($reHash != $this->hash) {
                $this->error = $this->messages['hash_incorrect'];
                return false;
            }
            return true;
        }
    }

    public function checkPermission() {
        $sql = "SELECT * FROM bmmanager.`api_accounts_to_permissions` WHERE `api_account` = '" . $this->apiUserID . "' AND `api_permission` = '" . $this->requestID . "'";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return false;
        } else {
            return true;
        }
    }

    public function clean($input) {



        $input = stripslashes($input);

        $out = $input;
        if (json_decode($out)) {
            return json_decode($out);
        } else {
            $this->error = $this->messages['no_json'];
            $this->respond();
        }
    }

    public function respond() {
        $response["error"] = $this->error;
        $response["response"] = $this->response;
        $response = json_encode($response, JSON_FORCE_OBJECT);
        echo $response;
        exit();
    }

    public function getResponse($function) {

        require("apiFunctions.php");
        $functions = new apiFunctions;

        switch ($function) {
            case "create_order":
                $this->response = $functions->create_order($this->request, $this->apiUserID);
                break;
            case "get_non_dispatched":
                $this->response = $functions->get_non_dispatched();
                break;
            case "get_pending_orders_products_list":
                $this->response = $functions->get_pending_orders_products_list();
                break;
            case "average_sale_price":
                $this->response = $functions->average_sale_price();
                break;
            case "create_new_product":
                $this->response = $functions->create_new_product($this->request);
                break;
            case "get_product_to_part_data":
                $this->response = $functions->get_product_to_part_data();
                break;
            case "get_product_to_part_data_where_none":
                $this->response = $functions->get_product_to_part_data_where_none();
                break;
            case "get_sales":
                $this->response = $functions->get_sales();
                break;
            case "get_to_dispatch_with_items":
                $this->response = $functions->get_to_dispatch_with_items();
                break;
            case "get_to_ebay_dispatch_with_items":
                $this->response = $functions->get_to_ebay_dispatch_with_items($this->request);
                break;
            case "get_last_two_months_orders":
                $this->response = $functions->get_last_two_months_orders();
                break;
            case "get_last_two_weeks_orders":
                $this->response = $functions->get_last_two_weeks_orders();
                break;
            case "get_the_last_weeks_orders":
                $this->response = $functions->get_the_last_weeks_orders();
                break;
            case "get_daily_sales":
                $this->response = $functions->get_daily_sales();
                break;
            case "get_daily_sales_31":
                $this->response = $functions->get_daily_sales_31();
                break;
            case "get_weekly_sales":
                $this->response = $functions->get_weekly_sales();
                break;
            case "get_monthly_sales":
                $this->response = $functions->get_monthly_sales();
                break;
            case "get_parts":
                $this->response = $functions->get_parts();
                break;
            case "get_product":
                $this->response = $functions->get_product($this->request);
                break;
            case "get_products":
                $this->response = $functions->get_products($this->thirdparty);
                break;
            case "get_ebay_low_stock":
                $this->response = $functions->get_ebay_low_stock();
                break;
            case "get_parts_for_product":
                $this->response = $functions->get_parts_for_product($this->request);
                break;
            case "create_part":
                $this->response = $functions->create_part($this->request);
                if ($this->response == $this->messages["none"]) {
                    $this->error = $this->messages['create_part_args'];
                }
                break;
            case "add_part_to_product":
                $this->responce = $functions->add_part_to_product($this->request);
                break;
            case "product_to_ebay_link":
                $this->response = $functions->product_to_ebay_link($this->request);
                break;
            case "get_stock_levels":
                $this->response = $functions->get_stock_levels();
                break;
            case "set_stock":
                $this->response = $functions->set_stock($this->request);
                break;
            case "set_stock_with_part_id":
                $this->response = $functions->set_stock_with_part_id($this->request);
                break;
            case "make_stock_record_for_all_parts":
                $this->response = $functions->make_stock_record_for_all_parts();
                break;
            case "get_predictions":
                $this->response = $functions->get_predictions();
                break;
            case "get_cut_list":
                $this->response = $functions->get_cut_list($this->request);
                break;
            case "get_cut_list_with_predictions":
                $this->response = $functions->get_cut_list_with_predictions();
                break;
            case "get_cut_list_with_machine":
                $this->response = $functions->get_cut_list_with_machine($this->request);
                break;
            case "clear_ebay_dispatched":
                $this->response = $functions->clear_ebay_dispatched($this->request);
                break;
            case "zero_stock":
                $this->response = $functions->zero_stock();
                break;
            case "mark_order_as_dispatched":
                $this->response = $functions->mark_order_as_dispatched($this->request);
                break;
            case "mark_order_as_ebay_dispatched":
                $this->response = $functions->mark_order_as_ebay_dispatched($this->request);
                break;
            case "get_order_status":
                $this->response = $functions->get_order_status($this->request);
                break;
            case "get_sale":
                $this->response = $functions->get_sale($this->request);
                break;
            case "sale_search":
                $this->response = $functions->sale_search($this->request);
                break;
            case "edit_sale":
                $this->response = $functions->edit_sale($this->request);
                break;
            case "undispatch":
                $this->response = $functions->undispatch($this->request);
                break;
            case "customer_sales":
                $this->response = $functions->customer_sales();
                break;
            case "part_sales":
                $this->response = $functions->part_sales();
                break;
            case "send_kmeans_multipart_groups":
                $this->response = $functions->send_kmeans_multipart_groups($this->request);
                break;
            case "get_kmeans_multipart_groups":
                $this->response = $functions->get_kmeans_multipart_groups();
                break;
            case "order_part":
                $this->response = $functions->order_part();
                break;
            case "timed_sales":
                $this->response = $functions->timed_sales();
                break;
            case "home_page_data":
                $this->response = $functions->home_page_data();
                break;
            default:
                $this->response = $this->messages["none"];
                $this->error = $this->messages['problem_calling_function'];
                break;
        }
    }

}
