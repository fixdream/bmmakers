<?php

class apiFunctions {

    public $link, $sales_platformmessages;

    public function __construct() {

// get connection to the db 
        $mysql = new MySQL();
        $this->link = $mysql->link;
        $this->messages = $mysql->messages;
    }

    public function create_order($params, $apiUserID) {
        if (!(
                isset($params->transaction_id) &&
                isset($params->amount_paid) &&
                isset($params->currency) &&
                isset($params->order_date) &&
                isset($params->order_time) &&
                isset($params->postcode) &&
                isset($params->phone) &&
                isset($params->email) &&
                isset($params->name) &&
                isset($params->country) &&
                isset($params->salesRecordNumber) &&
                isset($params->products)
                )) {
            return $this->messages["none"];
        }
        $sql = "INSERT INTO bmmanager.sales
                (
                `sales_platform`,
                `transaction_id`,
                `amount_paid`,
                `currency`,
                `date`,
                `time`,
                `address`,
                `postcode`,
                `phone`,
                `email`,
                `name`,
                `country`,
                `shop_id`,
                `salesRecordNumber`
                )VALUES(
                '" . $params->account . "',
                '" . $params->transaction_id . "',
                '" . $params->amount_paid . "',
                '" . $params->currency . "',
                '" . $params->order_date . "',
                '" . $params->order_time . "',
                '" . $params->address . "',
                '" . $params->postcode . "',
                '" . $params->phone . "',
                '" . $params->email . "',
                '" . $params->name . "',
                '" . $params->country . "',
                '" . $apiUserID . "',
                '" . $params->salesRecordNumber . "'
                )
                ";
        $result = mysqli_query($this->link, $sql);
        $order_number = $this->link->insert_id;
        foreach ($params->products AS $product) {
            $sql = "INSERT INTO bmmanager.sales_products_quantities
                    (
                    `sale_id`,
                    `sku`,
                    `quantity`,
                    `title`,
                    `unit_price`
                    )VALUES(
                    '" . $order_number . "',
                    '" . $product->product_id . "',
                    '" . $product->quantity . "',
                    '" . $product->title . "',
                    '" . $product->unit_price . "'
                    )
                    ";
            $result = mysqli_query($this->link, $sql);
        }
        return $order_number;
    }

    public function get_non_dispatched() {
        $sql = "SELECT * FROM bmmanager.orders_to_dispatch";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_order_status($params) {
        if (!(
                isset($params->id)
                )) {
            return $this->messages["none"];
        }
        $sql = "SELECT `dispatched`, `dispatched_date` FROM bmmanager.sales WHERE `id` = " . $params->id;
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function average_sale_price() {
        $sql = "SELECT * FROM bmmanager.sales_average_value";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            $return = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $return[0];
        }
    }

    public function addNewProduct($params) {
        require_once("/var/www/html/classes/newProduct.php");
        $product = new newProduct;
    }

    public function get_pending_orders_products_list() {
        return "hello world get_pending_orders_products_list";
    }

    public function get_product_to_part_data() {
        $sql = "SELECT * FROM bmmanager.parts_to_products";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_product_to_part_data_where_none() {
        $sql = "SELECT * FROM bmmanager.parts_to_products WHERE part_id  IS NULL";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_sales() {
        $sql = "SELECT `id`,`amount_paid`, REPLACE ( `date` , '-' , '' ) AS `date_number`,`date`,`time`,`postcode`,`shop_id`,`resend` 
                FROM bmmanager.sales ORDER BY `id` DESC LIMIT 5000;";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_sale($params) {
        if (!(
                isset($params->id)
                )) {

            return $this->messages["none"];
        }
        $sql = "SELECT * FROM bmmanager.sales_with_products_and_quantities WHERE `id` = " . $params->id;
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function edit_sale($params) {
        if (!(
                isset($params->id) and
                isset($params->notes) and
                isset($params->address)
                )) {

            return $this->messages["none"];
        }
        $sql = "UPDATE bmmanager.sales SET `notes` = '" . $params->notes . "', `address` = '" . $params->address . "' WHERE `id` = " . $params->id;
        if (mysqli_query($this->link, $sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function zero_stock() {
        $sql = "UPDATE bmmanager.stock SET `quantity` = 0";
        if (mysqli_query($this->link, $sql)) {
            return true;
        } else {
            return false;
        }
    }

    public function sale_search($params) {
        if (!(
                isset($params->field) and
                isset($params->search)
                )) {

            return $this->messages["none"];
        }
        $sql = "SELECT * FROM bmmanager.sales_with_products_and_quantities WHERE `" . $params->field . "` LIKE '%" . $params->search . "%'
                ORDER BY `id` DESC";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_to_dispatch_with_items() {
        $sql = "SELECT * FROM bmmanager.sales_with_products_and_quantities WHERE `dispatched` = 0";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_to_ebay_dispatch_with_items($params) {
        if (isset($params->shop)) {
            $sql = "SELECT * FROM bmmanager.sales_with_products_and_quantities WHERE `ebay_dispatched` = 0 and `dispatched` = 1 and `shop_id` = '" . $params->shop . "' ORDER BY `id` DESC";
        } else {
            $sql = "SELECT * FROM bmmanager.sales_with_products_and_quantities WHERE `ebay_dispatched` = 0 and `dispatched` = 1 ORDER BY `id` DESC";
        }
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_last_two_months_orders() {
        $two_months_ago = strtotime("-2 month");
        $sql = "SELECT * FROM bmmanager.sales_with_products_and_quantities WHERE `date` > '" . date('Y-m-d', $two_months_ago) . "' ORDER BY `id` DESC";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_last_two_weeks_orders() {
        $two_weeks_ago = strtotime("-2 week");
        $sql = "SELECT * FROM bmmanager.sales_with_products_and_quantities WHERE `date` > '" . date('Y-m-d', $two_weeks_ago) . "' ORDER BY `id` DESC";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_the_last_weeks_orders() {
        $one_week_ago = strtotime("-1 week");
        $sql = "SELECT * FROM bmmanager.sales_with_products_and_quantities WHERE `date` > '" . date('Y-m-d', $one_week_ago) . "' ORDER BY `id` DESC";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_daily_sales() {
        $sql = "SELECT * FROM bmmanager.daily_sales";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_daily_sales_31() {
        $sql = "SELECT * FROM bmmanager.daily_sales LIMIT 31";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_monthly_sales() {
        $sql = "SELECT * FROM bmmanager.monthly_sales";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_weekly_sales() {
        $sql = "SELECT * FROM bmmanager.weekly_sales";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_parts() {
        $sql = "SELECT * FROM bmmanager.parts ORDER BY id DESC";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_product($params) {
        if (!(
                isset($params->id)
                )) {

            return $this->messages["none"];
        }
        $sql = "SELECT * FROM bmmanager.products WHERE id = " . $params->id;
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            $return = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $return[0];
        }
    }

    public function get_products($thirdparty) {
        if ($thirdparty == 0) {
            $sql = "SELECT * FROM bmmanager.products_with_group";
        } else {
            $sql = "
                SELECT `sku`, `title`, ROUND((`price` * 0.8),2) AS 'price', 
                `price` AS 'full_ebay_price', 
                `image_url`, `ViewItemURL` 
                FROM bmmanager.products_with_group 
                WHERE `ebay_shop` = 1 or `ebay_shop` = 2";
        }

        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            $return = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $return;
        }
    }

    public function get_ebay_low_stock() {
        $sql = "SELECT * FROM bmmanager.ebay_low_stock";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            $return = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $return;
        }
    }

    public function product_to_ebay_link($params) {

        if (!(
                isset($params->id)
                )) {

            return $this->messages["none"];
        }
        $sql = "SELECT * FROM products_with_group where id = " . $params->id;
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            $return = mysqli_fetch_all($result, MYSQLI_ASSOC);
            return $return[0];
        }
    }

    public function get_parts_for_product($params) {
        if (!(
                isset($params->id)
                )) {

            return $this->messages["none"];
        }
        $sql = "SELECT p.`id`, `part_id`, `part_quantity`, `name`, `material`, `material thickness`, `process`, `material_id`, `product_id`  FROM bmmanager.product_parts pp
                LEFT JOIN bmmanager.parts p ON pp.part_id = p.id
                WHERE pp.`product_id` = " . $params->id;
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function create_part($params) {
        if (!(
                isset($params->name) &&
                isset($params->material) &&
                isset($params->material_thickness) &&
                isset($params->process) &&
                isset($params->material_id)
                )) {

            return $this->messages["none"];
        }

        $sql = "INSERT INTO bmmanager.parts
                (`name`,`material`,`material thickness`, `process`, `material_id`)
                VALUES
                (
                '" . $params->name . "',
                '" . $params->material . "',
                '" . $params->material_thickness . "',
                '" . $params->process . "',
                '" . $params->material_id . "'
                )";
        mysqli_query($this->link, $sql);
        $partID = mysqli_insert_id($this->link);

        $sql = "INSERT INTO  `bmmanager`.`stock` (
                `material_id` ,
                `machine_material_id` ,
                `parts_id` ,
                `quantity`
               )
               VALUES (
               '',  '',  " . $partID . ",  '0'
               )";
        if (mysqli_query($this->link, $sql)) {
            return $this->messages['inserted'];
        } else {
            return $this->messages['not_inserted'] . $sql;
        }
    }

    public function add_part_to_product($params) {
        if (!(
                isset($params->existing_part) &&
                isset($params->product) &&
                isset($params->quantity)
                )) {

            return $this->messages["none"];
        }
        $sql = "INSERT INTO  `bmmanager`.`product_parts` (
                `product_id` ,
                `part_id` ,
                `part_quantity`
               )
               VALUES (
               '" . $params->product . "',  '" . $params->existing_part . "',  '" . $params->quantity . "'
               )";
        if (mysqli_query($this->link, $sql)) {
            return $this->messages['inserted'];
        } else {
            return $this->messages['not_inserted'] . $sql;
        }
    }

    public function mark_order_as_dispatched($params) {
        if (!(
                isset($params->sale)
                )) {
            return $this->messages["none"];
        }
        $this->reduce_stock_after_order($params->sale);
        $sql = "UPDATE  `bmmanager`.`sales` SET  `dispatched` =  '1', `dispatched_date` = '" . date("Y/m/d") . "' WHERE  `sales`.`id` = '" . $params->sale . "' LIMIT 1";
        if (mysqli_query($this->link, $sql)) {
            return $this->messages['inserted'];
        } else {
            return $this->messages['not_inserted'] . $sql;
        }
    }

    public function reduce_stock_after_order($order) {
        $sql = "
            SELECT * FROM bmmanager.sales_products_quantities spq
            LEFT JOIN bmmanager.parts_to_products ptp ON spq.sku = ptp.sku
            WHERE `sale_id` = " . $order . "
            GROUP BY spq.sku
                ";
        $result = mysqli_query($this->link, $sql);
        $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
        foreach ($rows AS $part) {
            $sql = "UPDATE `stock` SET `quantity` = (`quantity` - " . $part['quantity'] . ") WHERE `parts` = " . $part['part_id'];
            mysqli_query($this->link, $sql);
        }
        $sql = "UPDATE `stock` SET `quantity` = 0 WHERE `quantity` < 0";
        mysqli_query($this->link, $sql);
    }

    public function mark_order_as_ebay_dispatched($params) {
        if (!(
                isset($params->sale)
                )) {
            return $this->messages["none"];
        }
        $sql = "UPDATE  `bmmanager`.`sales` SET  `ebay_dispatched` =  '1' WHERE  `sales`.`id` = '" . $params->sale . "' LIMIT 1";
        if (mysqli_query($this->link, $sql)) {
            return $this->messages['inserted'];
        } else {
            return $this->messages['not_inserted'] . $sql;
        }
    }

    public function undispatch($params) {
        if (!(
                isset($params->sale)
                )) {
            return $this->messages["none"];
        }
        $sql = "UPDATE  `bmmanager`.`sales` SET  `dispatched` =  '0' WHERE  `sales`.`id` = '" . $params->sale . "' LIMIT 1";
        if (mysqli_query($this->link, $sql)) {
            return $this->messages['inserted'];
        } else {
            return $this->messages['not_inserted'] . $sql;
        }
    }

    public function set_stock($params) {
        if (!(
                isset($params->stock_id) &&
                isset($params->quantity)
                )) {

            return $this->messages["none"];
        }
        $sql = "UPDATE  `bmmanager`.`stock` 
                SET `quantity` = '" . $params->quantity . "'
                WHERE `id` = '" . $params->stock_id . "'";
        if (mysqli_query($this->link, $sql)) {
            return $this->messages['inserted'];
        } else {
            return $this->messages['not_inserted'] . $sql;
        }
    }

    public function set_stock_with_part_id($params) {
        if (!(
                isset($params->part_id) &&
                isset($params->quantity)
                )) {

            return $this->messages["none"];
        }
        $sql = "UPDATE  `bmmanager`.`stock` 
                SET `quantity` = '" . $params->quantity . "'
                WHERE `parts_id` = '" . $params->part_id . "'";
        if (mysqli_query($this->link, $sql)) {
            return $this->messages['inserted'];
        } else {
            return $this->messages['not_inserted'] . $sql;
        }
    }

    public function get_stock_levels() {
        $sql = "SELECT * FROM bmmanager.parts_in_stock";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function make_stock_record_for_all_parts() {
        $parts = $this->get_parts();
        foreach ($parts AS $part) {
            $sql = "SELECT * FROM bmmanager.stock WHERE `parts_id` = " . $part["id"];
            $result = mysqli_query($this->link, $sql);
            if ($result->num_rows === 0) {
                $sql = "INSERT INTO  `bmmanager`.stock(
                        `parts_id`
                        )
                        VALUES (
                        '" . $part["id"] . "'
                        );";
                $result = mysqli_query($this->link, $sql);
            }
        }
        return $this->messages['inserted'];
    }

    public function get_predictions() {
        $sql = "SELECT * FROM bmmanager.prediction_parts;";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_cut_list($params) {
        if (!(
                isset($params->machine)
                )) {
            return $this->messages["none"];
        }
        $sql = "
            SELECT `part_number`, `name`, `machine`, `stock`, `quantity_required_for_orders`, 
            (`required_quantity` - `predicted_quantity`) AS 'required_quantity' 
            FROM bmmanager.parts_cut_list
            WHERE `machine` = '" . $params->machine . "'
            ORDER BY `quantity_required_for_orders` DESC;
            ";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_cut_list_with_predictions() {
        $sql = "SELECT * FROM bmmanager.parts_cut_list;";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function get_cut_list_with_machine($params) {
        if (!(
                isset($params->machine)
                )) {
            return $this->messages["none"];
        }
        $sql = "SELECT * FROM bmmanager.parts_cut_list WHERE `machine` = '" . $params->machine . "'";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function clear_ebay_dispatched($params) {
        if (isset($params->shop)) {
            $sql = "UPDATE bmmanager.sales SET `ebay_dispatched` = `dispatched` WHERE `shop_id` = '" . $params->shop . "'";
        } else {
            $sql = "UPDATE bmmanager.sales SET `ebay_dispatched` = `dispatched`;";
        }
        if (mysqli_query($this->link, $sql)) {
            return $this->messages['ebay_dispatched_cleared'];
        } else {
            return $this->messages['connection_error'];
        }
    }

    public function timed_sales() {
        $sql = "SELECT `amount_paid`, UNIX_TIMESTAMP(`date`) AS 'date_int' , `name` FROM bmmanager.sales WHERE `hide` = 0;";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function customer_sales() {
        $sql = "SELECT * FROM bmmanager.customer_purchases LIMIT 1000";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function part_sales() {
        $sql = "
        SELECT 
            `parts`.`id` AS `id`,
            `parts`.`name` AS `name`,
            `material_id`,
            IFNULL(SUM((`spq`.`quantity` * `pp`.`part_quantity`)),0) AS `Quantity`
        FROM
            ((`product_parts` `pp`
            LEFT JOIN (`products` `p`
            LEFT JOIN (`sales_products_quantities` `spq`
            LEFT JOIN `sales` `s` ON ((`spq`.`sale_id` = `s`.`id`))) ON ((`spq`.`sku` = `p`.`sku`))) ON ((`p`.`id` = `pp`.`product_id`)))
            LEFT JOIN `parts` ON ((`pp`.`part_id` = `parts`.`id`)))
        GROUP BY `parts`.`id`;
        ";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function order_part() {
        $sql = "
            SELECT
            `spq`.`sale_id`,
            `pp`.`part_id`,
            `spq`.`title`
            FROM
            `sales_products_quantities` `spq`
            LEFT JOIN `products` `p` 
            ON `spq`.`sku` = `p`.`sku`
            LEFT JOIN `product_parts` `pp`
            ON `p`.`id` = `pp`.`product_id`
            WHERE 
            `spq`.`sale_id` IN
              (SELECT `sale_id` FROM sales_products_quantities GROUP BY `sale_id` HAVING COUNT(*) > 1)
            AND `spq`.`sale_id` IN
              (SELECT `sale_id` FROM sales_products_quantities GROUP BY `sale_id` HAVING COUNT(*) < 3)
            AND NOT ISNULL(`part_id`);
        ";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function send_kmeans_multipart_groups($params) {
        if (!(
                isset($params->mat)
                )) {
            return $this->messages["none"];
        }
        // truncate table 
        $sql = "TRUNCATE kmeans_multipart_groups;";
        mysqli_query($this->link, $sql);

        foreach ($params->mat AS $row) {
            $sql = "INSERT INTO kmeans_multipart_groups(
                    `part1` ,
                    `part2` ,
                    `group`
                    )
                    VALUES (
                    '" . $row[0] . "',  '" . $row[1] . "',  '" . $row[2] . "'
                    );";
            mysqli_query($this->link, $sql);
        }

        return 1;
    }

    public function get_kmeans_multipart_groups() {
        $sql = "
        SELECT `kmg`.`part1`, `kmg`.`part2`, `kmg`.`group`, `p1`.`name` AS 'part1_name',  `p2`.`name` AS 'part2_name'
        FROM bmmanager.kmeans_multipart_groups `kmg`
        LEFT JOIN `parts` `p1` ON `kmg`.`part1` = `p1`.`id`
        LEFT JOIN `parts` `p2` ON `kmg`.`part2` = `p2`.`id`;
        ";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return $this->messages['no_results'];
        } else {
            return mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
    }

    public function home_page_data() {

        $out = array();
        $out["avg_sale_price"] = $this->average_sale_price();

        //to dispatch totals
        $sql = "SELECT * FROM bmmanager.orders_to_dispatch_totals;";
        $result = mysqli_query($this->link, $sql);
        $to_dispatch = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $out["to_dispatch"] = $to_dispatch[0];

        //total sales
        $sql = "SELECT FORMAT(SUM(`amount_paid`),2) AS 'sales_total', FORMAT(COUNT(*), 0) AS 'sales_count' FROM `sales` ;";
        $result = mysqli_query($this->link, $sql);
        $sales = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $out["total_sales"] = $sales[0];

        // last 7 days
        $start_point = gmdate("Y-m-d", strtotime('6 days ago'));
        $sql = "SELECT FORMAT(SUM(`amount_paid`),2) AS 'last_7_days_total_value', FORMAT(COUNT(*), 0) AS 'count' FROM `sales` WHERE `date` >= '" . $start_point . "'";
        $result = mysqli_query($this->link, $sql);
        $total = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $out["last_7_days"] = $total[0];

        // day sales
        $sql = "SELECT `date`, FORMAT(`day_total`,2) as 'day_total', `orders` FROM bmmanager.daily_sales ORDER BY `date` DESC LIMIT 31;";
        $result = mysqli_query($this->link, $sql);
        $days = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $out["daily_sales"] = $days;

        // last 31 days
        $start_point = gmdate("Y-m-d", strtotime('30 days ago'));
        $sql = "SELECT FORMAT(SUM(`amount_paid`),2) AS 'last_31_days_total_value', FORMAT(COUNT(*), 0) AS 'count' FROM `sales` WHERE `date` >= '" . $start_point . "'";
        $result = mysqli_query($this->link, $sql);
        $total = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $out["last_31_days"] = $total[0];

        // last 365 days
        $start_point = gmdate("Y-m-d", strtotime('364 days ago'));
        $sql = "SELECT FORMAT(SUM(`amount_paid`),2) AS 'last_365_days_total_value', FORMAT(COUNT(*), 0) AS 'count' FROM `sales` WHERE `date` >= '" . $start_point . "'";
        $result = mysqli_query($this->link, $sql);
        $total = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $out["last_365_days"] = $total[0];

        // Average day
        $sql = "SELECT FORMAT(AVG(`day_total`),2) AS 'avg' FROM bmmanager.daily_sales;";
        $result = mysqli_query($this->link, $sql);
        $total = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $out["avaerage_day"] = $total[0];

        // Average week
        $sql = "SELECT FORMAT(AVG(`total`),2) AS 'avg' FROM bmmanager.weekly_sales;";
        $result = mysqli_query($this->link, $sql);
        $total = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $out["avaerage_week"] = $total[0];

        // Average month
        $sql = "SELECT FORMAT(AVG(`total`),2) AS 'avg' FROM bmmanager.monthly_sales;";
        $result = mysqli_query($this->link, $sql);
        $total = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $out["avaerage_month"] = $total[0];

        return $out;
    }

}
