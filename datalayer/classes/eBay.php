<?php

ini_set('max_execution_time', 300);
error_reporting(E_ALL);

if (is_file('/var/www/html/datalayer/classes/eBaySession.php')) {
    require('/var/www/html/datalayer/classes/eBaySession.php');
} else {
    require('eBaySession.php');
}

class eBay {

    public $compatabilityLevel, $devID, $appID, $serverUrl, $userToken, $userToken2, $userToken3, $siteID, $verb, $certID,
            $XMLorders1, $XMLorders2, $XMLorders3, $orders, $orders1, $orders2, $orders3, $test, $link;

    function __construct() {

        if (is_file('/var/www/html/datalayer/classes/MySQL.php')) {
            require('/var/www/html/datalayer/classes/MySQL.php');
        } else {
            require('MySQL.php');
        }

        $mysql = new mysql;
        $this->link = $mysql->link;

        if (is_file('/var/www/html/datalayer/ebay_settings.php')) {
            require('/var/www/html/datalayer/ebay_settings.php');
        } else {
            require('../ebay_settings.php');
        }

        $this->devID = $devID;
        $this->appID = $appID;
        $this->certID = $certID;
        $this->siteNumber = $siteNumber;
        $this->userToken = $userToken;
        $this->userToken2 = $userToken2;
        $this->userToken3 = $userToken3;
        $this->serverUrl = $serverUrl;
        $this->siteID = $siteID;
        $this->compatabilityLevel = $compatabilityLevel;
        $this->XMLorders1 = $this->getOrders(1);
        $this->orders1 = $this->orders($this->XMLorders1);
        $this->orders = $this->orders1;
        if ($siteNumber > 1) {
            $this->XMLorders2 = $this->getOrders(2);
            $orders = $this->orders($this->XMLorders2);
            $this->orders2 = $orders[2];
            if ($this->orders2 != null) {
                $this->orders[2] = $this->orders2;
                $this->orders['site_2_count'] = $orders['site_2_count'];
                $this->orders['site_2_total'] = $orders['site_2_total'];
            }
        }
    }

    public function getOrders($site = 1) {

//        // db populater 1 month ago to 3 months ago
//        $CreateTimeFrom = gmdate("Y-m-d\TH:i:s", time() - 60 * 60 * 24 * 90); //current time minus 8 days
//        $CreateTimeTo = gmdate("Y-m-d\TH:i:s", time() - 60 * 60 * 24 * 30);
//
//        // db populater 365 days - overload     
//        $CreateTimeFrom = gmdate("Y-m-d\TH:i:s", time() - 60 * 60 * 24 * 365); //current time minus 365 days
//        $CreateTimeTo = gmdate("Y-m-d\TH:i:s");
        //Standard call for the last 5 days
        $CreateTimeFrom = gmdate("Y-m-d\TH:i:s", time() - 60 * 60 * 24 * 4);
        $CreateTimeTo = gmdate("Y-m-d\TH:i:s", time());


        if ($site == 1) {
            $token = $this->userToken;
        } elseif ($site == 2) {
            $token = $this->userToken2;
        }

        if (is_file('/var/www/html/datalayer/classes/eBaySession.php')) {
            require_once('/var/www/html/datalayer/classes/eBaySession.php');
        } else {
            require_once('MySQL.php');
        }

        ///Build the request Xml string
        $requestXmlBody = '<?xml version="1.0" encoding="utf-8" ?>';
        $requestXmlBody .= '<GetOrdersRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
        $requestXmlBody .= '<DetailLevel>ReturnAll</DetailLevel>';
        $requestXmlBody .= '<Shipped>true</Shipped>';
        $requestXmlBody .= '<SortingOrder>Ascending</SortingOrder>';
        $requestXmlBody .= '<IncludeFinalValueFee>true</IncludeFinalValueFee>';
        $requestXmlBody .= "<CreateTimeFrom>" . $CreateTimeFrom . "</CreateTimeFrom><CreateTimeTo>" . $CreateTimeTo . "</CreateTimeTo>";
        $requestXmlBody .= '<OrderRole>Seller</OrderRole><OrderStatus>All</OrderStatus>';
        $requestXmlBody .= "<RequesterCredentials><eBayAuthToken>" . $token . "</eBayAuthToken></RequesterCredentials>";
        $requestXmlBody .= '</GetOrdersRequest>';
        $verb = 'GetOrders';
        $session1 = new eBaySession($this->userToken, $verb);

        //send the request and get response
        $responseXml = $session1->sendHttpRequest($requestXmlBody, $verb);
        if (stristr($responseXml, 'HTTP 404') || $responseXml == '') {
            die('<P>Error sending request');
        }
        //Xml string is parsed and creates a DOM Document object
        $responseDoc = new DomDocument();
        $responseDoc->loadXML($responseXml);


        //get any error nodes
        $errors = $responseDoc->getElementsByTagName('Errors');
        $response["response"] = simplexml_import_dom($responseDoc);
        $response["site"] = $site;

        return $response;
    }

    public function orders($XMLorders) {
        $order = null;
        $transactions = $XMLorders["response"]->OrderArray->Order;
        $site = $XMLorders["site"];
        $orderCount = $XMLorders["response"]->PaginationResult->TotalNumberOfEntries;

        if ($orderCount != 0) {
            $order = array();
            $order[$site] = array();
            $count = 1;
            $order['site_' . $site . '_total'] = 0.00;
            $order['site_' . $site . '_count'] = 0;

            if ($transactions != null) {
                foreach ($transactions AS $transaction) {

                    $date = explode("T", $transaction->PaidTime);
                    if ($date[0] != "") {

                        $order[$site][$count]["amount-paid"] = $transaction->AmountPaid;
                        $order['site_' . $site . '_count'] += 1;
                        if (isset($date[1])) {
                            $time = explode(".", $date[1]);
                        }
                        $order[$site][$count]["date"] = $date[0];
                        $order[$site][$count]["time"] = $time[0];
                        $order[$site][$count]["transactions"] = $transaction->TransactionArray;
                        $order[$site][$count]["email"] = $transaction->TransactionArray->Buyer->Email;
                        $order[$site][$count]["shipping_service"] = $transaction->ShippingServiceSelected->ShippingService;
                        $order[$site][$count]["name"] = $transaction->ShippingAddress->Name;
                        $order[$site][$count]["amount_paid"] = $transaction->AmountPaid;
                        $order['site_' . $site . '_total'] += floatval($transaction->AmountPaid);

                        // get the buyer's shipping address 
                        $shippingAddress = $transaction->ShippingAddress;
                        $address = "" . strtoupper($shippingAddress->Name) . "<br>";
                        if ($shippingAddress->Street1 != null) {
                            $address .= strtoupper($shippingAddress->Street1) . "<br>";
                        }
                        if ($shippingAddress->Street2 != null) {
                            $address .= strtoupper($shippingAddress->Street2) . "<br>";
                        }
                        if ($shippingAddress->CityName != null) {
                            $address .=
                                    strtoupper($shippingAddress->CityName) . "<br>";
                        }
                        if ($shippingAddress->StateOrProvince != null) {
                            $address .=
                                    strtoupper($shippingAddress->StateOrProvince) . "<br>";
                        }
                        if ($shippingAddress->PostalCode != null) {
                            $address .=
                                    strtoupper($shippingAddress->PostalCode) . "";
                        }
                        $order[$site][$count]["postcode"] = strtoupper($shippingAddress->PostalCode);
                        $order[$site][$count]["address"] = $address;
                        $order[$site][$count]["phone"] = $shippingAddress->Phone;
                        $order[$site][$count]["dispatched"] = isset($transaction->ShippedTime);
                        $order[$site][$count]["shop"] = $site;
                    }
                    $count++;
                }//end loop
            }//end if
        }
        return $order;
    }

    public function checkForNoTransactionID($id) {
        $sql = "SELECT * FROM bmmanager.`sales` WHERE `transaction_id` = '" . $id . "'";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return true;
        } else {
            return false;
        }
    }

    public function checkNotDispatched($id) {
        $sql = "SELECT * FROM bmmanager.sales WHERE `transaction_id` = '" . $id . "' AND `dispatched` = 1";
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function setAsDispatched($id) {
        $sql = "UPDATE bmmanager.sales  SET `dispatched` = 1 WHERE `transaction_id` = '" . $id . "'";
        $result = mysqli_query($this->link, $sql);
        echo "<br>\n" . $sql;
        return $result;
    }

    public function clean($string) {
        return mysqli_escape_string($this->link, addslashes($string));
    }

    public function insertSql($order) {
        $email = $order["transactions"]->Transaction->Buyer->Email;
        $salesRecordNumber = $order["transactions"]->Transaction->ShippingDetails->SellingManagerSalesRecordNumber;
        $transactionID = $order["transactions"]->Transaction->TransactionID;
        $ebayFee = $order["transactions"]->Transaction->FinalValueFee;
        $siteID = $order["transactions"]->Transaction->TransactionSiteID;
        $salesPlatform = $order["transactions"]->Transaction->Platform;
        $currency = $order["amount_paid"]["currencyID"];
        $date = $order["date"];
        $time = $order["time"];
        $timestamp = $order["date"] . " " . $order["time"];
        $postcode = $order["postcode"];
        $name = $order["name"];
        $phone = $order["phone"];
        $dispatched = $order["dispatched"];
        $address = $order["address"];
        $amounPaid = $order["amount_paid"];
        $shopNumber = $order["shop"];

        $sql = "INSERT INTO bmmanager.`sales` (
            `sales_platform`, 
            `transaction_id`, 
            `amount_paid`, 
            `currency`, 
            `timestamp`, 
            `date`, 
            `time`, 
            `address`, 
            `postcode`, 
            `phone`, 
            `email`, 
            `name`, 
            `country`, 
            `shop_id`, 
            `dispatched`,
            `salesRecordNumber`
            ) 
            VALUES 
            (
            '" . $this->clean($salesPlatform) . "', 
            '" . $this->clean($transactionID) . "', 
            '" . $this->clean($amounPaid) . "', 
            '" . $this->clean($currency) . "', 
            '" . $this->clean($timestamp) . "', 
            '" . $this->clean($date) . "', 
            '" . $this->clean($time) . "', 
            '" . $this->clean($address) . "', 
            '" . $this->clean($postcode) . "', 
            '" . $this->clean($phone) . "', 
            '" . $this->clean($email) . "', 
            '" . $this->clean($name) . "', 
            'UK', 
            '" . $this->clean($shopNumber) . "', 
            '" . $this->clean($dispatched) . "', 
            '" . $this->clean($salesRecordNumber) . "'
            );";
        echo $sql . " " . "<br>transaction id" . $transactionID . "</br>";
        if ($this->checkForNoTransactionID($transactionID)) {
            echo "<br>\nInserted <br/>\n";
            if (!mysqli_query($this->link, $sql)) {
                echo "<br>\nProblem with insertion <br/>\n";
            }
            $saleID = $this->link->insert_id;
            $transactions = $order["transactions"];
            foreach ($transactions->Transaction as $transaction) {//print_r($transaction);
                $sql = "INSERT INTO bmmanager.`sales_products_quantities` (`sale_id`, `sku`, `other_identifier`, `quantity`, `title`, `variation_serial`) VALUES "
                        . "('" . $this->clean($saleID) . "', '" . $this->clean($transaction->Variation->SKU) . "', '" . $this->clean($transaction->Item->ItemID) . "', "
                        . "'" . $this->clean($transaction->QuantityPurchased) . "', '" . $this->clean($transaction->Item->Title) . "' , '" . $this->clean(json_encode($transaction->Variation->VariationSpecifics)) . "');";
                mysqli_query($this->link, $sql);
            }
        } else {
            echo "Already in db ";
            //echo "exists in db ".$transactionID."<br>";
            //already in the db - check if not dispatched and it is now dispatched
            if ($dispatched == 1) {
                //echo "order is dispatched ".$transactionID."<br>";
                if ($this->checkNotDispatched($transactionID)) {
                    $this->setAsDispatched($transactionID);
                    //echo "not dispatched and should be ".$transactionID."<br>";
                } else {
                    echo "<br>\nin db and dispatched in db<br>\n";
                }
            } else {
                echo "<br>\nin db dispatched<br>\n";
            }
        }
    }

    public function getListings($site = 1) {
        if ($site == 1) {
            $token = $this->userToken;
        } elseif ($site == 2) {
            $token = $this->userToken2;
        }
        require_once('eBaySession.php');
        ///Build the request Xml string

        $requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>
            <GetMyeBaySellingRequest xmlns="urn:ebay:apis:eBLBaseComponents">
                <RequesterCredentials><eBayAuthToken>' . $token . '</eBayAuthToken></RequesterCredentials>
              <Version>967</Version>
              <ActiveList>
                <Sort>TimeLeft</Sort>
                <Pagination>
                  <EntriesPerPage>200</EntriesPerPage>
                  <PageNumber>1</PageNumber>
                </Pagination>
              </ActiveList>
            </GetMyeBaySellingRequest>';

        $verb = 'GetMyeBaySelling ';
        $session1 = new eBaySession($this->userToken, $verb);

        //send the request and get response
        $responseXml = $session1->sendHttpRequest($requestXmlBody, $verb);
        if (stristr($responseXml, 'HTTP 404') || $responseXml == '') {
            die('<P>Error sending request');
        }
        //Xml string is parsed and creates a DOM Document object
        $responseDoc = new DomDocument();
        $responseDoc->loadXML($responseXml);


        //get any error nodes
        $errors = $responseDoc->getElementsByTagName('Errors');
        $response["response"] = simplexml_import_dom($responseDoc);
        $response["site"] = $site;

        return $response;
    }

    public function downloadListings($site = 1) {
          echo "\n<br>begining...";
          // get response
          $response = $this->getListings($site);
          // loop through product groups
          foreach ($response["response"]->ActiveList->ItemArray->Item AS $product) {

          // insert product group if not already in table
          $group = $this->check_for_group($product->ItemID);
          if ($group == false) {
          $sql = "INSERT INTO  `bmmanager`.`product_group` (
          `ebay_id` ,
          `StartTime` ,
          `ViewItemURL` ,
          `ViewItemURLForNaturalSearch` ,
          `ListingDuration` ,
          `ListingType` ,
          `Quantity` ,
          `Title` ,
          `WatchCount` ,
          `QuantityAvailable` ,
          `ebay_shop` ,
          `NewLeadCount`
          )
          VALUES (
          '" . $product->ItemID . "',
          '" . $product->ListingDetails->StartTime . "',
          '" . $product->ListingDetails->ViewItemURL . "',
          '" . $product->ListingDetails->ViewItemURLForNaturalSearch . "',
          '" . $product->ListingDuration . "',
          '" . $product->ListingType . "',
          '" . $product->Quantity . "',
          '" . $product->Title . "',
          '" . $product->WatchCount . "',
          '" . $product->QuantityAvailable . "',
          '" . $site . "',
          '" . $product->NewLeadCount . "'
          );";
          mysqli_query($this->link, $sql);
          $group = $this->link->insert_id;
          } else {
          // deleting the products so they can be updated
          $this->delete_products_in_group($group);
          }

          $thickness = $holes = $size = $colour = $packsize = $material = $plug = $orientation = $width = $sizes = $double = $circles = $shape = "";
          $sql = "";

          // check for variations
          if (isset($product->Variations->Variation)) {
          foreach ($product->Variations->Variation AS $vari) {
          if (isset($vari->VariationSpecifics->NameValueList)) {
          // loop through variations and save specifics where they are
          foreach ($vari->VariationSpecifics->NameValueList as $val) {
          switch ($val->Name) {
          case "Thickness":
          $thickness = $val->Value;
          break;
          case "Holes/No Holes":
          $holes = $val->Value;
          break;
          case "Size":
          $size = $val->Value;
          break;
          case "Colour":
          $colour = $val->Value;
          break;
          case "Pack Size":
          $packsize = $val->Value;
          break;
          case "Material":
          $material = $val->Value;
          break;
          case "With/Without Holes":
          $holes = $val->Value;
          break;
          case "Ply Thickness":
          $thickness = $val->Value;
          break;
          case "With Circles":
          $circles = $val->Value;
          break;
          case "Size / Shape":
          $shape = $val->Value;
          break;
          case "with 3pin usb plug":
          $plug = $val->Value;
          break;
          case "Orientation":
          $orientation = $val->Value;
          break;
          case "Width":
          $width = $val->Value;
          break;
          case "Sizes":
          $sizes = $val->Value;
          break;
          case "Double or Single Holes":
          $double = $val->Value;
          break;
          }
          }

          $sql = "INSERT INTO  `bmmanager`.`products` (
          `title` ,
          `group` ,
          `sku` ,
          `price` ,
          `quantity` ,
          `sold` ,
          `watchers` ,
          `Thickness` ,
          `Size` ,
          `Colour` ,
          `Pack Size` ,
          `Material` ,
          `With/Without Holes` ,
          `Ply Thickness` ,
          `With Circles` ,
          `Size / Shape` ,
          `with 3pin usb plug` ,
          `Orientation` ,
          `Width` ,
          `Sizes` ,
          `ebay_shop` ,
          `Double or Single Holes`
          )
          VALUES (
          '" . $vari->VariationTitle . "',
          '" . $group . "',
          '" . $vari->SKU . "',
          '" . $vari->StartPrice . "',
          '" . $vari->Quantity . "',
          '" . $vari->Sold . "',
          '" . $vari->WatchCount . "',
          '" . $thickness . "',
          '" . $size . "',
          '" . $colour . "',
          '" . $packsize . "',
          '" . $material . "',
          '" . $holes . "',
          '" . $thickness . "',
          '" . $circles . "',
          '" . $shape . "',
          '" . $plug . "',
          '" . $orientation . "',
          '" . $width . "',
          '" . $sizes . "',
          '" . $site . "',
          '" . $double . "'
          );";
          mysqli_query($this->link, $sql);
          }
          }
          }
          // insert Product if no variation
          if ($sql == "") {
          $sql = "INSERT INTO  `bmmanager`.`products` (
          `title` ,
          `group` ,
          `sku` ,
          `price` ,
          `quantity` ,
          `sold` ,
          `watchers` ,
          `Thickness` ,
          `Size` ,
          `Colour` ,
          `Pack Size` ,
          `Material` ,
          `With/Without Holes` ,
          `Ply Thickness` ,
          `With Circles` ,
          `Size / Shape` ,
          `with 3pin usb plug` ,
          `Orientation` ,
          `Width` ,
          `Sizes` ,
          `Double or Single Holes`
          )
          VALUES (
          '" . $vari->VariationTitle . "',
          '" . $group . "',
          '" . $vari->SKU . "',
          '" . $vari->StartPrice . "',
          '" . $vari->Quantity . "',
          '" . $vari->Sold . "',
          '" . $vari->WatchCount . "',
          '" . $thickness . "',
          '" . $size . "',
          '" . $colour . "',
          '" . $packsize . "',
          '" . $material . "',
          '" . $holes . "',
          '" . $thickness . "',
          '" . $circles . "',
          '" . $shape . "',
          '" . $plug . "',
          '" . $orientation . "',
          '" . $width . "',
          '" . $sizes . "',
          '" . $double . "'
          );";
          mysqli_query($this->link, $sql);
          }
          }
          echo "\n<br>completed";
          
         
    }

    public function check_for_group($ebay_id) {
        $sql = "SELECT * FROM product_group WHERE ebay_id = " . $ebay_id;
        $result = mysqli_query($this->link, $sql);
        if ($result->num_rows === 0) {
            return false;
        } else {
            $number = mysqli_fetch_array($result);
            return $number[0];
        }
    }

    private function delete_products_in_group($group) {
        $sql = "DELETE FROM products WHERE `group` = " . $group;
        $result = mysqli_query($this->link, $sql);
    }

    public function getProductGroupFromebayID($id) {
        $sql = "SELECT `id` FROM bmmanager.product_group where `ebay_id` =  " . $id;//echo $sql;
        $result = mysqli_query($this->link, $sql);
        $group = mysqli_fetch_array($result);
        return $group[0];
    }

    public function updateProductsFromListings($site = 1) {

        // get response
        $response = $this->getListings($site);//print_r($response);exit();

        // loop through product groups
        foreach ($response["response"]->ActiveList->ItemArray->Item AS $product) {
            $thickness = $holes = $size = $colour = $packsize = $material = $plug = $orientation = $width = $sizes = $double = $circles = $shape = "";
            $sql = "";
            $group = $this->getProductGroupFromebayID($product->ItemID);

            // check for variations
            if (isset($product->Variations->Variation)) {
                foreach ($product->Variations->Variation AS $vari) {
                    if (isset($vari->VariationSpecifics->NameValueList)) {
                        // loop through variations and save specifics where they are
                        foreach ($vari->VariationSpecifics->NameValueList as $val) {
                            switch ($val->Name) {
                                case "Thickness":
                                    $thickness = $val->Value;
                                    break;
                                case "Holes/No Holes":
                                    $holes = $val->Value;
                                    break;
                                case "Size":
                                    $size = $val->Value;
                                    break;
                                case "Colour":
                                    $colour = $val->Value;
                                    break;
                                case "Pack Size":
                                    $packsize = $val->Value;
                                    break;
                                case "Material":
                                    $material = $val->Value;
                                    break;
                                case "With/Without Holes":
                                    $holes = $val->Value;
                                    break;
                                case "Ply Thickness":
                                    $thickness = $val->Value;
                                    break;
                                case "With Circles":
                                    $circles = $val->Value;
                                    break;
                                case "Size / Shape":
                                    $shape = $val->Value;
                                    break;
                                case "with 3pin usb plug":
                                    $plug = $val->Value;
                                    break;
                                case "Orientation":
                                    $orientation = $val->Value;
                                    break;
                                case "Width":
                                    $width = $val->Value;
                                    break;
                                case "Sizes":
                                    $sizes = $val->Value;
                                    break;
                                case "Double or Single Holes":
                                    $double = $val->Value;
                                    break;
                            }
                        }
                  $sql = "
                SELECT `id` FROM bmmanager.products 
                WHERE 	
                `group` = '" . $group . "' AND
                `Thickness` = '" . $thickness . "' AND
                `Size` = '" . $size . "' AND
                `Colour` = '" . $colour . "' AND
                `Pack Size` = '" . $packsize . "' AND 
                `Material` = '" . $material . "' AND
                `With/Without Holes` = '" . $holes . "' AND 
                `Ply Thickness` = '" . $thickness . "' AND
                `With Circles` = '" . $circles . "' AND
                `Size / Shape` = '" . $shape . "' AND
                `with 3pin usb plug` = '" . $plug . "' AND
                `Orientation` = '" . $orientation . "' AND
                `Width` = '" . $width . "' AND
                `Sizes` = '" . $sizes . "' AND
                `Double or Single Holes` = '" . $double . "';
                ";
                $result = mysqli_query($this->link, $sql);
                $id = mysqli_fetch_array($result);

                $sql = "UPDATE bmmanager.products 
                    SET `sku` = '".$vari->SKU."', 
                    `watchers` = '".$vari->WatchCount."', 
                    `price` = '".$vari->StartPrice."', 
                    `sold` = '".$vari->QuantitySold."', 
                    `quantity` = '".$vari->Quantity."' 
                    WHERE `id` = '".$id[0]."'
";
                mysqli_query($this->link, $sql);
                //echo "<br>".$sql;                      
                        
                    }else{echo "wtf why?";}
                }
            }else{
                // update - non varient products
                                
                $sql = "UPDATE bmmanager.products 
                    SET `sku` = '".$vari->SKU."', 
                    `watchers` = '".$vari->WatchCount."', 
                    `price` = '".$vari->StartPrice."', 
                    `sold` = '".$vari->QuantitySold."', 
                    `quantity` = '".$vari->Quantity."' 
                    WHERE `group` = '".$group."'";
                mysqli_query($this->link, $sql);
                //echo "<br>".$sql;
            }
        }
    }

}
