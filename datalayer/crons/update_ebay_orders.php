<?php 
ob_start();
ini_set('display_errors', 1);

if (is_file('/var/www/html/datalayer/classes/eBay.php')) {
  require('/var/www/html/datalayer/classes/eBay.php');
} else {
  require('../classes/eBay.php');
}
$ebay = new eBay;

$response = $ebay->orders;

// Get shop one's orders
if(is_array($response[1]))
{
    foreach($response[1] AS $order){
        $ebay->insertSql($order);
    }
}

// Get shop two's orders
if(is_array($response[2]))
{
    foreach($response[2] AS $order){
        $ebay->insertSql($order);
    }
}