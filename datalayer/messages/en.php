<?php

$messages["no_request"] = "Your request did not send a POST or GET variable which is called 'request'.";
$messages["no_type"] = "Your request did not specify a type.";
$messages["no_account"] = "Your request did not specify your account name.";
$messages["no_hash"] = "Your request did not specify your hash.";
$messages["user_and_hash_no_match"] = "Your hash and/or Account are incorrect.";
$messages["invalid_function"] = "You requested an invalid function.";
$messages["no_permission"] = "You dont have the permissions for this function.";
$messages["no_json"] = "Your request was not valid JSON";
$messages["out_of_time"] = "The time you have given too far away from this servers time, server time is: ".time();
$messages["hash_incorrect"] = "The Hash you have provided is incorrect ";
$messages['inserted'] = "Your input has sucessfully been inserted into the db";
$messages['not_inserted'] = "Your input has not been inserted into the db there is an error with the SQL: <br>";
$messages["create_part_args"] = "You did not provide the required fields: name, material, material_thickness, process, material_id";
$messages["ebay_dispatched_cleared"] = "The 'eBay To Dispatch' has been wiped clean";
$messages["ebay_dispatched_cleared"] = "The 'eBay To Dispatch' has been wiped clean";

$messages["connection_error"] = "There was a problem with the system, the database is not working as expected.";


$messages["problem_calling_function"] = "The function you called does not exist";
$messages["no_results"] = "No results";
$messages["none"] = "none";