<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class order extends db {

    private $currency;
    public $id, $fname, $sname, $user, $order_ref, $paid, $total_amount, $date, $time,
            $timestamp, $payment_chanel, $email, $shipping_address, $billing_address,
            $shipping_value, $products, $phone, $payment_ref;

    public function __construct($order = false) {
        $this->products = array();
        parent::__construct();

        if ($order) {
            $this->load_order($order);
        } elseif (isset($_SESSION['order'])) {
            $this->load_order($_SESSION['order']);
        } else {
            $this->create_order();
            $_SESSION['order'] = $this->id;
            ob_flush();
        }
    }

    public function load_order($order_id) {
        $sql = "SELECT * FROM `orders` WHERE `id` = " . $order_id;
        $result = mysqli_query($this->cn, $sql);
        $order = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $this->id = $order[0]['id'];
        $this->user = $order[0]['user'];
        $this->email = $order[0]['email'];
        $this->phone = $order[0]['phone'];
        $this->fname = $order[0]['fname'];
        $this->sname = $order[0]['sname'];
        $this->order_ref = $order[0]['order_ref'];
        $this->paid = $order[0]['paid'];
        $this->total_amount = $order[0]['total_amount'];
        $this->currency = $order[0]['currency'];
        $this->date = $order[0]['date'];
        $this->time = $order[0]['time'];
        $this->timestamp = $order[0]['timestamp'];
        $this->payment_chanel = $order[0]['payment_chanel'];
        $this->payment_ref = $order[0]['payment_ref'];
        $this->billing_address = $order[0]['billing_address'];
        $this->shipping_address = $order[0]['shipping_address'];

        $sql = "SELECT * FROM `order_products` WHERE `order` = " . mysqli_real_escape_string($this->cn, $order_id);
        $result = mysqli_query($this->cn, $sql);
        $products = mysqli_fetch_all($result, MYSQLI_ASSOC);
        foreach ($products AS $product) {
            $this->products[$product['product']] = $product['quantity'];
        }
    }

    public function save_order($withProducts = true) {
        $sql = "UPDATE `orders` SET 
                `user` = '" . mysqli_real_escape_string($this->cn, $this->user) . "',
                `email` = '" . mysqli_real_escape_string($this->cn, $this->email) . "',
                `phone` = '" . mysqli_real_escape_string($this->cn, $this->phone) . "',
                `fname` = '" . mysqli_real_escape_string($this->cn, $this->fname) . "',
                `sname` = '" . mysqli_real_escape_string($this->cn, $this->sname) . "',
                `order_ref` = '" . mysqli_real_escape_string($this->cn, $this->order_ref) . "',
                `paid` = '" . mysqli_real_escape_string($this->cn, $this->paid) . "',
                `total_amount` = '" . mysqli_real_escape_string($this->cn, $this->total_amount) . "',
                `payment_chanel` = '" . mysqli_real_escape_string($this->cn, $this->payment_chanel) . "',
                `payment_ref` = '" . mysqli_real_escape_string($this->cn, $this->payment_ref) . "',
                `billing_address` = '" . mysqli_real_escape_string($this->cn, $this->billing_address) . "',
                `shipping_address` = '" . mysqli_real_escape_string($this->cn, $this->shipping_address) . "'
                WHERE `id` = '" . mysqli_real_escape_string($this->cn, $this->id) . "'
                ";
        mysqli_query($this->cn, $sql);
        if ($withProducts) {
            $this->remove_order_products();
            $this->add_order_products();
        }
    }

    public function add_cart($cart) {
        $this->products = $cart->productsWithQuanties;
        $this->total_amount = $cart->total_value;
    }

    private function remove_order_products() {
        $sql = "DELETE FROM `order_products` WHERE `order` = " . mysqli_real_escape_string($this->cn, $this->id);
        mysqli_query($this->cn, $sql);
    }

    private function add_order_products() {
        foreach ($this->products AS $product => $quantity) {
            $pro = new product($product);
            $sql = "INSERT INTO `order_products` 
                    (
                    `order`,
                    `product`,
                    `sku`,
                    `quantity`,
                    `price`
                    )
                    VALUES
                    (
                    '" . mysqli_real_escape_string($this->cn, $this->id) . "',
                    '" . mysqli_real_escape_string($this->cn, $product) . "',
                    '" . mysqli_real_escape_string($this->cn, $pro->sku) . "',
                    '" . mysqli_real_escape_string($this->cn, $quantity) . "',
                    '" . mysqli_real_escape_string($this->cn, $pro->price) . "'
                    )";
            $result = mysqli_query($this->cn, $sql);
        }
    }

    public function create_order() {
        $date = date("Y/m/d");
        $time = date("h:i:sa");
        $timestamp = date('Y-m-d H:i:s');
        $sql = "INSERT INTO `orders` (`date`,`time`, `timestamp`, `currency`)VALUES('" . $date . "','" . $time . "','" . $timestamp . "', 'GBP')";
        $result = mysqli_query($this->cn, $sql);
        $this->id = $this->cn->insert_id;
    }

}
