<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class cart {

    public $productsWithQuanties, $total_products, $total_count, $total_value;

    public function __construct() {
        $this->load_cart();
        $this->cart_counts();
    }

    public function load_cart() {
        if (isset($_SESSION['productsWithQuanties'])) {
            $this->productsWithQuanties = $_SESSION['productsWithQuanties'];
        } else {
            $this->productsWithQuanties = array();
        }
    }

    public function save_cart() {
        $_SESSION['productsWithQuanties'] = $this->productsWithQuanties;
        ob_flush();
    }

    public function empty_cart() {
        $this->productsWithQuanties = array();
        $this->cart_counts();
        $this->save_cart();
    }

    public function in_cart($product_id) {
        if(array_key_exists($product_id,$this->productsWithQuanties)){
            return $this->productsWithQuanties[$product_id];
        }else{
            return 0;
        }
    }

    public function change_product($product_id, $quantity) {
        if ($product_id == "") {
            return false;
        }
        if ($quantity > 0) {
            $this->productsWithQuanties[$product_id] = $quantity;
        } else {
            unset($this->productsWithQuanties[$product_id]);
        }
        $this->cart_counts();
        $this->save_cart();
        return true;
    }

    public function cart_counts() {
        $this->total_products = count($this->productsWithQuanties);
        $this->total_count = 0;
        $this->total_value = 0;
        foreach ($this->productsWithQuanties AS $product => $quantity) {
            $pro = new product($product);
            $this->total_value = $this->total_value + ($pro->price * $quantity);
            $this->total_count = $this->total_count + $quantity;
        }
        $this->total_value = $this->total_value;
    }

}
