<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require("db.php");

class product extends db {

    protected $cn, $db;
    public $id, $sku, $price, $title, $description, $group, $group_title, $group_description, $group_long_description, $category, $sub_category,
            $image1, $image2, $image3, $image4;

    public function __construct($product = 0) {
        parent::__construct();

        if ($product != 0) {
            $this->load_product($product);
        }
    }

    public function load_product($product) {
        $sql = "SELECT * FROM `product_with_group` WHERE `id` = '" . $this->cleanInput($product) . "';";
        $result = mysqli_query($this->cn, $sql);
        $row = mysqli_fetch_all($result, MYSQLI_ASSOC);
        $product = $row[0];
        $this->id = $product['id'];
        $this->sku = $product['sku'];
        $this->price = $product['price'];
        $this->title = $product['title'];
        $this->description = $product['description'];
        $this->group = $product['group'];
        $this->group_title = $product['group_title'];
        $this->group_description = $product['group_description'];
        $this->group_long_description = $product['group_long_description'];
        $this->category = $product['category'];
        $this->sub_category = $product['sub_category'];

        $sql = "SELECT * FROM `product_images` WHERE `sku` = '" . $this->sku . "';";
        $result = mysqli_query($this->cn, $sql);
        $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

        // images
        foreach ($rows AS $row) {
            switch ($row["position"]) {
                case 1:
                    $this->image1 = $row["link"];
                    break;
                case 2:
                    $this->image2 = $row["link"];
                    break;
                case 3:
                    $this->image3 = $row["link"];
                    break;
                case 4:
                    $this->image4 = $row["link"];
                    break;
            }
        }
    }

    public function save_product() {
        // group
        $sql = "UPDATE  `product_group` SET  
                `title` =  '" . $this->group_title . "',
                `short_description` =  '" . $this->group_description . "',
                `long_description` =  '" . $this->group_long_description . "',
                `category` =  '" . $this->category . "',
                `sub_category` =  '" . $this->sub_category . "' 
                 WHERE `id` = " . $this->group . " LIMIT 1 ;";
        $result = mysqli_query($this->cn, $sql);
        
        // product
        $sql = "UPDATE  `products` SET  
                `sku` =  '" . $this->sku . "',
                `price` =  '" . $this->price . "',
                `title` =  '" . $this->title . "',
                `description` =  '" . $this->description . "' 
                WHERE  `id` = " . $this->id . " LIMIT 1;";
        $result = mysqli_query($this->cn, $sql);
        
        // image 1
        $sql = "UPDATE  `product_images` SET  
                `link` =  '" . $this->image1 . "'
                WHERE `sku` = '" . $this->sku . "' AND
                `position` = 1 
                LIMIT 1;
                ";
        $result = mysqli_query($this->cn, $sql);
        
        // image 2
        $sql = "UPDATE  `product_images` SET  
                `link` =  '" . $this->image2 . "'
                WHERE `sku` = '" . $this->sku . "' AND
                `position` = 2 
                LIMIT 1;
                ";
        $result = mysqli_query($this->cn, $sql);   
        
        // image 3
        $sql = "UPDATE  `product_images` SET  
                `link` =  '" . $this->image3 . "'
                WHERE `sku` = '" . $this->sku . "' AND
                `position` = 3 
                LIMIT 1;
                ";
        $result = mysqli_query($this->cn, $sql);
        
        // image 4
        $sql = "UPDATE  `product_images` SET  
                `link` =  '" . $this->image4 . "'
                WHERE `sku` = '" . $this->sku . "' AND
                `position` = 4 
                LIMIT 1;
                ";
        $result = mysqli_query($this->cn, $sql);    
        
    }

    public function create_product($sku, $group = 0) {
        $this->sku = $sku;

        if ($group == 0) {
            $sql = "INSERT INTO  `product_group` (
                    `id`
                    )
                    VALUES (
                    NULL
                    );";
            $result = mysqli_query($this->cn, $sql);
            $this->group = mysqli_insert_id($this->cn);
        } else {
            $this->group = $group;
        }
        $sql = "INSERT INTO  `products` (
                `id`,
                `sku`,
                `group`
                )
                VALUES (
                NULL,
                '" . $this->sku . "',
                '" . $this->group . "'
                );";
        echo $sql;
        $result = mysqli_query($this->cn, $sql);
        $this->id = mysqli_insert_id($this->cn);

        $count = 1;
        while ($count <= 4) {
            $sql = "INSERT INTO  `product_images` (
                `group` ,
                `sku` ,
                `link` ,
                `position`
                )
                VALUES (
                '" . $this->group . "',  '" . $this->sku . "',  '', " . $count . "
                );
                ";
            $result = mysqli_query($this->cn, $sql);
            $count++;
        }
    }

}
