<?php
ob_start();
session_start();

require("../classes/product.php");
$product_id = $_GET['id'];
$product = new product($product_id);

require("../classes/marketing.php");
$marketing = new marketing;

require("../classes/cart.php");
$cart = new cart;
if(isset($_POST['add_product_to_cart'])){
    $cart->change_product($_POST['add_product_to_cart'], $_POST['quantity']);
}

require("./views/view_functions.php");
$vf = new view_functions;

echo $vf->get_header("Home - Wooden Ring Binders",$cart, $marketing);


echo $vf->product_page($product,$cart);
//echo $vf->related_products();



echo $vf->get_footer();
 
     