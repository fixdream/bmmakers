<?php

ob_start();
session_start();

require("../classes/product.php");

require("../classes/cart.php");
$cart = new cart;
if($cart->total_count == 0){
    header("Location: /home");
}

require("../classes/marketing.php");
$marketing = new marketing;

if (isset($_POST['update_cart'])) {
    foreach ($_POST AS $product => $quantity) {
        if($product != "update_cart") {
            $cart->change_product($product, $quantity);
        }
    }
}

//session_destroy();
require("./views/view_functions.php");
$vf = new view_functions;

echo $vf->get_header("Basket Page - Wooden Ring Binders", $cart, $marketing);


echo $vf->basket_page($cart);


echo $vf->get_footer();

