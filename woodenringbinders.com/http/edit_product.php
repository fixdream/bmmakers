<?php

ob_start();
session_start();

require("../classes/security.php");
$sec = new security();
$sec->admin_secure(); // MUST BE LEFT SECURE DO NOT REMOVE

require("../classes/marketing.php");
$marketing = new marketing;

require("../classes/product.php");

if (!(isset($_GET['id']))) {
    if (isset($_POST['create'])) {
        $product = new product();
        $product->create_product($_POST['sku'], $_POST['group']);
        $product->save_product();
        header("Location: ./edit_product.php?id=".$product->id);
    } else {
        echo '
        <form action="" method="POST">
        SKU<br>
        <input type="text" value="" name="sku" class="form-control"/><br>
        Group<br>
        <input type="text" value="0" name="group" class="form-control"/><br>
        <input type="hidden" value="1" name="create" class="form-control"/><br>
        <input type="submit" value="create"  class="form-control"/><br>
            </form>
        ';
    }
    exit();
}

$product_id = $_GET['id'];
$product = new product($product_id);

if (isset($_POST['change_pro'])) {
    $product->image1 = $_POST['image1'];
    $product->image2 = $_POST['image2'];
    $product->image3 = $_POST['image3'];
    $product->image4 = $_POST['image4'];
    $product->price = $_POST['price'];
    $product->title = $_POST['title'];
    $product->description = $_POST['description'];
    $product->group = $_POST['group'];
    $product->group_description = $_POST['group_description'];
    $product->group_long_description = $_POST['group_long_description'];
    $product->category = $_POST['category'];
    $product->sub_category = $_POST['sub_category'];
    $product->save_product();
}

require("../classes/cart.php");
$cart = new cart;
if (isset($_POST['add_product_to_cart'])) {
    $cart->change_product($_POST['add_product_to_cart'], $_POST['quantity']);
}

require("./views/view_functions.php");
$vf = new view_functions;

echo $vf->get_header("Home - Wooden Ring Binders", $cart, $marketing);




echo $vf->edit_product($product);





echo $vf->get_footer();

