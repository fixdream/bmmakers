<?php

class view_functions {

    public $header, $banner, $footer;

    public function get_header($title, $cart, $marketing) {
        $header = file_get_contents("./views/header.html");
        $header = str_replace("[[title]]", $title, $header);
        //require('./../classes/product.php');
        //cart
        if ($cart->total_count == 0) {
            $items_incart = "<li>Your&nbsp;Basket&nbsp;Is Empty</li>";
        } else {
            $s = "";
            if ($cart->total_count > 1) {
                $s = "s";
            }
            $items_incart = "<li>" . $cart->total_count . " Item" . $s . " in your basket</li>";
            foreach ($cart->productsWithQuanties AS $product => $quantity) {
                $pro = new product($product);
                $items_incart .= '                
                  <li>
                    <a href="single-product.html">
                      <div class="media">
                        <img class="media-left media-object" src="/img/home/cart-items/cart-item-01.jpg" alt="cart-Image">
                        <div class="media-body">
                          <h5 class="media-heading">' . $pro->title . '<br><span>' . $quantity . ' X &pound;' . $pro->price . '</span></h5>
                        </div>
                      </div>
                    </a>
                  </li>
                  <li>';
            }
            $items_incart .= '
                <div class="btn-group" role="group" aria-label="...">
                    <form action="/basket"><button type="submit" class="btn btn-default" >Edit Basket</button></form>
                    <form action="/checkout"><button type="submit" class="btn btn-default" >Checkout</button></form>
        </div>';
        }

        $header = str_replace("[[items_incart]]", $items_incart, $header);
        $header = str_replace("[[cart_total]]", number_format($cart->total_value, 2), $header);
        $header = str_replace("[[badge]]", $cart->total_count, $header);
        $header = str_replace("[[marketing_message]]", $marketing->message, $header);

        $this->header = $header;
        return $header;
    }

    public function get_banner() {
        $banner = file_get_contents("./views/banner.html");
        $this->banner = $banner;
        return $banner;
    }

    public function get_footer() {
        $footer = file_get_contents("./views/footer.html");
        $footer = str_replace("[[year]]", date("Y"), $footer);
        $this->banner = $footer;
        return $footer;
    }

    public function basket_page($cart) {
        $products = "";
        $count = 0;
        foreach ($cart->productsWithQuanties AS $product => $quantity) {
            $count++;
            $pro = new product($product);
            $products .= '
                        <tr>
                          <td class="">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <span class="cartImage"><img src="/img/products/cart-image1.jpg" alt="image"></span>
                          </td>
                          <td class="">' . $pro->title . '</td>
                          <td class="">&pound;' . $pro->price . '</td>
                          <td class="count-input">
                            <a class="incr-btn" data-action="decrease" href="#"><i class="fa fa-minus"></i></a>
                            <input class="quantity" type="text" name="' . $product . '" value="' . $quantity . '">
                            <a class="incr-btn" data-action="increase" href="#"><i class="fa fa-plus"></i></a>
                          </td>
                          <td class="">&pound;' . number_format(($pro->price * $quantity), 2) . '</td>
                        </tr>';
        }

        $basket = file_get_contents("./views/basket.html");
        $basket = str_replace("[[products]]", $products, $basket);
        $basket = str_replace("[[grand_total]]", number_format($cart->total_value, 2), $basket);
        $basket = str_replace("[[products]]", $products, $basket);
        return $basket;
    }

    public function product_page($product, $cart) {
        $out = file_get_contents("./views/product.html");
        $out = str_replace("[[image1]]", $product->image1, $out);
        $out = str_replace("[[image2]]", $product->image2, $out);
        $out = str_replace("[[image3]]", $product->image3, $out);
        $out = str_replace("[[image4]]", $product->image4, $out);

        $out = str_replace("[[title]]", $product->title, $out);
        $out = str_replace("[[price]]", $product->price, $out);
        $out = str_replace("[[description]]", $product->description, $out);
        $out = str_replace("[[productID]]", $product->id, $out);

        $in_cart = $cart->in_cart($product->id);
        $cart_message = "";
        if ($in_cart > 0) {
            $cart_message = "<p><b>You have " . $in_cart . " of these in the basket</b></p>";
        }
        $out = str_replace("[[in_cart]]", $cart_message, $out);

        $out = str_replace("[[group_description]]", $product->group_description, $out);

        $quantities = '<option value="0">Qty</option>';
        $count = 1;
        $selected = "";
        while ($count <= 10) {
            if ($in_cart == $count) {
                $selected = "selected";
            }
            $quantities .= '<option value="' . $count . '" ' . $selected . '>' . $count . '</option>';
            $selected = "";
            $count++;
        }
        $out = str_replace("[[quantity_options]]", $quantities, $out);

        return $out;
    }

    public function related_products() {
        $out = file_get_contents("./views/related_products.html");
        $out = str_replace("[[delivery]]", number_format(0, 2), $out);

        return $out;
    }

    public function checkout_page($cart, $message) {
        $basket = file_get_contents("./views/checkout_page.html");
        $subtotal = $cart->total_value;
        //$shipping = number_format((3 * $cart->total_count), 2);
        $total = number_format($subtotal, 2);
        $basket = str_replace("[[subtotal]]", number_format($subtotal, 2), $basket);
        //$basket = str_replace("[[shipping]]", $shipping, $basket);
        $basket = str_replace("[[total]]", $total, $basket);
        $basket = str_replace("[[message]]", $message, $basket);
        $basket = str_replace("[[delivery]]", number_format(0, 2), $basket);

        return $basket;
    }

    public function payment_page($cart, $order, $message) {
        $basket = file_get_contents("./views/payment.html");
        $subtotal = number_format($cart->total_value, 2);
        $total = $subtotal;
        $basket = str_replace("[[subtotal]]", $subtotal, $basket);
        $basket = str_replace("[[total]]", $total, $basket);
        $basket = str_replace("[[shipping_address]]", $order->shipping_address, $basket);

        $paypalForm = $this->paypal_form($order);
        $basket = str_replace("[[paypal_form]]", $paypalForm, $basket);
        $basket = str_replace("[[message]]", $message, $basket);
        $basket = str_replace("[[delivery]]", number_format(0, 2), $basket);
        return $basket;
    }

    public function confirm_page($cart, $order) {
        $basket = file_get_contents("./views/confirmation.html");
        $subtotal = number_format($cart->total_value, 2);
        $total = $subtotal;
        $basket = str_replace("[[subtotal]]", $subtotal, $basket);
        $basket = str_replace("[[total]]", $total, $basket);
        $basket = str_replace("[[shipping_address]]", $order->shipping_address, $basket);

        $basket = str_replace("[[email]]", $order->email, $basket);
        $basket = str_replace("[[shipping_address]]", $order->shipping_address, $basket);
        $basket = str_replace("[[temp]]", $order->id, $basket);
        $basket = str_replace("[[delivery]]", number_format(0, 2), $basket);
        return $basket;
    }

    public function paypal_form($order) {
        $out = '<form action="https://www.paypal.com/cgi-bin/webscr" method="post">  ';
        $out .= '
                 <input type="hidden" name="cmd" value="_cart">   <!--required -->
                 <input type="hidden" name="upload" value="1">  <!--required -->
                 <input type="hidden" name="currency_code" value="GBP">
                 <input type="hidden" name="business" value="enquiries@gbmakers.com"> <!--required -->
                 <input type="hidden" name="invoice" value="' . $order->id . '">
                ';
        $count = 1;
        foreach ($order->products AS $product => $quantity) {
            $pro = new product($product);
            $out .= '
            <input type = "hidden" name = "item_name_' . $count . '" value = "' . $pro->title . '"> <!--title -->
            <input type = "hidden" name = "item_number_' . $count . '" value = "' . $pro->sku . '"> <!--title -->
            <input type = "hidden" name = "amount_' . $count . '" value = "' . $pro->price . '">
            <input type = "hidden" name = "quantity_' . $count . '" value = "' . $quantity . '"> ';
            $count++;
        }
        $out .= '<input type="image" src="./img/paypal.png" style="width:250px;" /></form>';
        return $out;
    }


    public function edit_product($product) {
        $pro = file_get_contents("./views/edit_product.html");
        $pro = str_replace("[[price]]", $product->price, $pro);
        $pro = str_replace("[[title]]", $product->title, $pro);
        $pro = str_replace("[[description]]", $product->description, $pro);
        $pro = str_replace("[[group]]", $product->group, $pro);
        $pro = str_replace("[[group_title]]", $product->group_title, $pro);
        $pro = str_replace("[[group_description]]", $product->group_description, $pro);
        $pro = str_replace("[[group_long_description]]", $product->group_long_description, $pro);
        $pro = str_replace("[[category]]", $product->category, $pro);
        $pro = str_replace("[[sub_category]]", $product->sub_category, $pro);
        $pro = str_replace("[[image1]]", $product->image1, $pro);
        $pro = str_replace("[[image2]]", $product->image2, $pro);
        $pro = str_replace("[[image3]]", $product->image3, $pro);
        $pro = str_replace("[[image4]]", $product->image4, $pro);
        return $pro;
    }
}
