<?php

ob_start();
session_start();
require("../classes/product.php");

if (isset($_GET["id"]))
    $product_id = $_GET['id'];else $product_id = 3233;
$product = new product($product_id, 1);

require("../classes/marketing.php");
$marketing = new marketing;

require("../classes/cart.php");
$cart = new cart;
if (isset($_POST['add_product_to_cart'])) {
    $cart->change_product($_POST['add_product_to_cart'], $_POST['quantity']);
}

require("./views/view_functions.php");
$vf = new view_functions;

$meta = array();
$meta["title"] = "GBmakers - " . $product->title;
$meta["description"] = "The home of interior design craft pieces and wooden craft supplies";
$meta["keywords"] = "wooden crafts, ring binders, wall art";
echo $vf->get_header($meta, $cart, $marketing);


echo $vf->product_page($product, $cart);
//echo $vf->related_products();



echo $vf->get_footer();

