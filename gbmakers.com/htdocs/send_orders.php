<?php

require_once("/opt/bitnami/apache2/classes/apiCaller.php");
$apiCaller = new apiCaller;

require_once("/opt/bitnami/apache2/classes/db.php");
$db = new db;

$sql = "SELECT * FROM orders 
        WHERE `sent_for_processing` <> 1 AND `paid` = 1;";
$result = mysqli_query($db->cn, $sql);
$orders = mysqli_fetch_all($result, MYSQLI_ASSOC);

foreach ($orders AS $order) {
    $sql = "SELECT * FROM order_products op LEFT JOIN products p ON p.sku = op.sku
        WHERE `order` = " . $order["id"] . ";";
    $result = mysqli_query($db->cn, $sql);
    $order_products = mysqli_fetch_all($result, MYSQLI_ASSOC);
    $products = array();

    foreach ($order_products AS $product) {
        $products[] = array("product_id" => $product["sku"], "quantity" => $product['quantity'], "title" => $product['title'], "unit_price" => $product['price']);
    }

    $order_date = date("Y-m-d");
    $order_time = date("H:i:s");
    $address_array = explode("<br>",$order["shipping_address"]); 
    $postcode = end($address_array);
    $orderNumber = $apiCaller->send_order($order["id"], $order["total_amount"], $order_date, $order_time, $order["shipping_address"], $postcode, $order["phone"], $order["email"], $order["fname"] . " " . $order["sname"], $order["id"], $products);
    
    $sql = "UPDATE `gbmakerswebsite`.`orders` SET `sent_for_processing` = 1, `order_number` = ".$orderNumber." WHERE `id` = ".$order["id"];
    $result = mysqli_query($db->cn, $sql);
}
