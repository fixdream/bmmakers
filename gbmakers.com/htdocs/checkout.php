<?php

ob_start();
session_start();
//session_destroy();
require("../classes/product.php");

require("../classes/cart.php");
$cart = new cart;
if($cart->total_count == 0){
    header("Location: /home");
}

require("../classes/marketing.php");
$marketing = new marketing;

$message = "";
require("../classes/order.php");
if (isset($_POST['update_order'])) {

    $error = 0;
    if ($_POST['fname'] == "") {
        $error = 1;
        $message = '<div class="alert alert-warning" role="alert">You didn\'t give your first name</div>';
    }
    if ($_POST['sname'] == "") {
        $error = 1;
        $message = '<div class="alert alert-warning" role="alert">You didn\'t give your second name/div>';
    }
    if ($_POST['phone'] == "") {
        $error = 1;
        $message = '<div class="alert alert-warning" role="alert">You didn\'t give your phone number</div>';
    }
    if ($_POST['address1'] == "") {
        $error = 1;
        $message = '<div class="alert alert-warning" role="alert">You didn\'t give the first line of your address</div>';
    }
    if ($_POST['postcode'] == "") {
        $error = 1;
        $message = '<div class="alert alert-warning" role="alert">You didn\'t give your Postcode</div>';
    }
    if ($_POST['email'] == "") {
        $error = 1;
        $message = '<div class="alert alert-warning" role="alert">You didn\'t give your Email</div>';
    }
    
    if (isset($_POST['maillist'])) {
        $marketing->email = $_POST['email'];
        $marketing->add_email_to_list();
    }
    
    if ($error == 0) {
        $order = new order();
        $order->add_cart($cart);
        $order->email = $_POST['email'];
        $order->fname = $_POST['fname'];
        $order->sname = $_POST['sname'];
        $order->phone = $_POST['phone'];
        $company = "";
        if ($_POST['company'] != "") {
            $company = $_POST['company'] . "<br>";
        }
        $order->shipping_address = $_POST['fname'] . " " . $_POST['sname'] . "<br>" . $company . $_POST['address1'] . "<br>" . $_POST['address2'] . "<br>" . $_POST['postcode'];
        $order->billing_address = $_POST['fname'] . " " . $_POST['sname'] . "<br>" . $_POST['company'] . "<br>" . $_POST['address1'] . "<br>" . $_POST['address2'] . "<br>" . $_POST['postcode'];
        $order->save_order();
        header("Location: /payment");
    } else {
        
    }
} else {
    if (isset($_SESSION['order'])) {
        $order = new order();
    }
}

require("./views/view_functions.php");
$vf = new view_functions;

$meta = array();
$meta["title"] = "GBmakers - Checkout Page";
$meta["description"] = "The home of interior design craft pieces and wooden craft supplies";
$meta["keywords"] = "wooden crafts, ring binders, wall art";
echo $vf->get_header($meta, $cart, $marketing);


echo $vf->checkout_page($cart, $message);


echo $vf->get_footer();

