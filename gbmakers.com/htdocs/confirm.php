<?php

ob_start();
session_start();
//session_destroy();
require("../classes/product.php");

require("../classes/cart.php");
$cart = new cart;

require("../classes/marketing.php");
$marketing = new marketing;

if (isset($_SESSION['order'])) {
    require("../classes/order.php");
    $order = new order();
}else{
    header("Location: /home");
}
if($order->paid == 0){
    header("Location: /home");
}

require("./views/view_functions.php");
$vf = new view_functions;

$meta = array();
$meta["title"] = "GBmakers - Confirmation Page";
$meta["description"] = "The home of interior design craft pieces and wooden craft supplies";
$meta["keywords"] = "wooden crafts, ring binders, wall art";
echo $vf->get_header($meta, $cart, $marketing);


echo $vf->confirm_page($cart, $order);


echo $vf->get_footer();

session_destroy();

echo '<script src="https://js.stripe.com/v3/"></script>';
echo '<script src="js/payment.js"></script>';
