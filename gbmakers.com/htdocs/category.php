<?php

ob_start();
session_start();

require("../classes/product.php");

require("../classes/marketing.php");
$marketing = new marketing;

require("../classes/cart.php");
$cart = new cart;
if (isset($_POST['add_product_to_cart'])) {
    $cart->change_product($_POST['add_product_to_cart'], $_POST['quantity']);
}

require("./views/view_functions.php");
$vf = new view_functions;

$cat = $_GET['cat'];

$meta = array();
$meta["title"] = "GBmakers - " . $cat;
$meta["description"] = "The home of interior design craft pieces and wooden craft supplies";
$meta["keywords"] = "wooden crafts, ring binders, wall art";
echo $vf->get_header($meta, $cart, $marketing);

$cat = explode("/",$_GET['cat']);
if (isset($cat[1])) {
    $page = $vf->category_page($cat[0], $cat[1]);
} else {
        $page = $vf->category_page($cat[0]);
}

echo $page;
//echo $vf->related_products();



echo $vf->get_footer();