<?php

ob_start();
session_start();

require("../classes/product.php");
$search = "";
if(isset($_POST['search'])) {
    $search = $_POST['search'];
}

require("../classes/marketing.php");
$marketing = new marketing;

require("../classes/cart.php");
$cart = new cart;
if (isset($_POST['add_product_to_cart'])) {
    $cart->change_product($_POST['add_product_to_cart'], $_POST['quantity']);
}

require("./views/view_functions.php");
$vf = new view_functions;

$meta = array();
$meta["title"] = "GBmakers - Search";
$meta["description"] = "The home of interior design craft pieces and wooden craft supplies";
$meta["keywords"] = "wooden crafts, ring binders, wall art";
echo $vf->get_header($meta, $cart, $marketing);

echo $vf->search_page($search);
//echo $vf->related_products();



echo $vf->get_footer();

