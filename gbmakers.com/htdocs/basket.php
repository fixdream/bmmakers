<?php

ob_start();
session_start();

require("../classes/product.php");

require("../classes/cart.php");
$cart = new cart;
if($cart->total_count == 0){
    header("Location: /home");
}

require("../classes/marketing.php");
$marketing = new marketing;

if (isset($_POST['update_cart'])) {
    foreach ($_POST AS $product => $quantity) {
        if($product != "update_cart") {
            $cart->change_product($product, $quantity);
        }
    }
}

if (isset($_POST["delete"])){
    $cart->change_product($_POST["delete"], 0);
}

//session_destroy();
require("./views/view_functions.php");
$vf = new view_functions;

$meta = array();
$meta["title"] = "GBmakers - Basket Page";
$meta["description"] = "The home of interior design craft pieces and wooden craft supplies";
$meta["keywords"] = "wooden crafts, ring binders, wall art";
echo $vf->get_header($meta, $cart, $marketing);


echo $vf->basket_page($cart);


echo $vf->get_footer();

