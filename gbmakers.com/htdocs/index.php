<?php
ob_start();
session_start();
//session_destroy();
require("../classes/product.php");


require("../classes/marketing.php");
$marketing = new marketing;

require("../classes/cart.php");
$cart = new cart;


require_once("./views/view_functions.php");
$vf = new view_functions;

$meta = array();
$meta["title"] = "GBmakers - Home";
$meta["description"] = "The home of interior design craft pieces and wooden craft supplies";
$meta["keywords"] = "wooden crafts, ring binders, wall art";
echo $vf->get_header($meta, $cart, $marketing);
echo $vf->get_banner();

$list = array(3262, 3244, 3233, 3232, 3227, 3336, 3280, 3326);
echo $vf->home_page_products($list);


echo $vf->get_footer();
 
     