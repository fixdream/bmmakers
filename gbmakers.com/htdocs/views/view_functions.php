<?php

class view_functions {

    public $header, $banner, $footer;

    public function get_header($meta, $cart, $marketing) {
        $header = file_get_contents("./views/header.html");
        $header = str_replace("[[title]]", $meta["title"], $header);
        $header = str_replace("[[description]]", $meta["description"], $header);
        $header = str_replace("[[keywords]]", $meta["keywords"], $header);

        //cart
        if ($cart->total_count == 0) {
            $items_incart = "<li>Your&nbsp;Basket&nbsp;Is Empty</li>";
        } else {
            $s = "";
            if ($cart->total_count > 1) {
                $s = "s";
            }
            $items_incart = "<li>" . $cart->total_count . " Item" . $s . " in your basket</li>";
            foreach ($cart->productsWithQuanties AS $product => $quantity) {
                $pro = new product($product);
                $items_incart .= '                
                  <li>
                    <a href="single-product.html">
                      <div class="media">
                        <img class="media-left media-object" src="' . $pro->image1 . '" style="width:60px;" alt="cart-Image">
                        <div class="media-body">
                          <h5 class="media-heading"><small>' . $pro->title . '</small><br><span>' . $quantity . ' X &pound;' . $pro->price . '</span></h5>
                        </div>
                      </div>
                    </a>
                  </li>
                  <li>';
            }
            $items_incart .= '
                <div class="btn-group" role="group" aria-label="...">
                    <form action="/basket"><button type="submit" class="btn btn-default" >Edit Basket</button></form>
                    <form action="/checkout"><button type="submit" class="btn btn-default" >Checkout</button></form>
        </div>';
        }

        $header = str_replace("[[items_incart]]", $items_incart, $header);
        $header = str_replace("[[cart_total]]", number_format($cart->total_value, 2), $header);
        $header = str_replace("[[badge]]", $cart->total_count, $header);
        $header = str_replace("[[marketing_message]]", $marketing->message, $header);
        include("../classes/page.php");
        $page = new page;
        $header = str_replace("[[menu]]", $this->menu($page), $header);

        $this->header = $header;
        return $header;
    }

    public function get_banner() {
        $out = '
      <!-- BANNER -->
      <div class="bannercontainer bannerV1">
        <div class="fullscreenbanner-container">
          <div class="fullscreenbanner">
            <ul>';
        $end = "                    
            </ul>
          </div>
        </div>
      </div>";

        $template = file_get_contents("./views/banner.html");
        $bannerData = json_decode(file_get_contents("./views/banner.json"), 1);
        foreach ($bannerData AS $banner) {
            $frame = $template;
            $frame = str_replace("[[directionMeaasge]]", $banner["directionMessage"], $frame);
            $frame = str_replace("[[fromPrice]]", $banner["fromPrice"], $frame);
            $frame = str_replace("[[title]]", $banner["title"], $frame);
            $frame = str_replace("[[message]]", $banner["message"], $frame);
            $frame = str_replace("[[image]]", $banner["image"], $frame);
            $frame = str_replace("[[link]]", $banner["link"], $frame);
            $out .= $frame;
        }
        $out .= $end;
        $this->banner = $out;
        return $out;
    }

    public function get_footer() {
        $footer = file_get_contents("./views/footer.html");
        $footer = str_replace("[[year]]", date("Y"), $footer);
        $this->banner = $footer;
        return $footer;
    }

    public function about_page() {
        $about = file_get_contents("./views/about.html");
        return $about;
    }

    public function contact_page() {
        $contact = file_get_contents("./views/contact.html");
        return $contact;
    }

    public function basket_page($cart) {
        $products = "";
        $count = 0;
        foreach ($cart->productsWithQuanties AS $product => $quantity) {
            $count++;
            $pro = new product($product);
            $products .= '
                        <tr>
                          <td class="">
                            <button type="submit" class="close" onclick="remove(' . $product . ')" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <span class="cartImage"><img src="' . $pro->image1 . '" style="width:50px;" alt="image"></span>
                          </td>
                          <td class="">' . $pro->title . '</td>
                          <td class="">&pound;' . $pro->price . '</td>
                          <td class="count-input">
                            <a class="incr-btn" data-action="decrease" href="#"><i class="fa fa-minus"></i></a>
                            <input class="quantity" type="text" name="' . $product . '" value="' . $quantity . '">
                            <a class="incr-btn" data-action="increase" href="#"><i class="fa fa-plus"></i></a>
                          </td>
                          <td class="">&pound;' . number_format(($pro->price * $quantity), 2) . '</td>
                        </tr>';
        }

        $basket = file_get_contents("./views/basket.html");
        $basket = str_replace("[[products]]", $products, $basket);
        $basket = str_replace("[[grand_total]]", number_format($cart->total_value, 2), $basket);
        $basket = str_replace("[[products]]", $products, $basket);
        return $basket;
    }

    public function product_page($product, $cart) {
        $out = file_get_contents("./views/product.html");
        $out = str_replace("[[image1]]", $product->image1, $out);
        $out = str_replace("[[image2]]", $product->image2, $out);
        $out = str_replace("[[image3]]", $product->image3, $out);
        $out = str_replace("[[image4]]", $product->image4, $out);

        $out = str_replace("[[title]]", $product->title, $out);
        $out = str_replace("[[price]]", $product->price, $out);
        $out = str_replace("[[description]]", $product->description, $out);
        $out = str_replace("[[productID]]", $product->id, $out);

        $product->create_product_data_layer();
        $out = str_replace("[[datalayer]]", $product->data_layer, $out);

        $in_cart = $cart->in_cart($product->id);
        $cart_message = "";
        if ($in_cart > 0) {
            $cart_message = "<p><b>You have " . $in_cart . " of these in the basket</b></p>";
        }
        $out = str_replace("[[in_cart]]", $cart_message, $out);

        $out = str_replace("[[group_description]]", $product->group_description, $out);

        $quantities = '<option value="0">Qty</option>';
        $count = 1;
        $selected = "";
        while ($count <= 10) {
            if ($in_cart == $count) {
                $selected = "selected";
            }
            $quantities .= '<option value="' . $count . '" ' . $selected . '>' . $count . '</option>';
            $selected = "";
            $count++;
        }
        $out = str_replace("[[quantity_options]]", $quantities, $out);

        //skus
        $dropdowns = "";
        $count = 1;
        foreach ($product->variation_values AS $name => $var) {
            $dropdowns .= "
                    <h3>$name</h3>
                    <span class='quick-drop'>
                        <select name='$name' id='option_$count' class='select-drop'>
                    ";
            foreach ($var AS $value) {
                $dropdowns .= "<option value='$value'>$value</option>";
            }
            $dropdowns .= '</select>
              </span>';
            // $skus .= "<option value='" . $sku["id"] . "'>" . $title . "</option>";
            $count++;
        }
        $out = str_replace("[[dropdowns]]", $dropdowns, $out);

        $script = $this->product_page_script($product);
        $out = str_replace("[[script]]", $script, $out);

        return $out;
    }

    private function product_page_script($product) {
        //script 
        $script = '<script>$( document ).ready(function() {';
        
        // listeners 
        for ($i = 1; $i <= count($product->variation_values); $i++) {
            $script .= '
                if($("#option_'.$i.'").length != 0) {
                    $("#option_'.$i.'").on("change", function() {
                        get_sku();
                    });
                }';
        }
        // sku function
        $script .= 'var get_sku = function()  {';
        for ($i = 1; $i <= count($product->variation_values); $i++) {
                $script .= 'var opt_'.$i.' = $("#option_'.$i.'").val();console.log(opt_'.$i.');';

        }
        $script .= 'products.foreach(function(product){console.log(product)});';
        
        $script .= '};';
                
                
            
        $script .= 'console.log(JSON.parse(products));';
        $script .= '});</script>';
        return $script;
    }

    public function related_products() {
        $out = file_get_contents("./views/related_products.html");
        $out = str_replace("[[delivery]]", number_format(0, 2), $out);

        return $out;
    }

    public function checkout_page($cart, $message) {
        $basket = file_get_contents("./views/checkout_page.html");
        $subtotal = $cart->total_value;
        //$shipping = number_format((3 * $cart->total_count), 2);
        $total = number_format($subtotal, 2);
        $basket = str_replace("[[subtotal]]", number_format($subtotal, 2), $basket);
        //$basket = str_replace("[[shipping]]", $shipping, $basket);
        $basket = str_replace("[[total]]", $total, $basket);
        $basket = str_replace("[[message]]", $message, $basket);
        $basket = str_replace("[[delivery]]", number_format(0, 2), $basket);

        return $basket;
    }

    public function payment_page($cart, $order, $message) {
        $basket = file_get_contents("./views/payment.html");
        $subtotal = number_format($cart->total_value, 2);
        $total = $subtotal;
        $basket = str_replace("[[subtotal]]", $subtotal, $basket);
        $basket = str_replace("[[total]]", $total, $basket);
        $basket = str_replace("[[shipping_address]]", $order->shipping_address, $basket);

        $paypalForm = $this->paypal_form($order);
        $basket = str_replace("[[paypal_form]]", $paypalForm, $basket);
        $basket = str_replace("[[message]]", $message, $basket);
        $basket = str_replace("[[delivery]]", number_format(0, 2), $basket);
        return $basket;
    }

    public function confirm_page($cart, $order) {
        $basket = file_get_contents("./views/confirmation.html");
        $subtotal = number_format($cart->total_value, 2);
        $total = $subtotal;
        $basket = str_replace("[[subtotal]]", $subtotal, $basket);
        $basket = str_replace("[[total]]", $total, $basket);
        $basket = str_replace("[[shipping_address]]", $order->shipping_address, $basket);

        $basket = str_replace("[[email]]", $order->email, $basket);
        $basket = str_replace("[[shipping_address]]", $order->shipping_address, $basket);
        $basket = str_replace("[[temp]]", $order->id, $basket);
        $basket = str_replace("[[delivery]]", number_format(0, 2), $basket);
        return $basket;
    }

    public function category_page($cat, $subcat = "") {
        $product = new product;
        if ($subcat == "") {
            $products = $product->get_category_products($cat);
            $title = str_replace("_", " ", $cat);
        } else {
            $products = $product->get_category_products($cat, $subcat);
            $title = str_replace("_", " ", $cat) . " / " . str_replace("_", " ", $subcat);
        }
        $out = '
      <!-- LIGHT SECTION -->
      <section class="lightSection clearfix pageHeader">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-6">
              <div class="page-title">
                <h2>' . $title . '</h2>
              </div>
            </div>
            <div class="col-6">
              <ol class="breadcrumb pull-right">
                <li>
                  <a href="/home">Home</a>
                </li>
                <li>
                  <a href="/category/' . $cat . '">' . str_replace("_", " ", $cat) . ' </a>
                </li>';
        if ($subcat != 0) {
            $out .= '<li class="active">' . str_replace("_", " ", $subcat) . '</li>';
        }
        $out .= '
              </ol>
            </div>
          </div>
        </div>
      </section>
        ';

        $out .= $this->product_list($products);
        return $out;
    }

    public function home_page_products($list) {
        $product = new product;
        $products = $product->get_product_list($list);
        $count = count($products);
        $out = '';
        $out .= $this->product_list($products);
        return $out;
    }

    public function search_page($search) {
        $product = new product;
        $products = $product->search_products($search);
        $count = count($products);
        $out = '
                  <!-- LIGHT SECTION -->
      <section class="lightSection clearfix pageHeader">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-12">
              <div class="page-title">
                <h2>' . $count . ' results for search term: "' . strip_tags($search) . '"</h2>
              </div>
            </div>
          </div>
        </div>
      </section>
   ';

        $out .= $this->product_list($products);
        return $out;
    }

    public function product_list($products) {
        $out = '
      <!-- MAIN CONTENT SECTION -->
      <section class="mainContent clearfix productsContent">
        <div class="container">          
        <div class="row">
';
        foreach ($products AS $product) {
            $out .= '
            <div class="col-lg-3 col-md-4 ">
              <div class="productBox">
                <div class="productImage clearfix">
                  <a href="/product/' . $product['id'] . '"><img src="' . $product['link'] . '"></a>

                </div>
                <div class="productCaption clearfix">
                  <a href="single-product.html">
                    <h5>' . $product['title'] . '</h5>
                  </a>
                  <h3></h3>
                </div>
              </div>
            </div>
                    ';
        }
        $out .= '
          </div>
        </div>
      </section>';
        return $out;
    }

    public function paypal_form($order) {
        $out = '<form action="https://www.paypal.com/cgi-bin/webscr" method="post">  ';
        $out .= '
                 <input type="hidden" name="cmd" value="_cart">   <!--required -->
                 <input type="hidden" name="upload" value="1">  <!--required -->
                 <input type="hidden" name="currency_code" value="GBP">
                 <input type="hidden" name="business" value="enquiries@gbmakers.com"> <!--required -->
                 <input type="hidden" name="invoice" value="' . $order->id . '">
                ';
        $count = 1;
        foreach ($order->products AS $product => $quantity) {
            $pro = new product($product);
            $out .= '
            <input type = "hidden" name = "item_name_' . $count . '" value = "' . $pro->title . '"> <!--title -->
            <input type = "hidden" name = "item_number_' . $count . '" value = "' . $pro->sku . '"> <!--title -->
            <input type = "hidden" name = "amount_' . $count . '" value = "' . $pro->price . '">
            <input type = "hidden" name = "quantity_' . $count . '" value = "' . $quantity . '"> ';
            $count++;
        }
        $out .= '<input type="image" src="./img/paypal.png" style="width:250px;" /></form>';
        return $out;
    }

    public function menu($page) {
        // making page active: <li class="nav-item dropdown  active ">
        $out = '       <div class="collapse navbar-collapse navbar-ex1-collapse">
                            <ul class="nav navbar-nav ml-auto">';
        foreach ($page->categories AS $cat) {
            $category = str_replace("_", " ", $cat['category']);
            $out .= '                <li class="nav-item dropdown">
                                    <a href="/category/' . $category . '" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">' . $category . '</a>
                                    <ul class="dropdown-menu dropdown-menu-left">';
            foreach ($page->subcategories[$cat['category']] AS $sub) {
                $subcategory = str_replace("_", " ", $sub['sub_category']);
                $out .= ' <li class="active"><a href="/category/' . $cat['category'] . '/' . $sub['sub_category'] . '">' . $subcategory . '</a></li>';
            }
            $out .= '
                                    </ul>
                                </li>';
        }
        $out .= '
                            </ul>
                        </div>';

        return $out;
    }

    public function edit_product($product) {
        $pro = file_get_contents("./views/edit_product.html");
        $pro = str_replace("[[price]]", $product->price, $pro);
        $pro = str_replace("[[title]]", $product->title, $pro);
        $pro = str_replace("[[description]]", $product->description, $pro);
        $pro = str_replace("[[group]]", $product->group, $pro);
        $pro = str_replace("[[group_title]]", $product->group_title, $pro);
        $pro = str_replace("[[group_description]]", $product->group_description, $pro);
        $pro = str_replace("[[group_long_description]]", $product->group_long_description, $pro);
        $pro = str_replace("[[category]]", $product->category, $pro);
        $pro = str_replace("[[sub_category]]", $product->sub_category, $pro);
        $pro = str_replace("[[image1]]", $product->image1, $pro);
        $pro = str_replace("[[image2]]", $product->image2, $pro);
        $pro = str_replace("[[image3]]", $product->image3, $pro);
        $pro = str_replace("[[image4]]", $product->image4, $pro);
        return $pro;
    }

}
