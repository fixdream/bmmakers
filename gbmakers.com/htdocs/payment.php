<?php

ob_start();
session_start();
//session_destroy();
require("../classes/product.php");

require("../classes/cart.php");
$cart = new cart;
if($cart->total_count == 0){
    header("Location: ./home");
}

require("../classes/marketing.php");
$marketing = new marketing;

require("../classes/order.php");
$order = new order();
$message = "";
if($order->email == ""){
    header("Location: ./home");
}



if (isset($_POST['update_order'])) {
    $order = new order();
    if ($_POST['postcode'] != "") {
        $order->billing_address = $_POST['address1'] . "<br>" . $_POST['address2'] . "<br>" . $_POST['postcode'];
        $order->save_order();
    }

    $error = 0;
    if ($_POST['card_number'] == "") {
        $error = 1;
        $message = '<div class="alert alert-warning" role="alert">You didn\'t give a card number</div>';
    }
    if ($_POST['exp_month'] == "") {
        $error = 1;
        $message = '<div class="alert alert-warning" role="alert">You didn\'t give a card expiry month</div>';
    }
    if ($_POST['exp_year'] == "") {
        $error = 1;
        $message = '<div class="alert alert-warning" role="alert">You didn\'t give a card expiry year</div>';
    }
    if ($_POST['cvc'] == "") {
        $error = 1;
        $message = '<div class="alert alert-warning" role="alert">You didn\'t give a card CVC</div>';
    }
    if ($_POST['card_name'] == "") {
        $error = 1;
        $message = '<div class="alert alert-warning" role="alert">You didn\'t give the name on the card</div>';
    }
    if ($error == 0) {
        require("../classes/stripe.php");

        try {
            $payment = new stripe($_POST['card_number'], $_POST['exp_month'], $_POST['exp_year'], $_POST['cvc'], $order->total_amount, $order->email, $_POST['card_name'],$order->id);
            if ($payment->charge['status'] == "succeeded") {
                $order->payment_chanel = 'STRIPE';
                $order->paid = 1;
                $order->payment_ref = $payment->charge['id'];
                $order->save_order();
                header("Location: ./confirm");
            } else {
                $message = '<div class="alert alert-danger" role="alert">There was a problem with payment</div>';
            }
        } catch (Exception $e) {
            $message = '<div class="alert alert-danger" role="alert">There was a problem with payment</div>';
        }
    }
}

require("./views/view_functions.php");
$vf = new view_functions;

$meta = array();
$meta["title"] = "GBmakers - Payment Page";
$meta["description"] = "The home of interior design craft pieces and wooden craft supplies";
$meta["keywords"] = "wooden crafts, ring binders, wall art";
echo $vf->get_header($meta, $cart, $marketing);


echo $vf->payment_page($cart, $order, $message);


echo $vf->get_footer();

echo '<script src="https://js.stripe.com/v3/"></script>';
echo '<script src="js/payment.js"></script>';
