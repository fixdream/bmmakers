/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



$(document).ready(function () {
    tog = 1;
    $("#paypal_box").hide();
    $('#paypal').change(function () {
        if (tog == 1) {
            $("#paypal_box").show();
            $("#card_payment").hide();
            tog = 0;
        } else {
            $("#paypal_box").hide();
            $("#card_payment").show();
            tog = 1;
        }
    });

    $('#card').change(function () {
        if (tog == 1) {
            $("#paypal_box").show();
            $("#card_payment").hide();
            tog = 0;
        } else {
            $("#paypal_box").hide();
            $("#card_payment").show();
            tog = 1;
        }
    });
    
    //billing_check
    tog2 = 1
     $('#billing_check').change(function () {
        if (tog2 == 1) {
            $("#billing_form").show();
            tog2 = 0;
        } else {
            $("#billing_form").hide();
            tog2 = 1;
        }
    });   
    
});
