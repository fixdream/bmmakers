<?php

/*
Test Card Details:
 
4242424242424242	Visa
4000056655665556	Visa (debit)
5555555555554444	Mastercard
2223003122003222	Mastercard (2-series)
5200828282828210	Mastercard (debit)
5105105105105100	Mastercard (prepaid)
378282246310005	American Express
371449635398431	American Express
6011111111111117	Discover
6011000990139424	Discover
30569309025904	Diners Club
38520000023237	Diners Club
3530111333300000	JCB
6200000000000005	UnionPay
*/
class stripe {

    public $token, $charge, $key;

    public function __construct($card_number,$exp_month,$exp_year,$cvc,$total,$email,$name,$order_id) {
        
        require("../settings.php");
        if ($payments == "live") {
            $this->key = $stripeKey['live'];
        } else {
            $this->key = $stripeKey['dev'];
        }
        
        require('stripe/init.php');
        \Stripe\Stripe::setApiKey($this->key);

        $this->token = \Stripe\Token::create(array(
            "card" => array(
                "name" => $name,
                "number" => $card_number,
                "exp_month" => $exp_month,
                "exp_year" => $exp_year,
                "cvc" => $cvc
            )
        ));
         
        $description = "Charge for customer: ".$email." order: ".$order_id." site: ".$site;
        
        $this->charge = \Stripe\Charge::create(array(
            "amount" => ($total * 100),
            "currency" => "gbp",
            "source" => $this->token['id'], // obtained with Stripe.js
            "description" => $description
        ));
    }

}
