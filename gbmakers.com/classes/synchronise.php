<?php

/*
 * To reimport truncate 4 tables: product_group, products, product_variations, product_images
 */
require("db.php");

class synchronise extends db {

    // connection variables
    private $appID, $compatabilityLevel;
    // singular object attributes
    public $product_group, $description, $ending_time, $location, $category, $category_leaf_id, $title, $single_item_quantity,
            $status, $country, $condition, $are_variations, $single_item_price, $returns_accepted, $brand,
            $shipping_options, $collection_only;
    // array object attributes
    public $images, $specifics, $variations;
    // ebay user
    public $feedback_score, $feedback_percent, $ebay_user;
    // all returned data from ebay
    public $listingData, $rawShippingData, $is_fixed_price, $listing_type, $dispatch_days;

    public function __construct() {
        parent::__construct();
        $this->compatabilityLevel = 717;
        $this->appID = "patricks-gbmakers-PRD-d60bef6c5-be5686bb";

        $this->listingData = $this->import_product();

        $this->description = $this->listingData["Item"]["Description"];
        $this->ending_time = $this->listingData["Item"]["EndTime"];
        $this->location = $this->listingData["Item"]["Location"];
        $this->category = $this->listingData["Item"]["PrimaryCategoryName"];
        $this->category_leaf_id = $this->listingData["Item"]["PrimaryCategoryID"];
        $this->title = $this->listingData["Item"]["Title"];
        $this->status = $this->listingData["Item"]["ListingStatus"];
        $this->country = $this->listingData["Item"]["Country"];
        if (!isset($this->listingData["Item"]["ConditionDisplayName"]))
            $this->listingData["Item"]["ConditionDisplayName"] = "";
        $this->condition = $this->listingData["Item"]["ConditionDisplayName"];
        $this->listing_type = $this->listingData["Item"]["ListingType"];
        $this->is_fixed_price = $this->listing_type == "FixedPriceItem" ? 1 : 0;

        $this->ebay_user = $this->listingData["Item"]["Seller"]["UserID"];
        $this->feedback_percent = $this->listingData["Item"]["Seller"]["PositiveFeedbackPercent"];
        $this->feedback_score = $this->listingData["Item"]["Seller"]["FeedbackScore"];
        $this->collection_only = 0;
        
        $this->set_vars();
        $this->save_variations();
        //echo "<script>location.reload();</script>";
    
    }

 


    public function save_variations() {print_r($this->variations);
        if ($this->are_variations) {
            foreach ($this->variations AS $var) {
                $title = "";
                foreach($var['specifics'] AS $key => $val){
                    $title .= $key." - ".$val." ";
                }
                $sql = "
                    UPDATE `products` 
                    SET `title` = '" . trim($title) . "'
                    WHERE `sku` = '" . $var['SKU'] . "'";
                mysqli_query($this->cn, $sql);
                $product = mysqli_insert_id($this->cn);
            }
        } else {
            
        }
    }

    private function import_product() {        
        $sql = "SELECT `id`, `ebay_listing` FROM `product_group` WHERE `sync` IS NULL LIMIT 1";
        $result = mysqli_query($this->cn, $sql)->fetch_assoc();
        $this->product_id = $result["ebay_listing"];
        
        if($this->product_id == "")
            exit;
        
        $sql = "UPDATE `product_group` SET `sync` = 1 WHERE `id` = '".$result['id']."' LIMIT 1";
        $result = mysqli_query($this->cn, $sql);

        $url = "http://open.api.ebay.com/shopping?callname=GetSingleItem&responseencoding=XML&"
                . "appid=$this->appID&version=$this->compatabilityLevel&IncludeSelector=Details,Variations,Description,ItemSpecifics,ShippingCosts,DispatchTimeMax,&&ItemID=$this->product_id";
        $responseDoc = file_get_contents($url);
        $response = new SimpleXMLElement($responseDoc);
        $response = json_decode(json_encode($response), TRUE);
        if ((!isset($response["Ack"])) || ($response["Ack"] != "Success" && $response["Ack"] != "Warning")) {
            echo "error with ebay's response ";
            print_r($response);
            exit;
        }
        return $response;
    }
    
    
    private function set_vars() {
        if (isset($this->listingData["Item"]["Variations"])) {
            $this->variations = array();

            // if there is only one variation
            if (isset($this->listingData["Item"]["Variations"]["Variation"]["StartPrice"])) {
                $variation = $this->listingData["Item"]["Variations"]["Variation"];
                $this->are_variations = false;
                $var["price"] = $variation["StartPrice"];
                $var["quantity"] = $variation["Quantity"];
                $var["sold"] = $variation["SellingStatus"]["QuantitySold"];
                if (isset($variation["SKU"]))
                    $var["SKU"] = $variation["SKU"];
                if (isset($variation["VariationSpecifics"]["NameValueList"][0]) && is_array($variation["VariationSpecifics"]["NameValueList"][0])) {
                    $specifics = $variation["VariationSpecifics"]["NameValueList"];
                    foreach ($specifics AS $spec)
                        $var["specifics"][$spec["Name"]] = $spec["Value"];
                } else
                    $var["specifics"][$variation["VariationSpecifics"]["NameValueList"]["Name"]] = $variation["VariationSpecifics"]["NameValueList"]["Value"];
                $this->variations[] = $var;
            } else {
                $this->are_variations = true;
                // multiple variations
                foreach ($this->listingData["Item"]["Variations"]["Variation"] AS $variation) {
                    $var["price"] = $variation["StartPrice"];
                    $var["quantity"] = $variation["Quantity"];
                    $var["sold"] = $variation["SellingStatus"]["QuantitySold"];
                    if (isset($variation["SKU"]))
                        $var["SKU"] = $variation["SKU"];
                    $specifics = $variation["VariationSpecifics"]["NameValueList"];
                    if (isset($specifics["Name"]))
                        $specifics = array($specifics);
                    foreach ($specifics AS $spec)
                        $var["specifics"][$spec["Name"]] = $spec["Value"];
                    $this->variations[] = $var;
                }
            }
        }
        else {
            // no variations - yes there can be 1 variation 'with variations' in ebay
            $this->are_variations = false;
            $this->single_item_price = $this->listingData["Item"]["CurrentPrice"];
            $this->single_item_quantity = $this->listingData["Item"]["Quantity"];
        }
    }


}
