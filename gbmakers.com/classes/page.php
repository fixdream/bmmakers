<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class page extends db {

    public $categories, $subcategories;

    public function __construct($order = false) {
        $this->subcategories = array();
        $this->categories = array();
        parent::__construct();
        $this->menu();
    }

    public function menu() {
        $stmt = $this->cn->prepare("SELECT DISTINCT  `category` FROM product_group  WHERE `display` = 1;");
        //$stmt->bind_param("sss", $firstname, $lastname, $email);
        $stmt->execute();
        $result = $stmt->get_result();
        while ($this->categories[] = $result->fetch_assoc()) {
            
        }
        $stmt->close();

        foreach ($this->categories AS $cat) {
            $stmt = $this->cn->prepare("SELECT DISTINCT  `sub_category` FROM product_group WHERE `category` = ?  AND `display` = 1");
            $stmt->bind_param("s", $cat['category']);
            $stmt->execute();
            $result = $stmt->get_result();
            while ($this->subcategories[$cat['category']][] = $result->fetch_assoc()) {
                
            }
            $stmt->close();
        }
    }

}
