<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class marketing extends db {

    public $cn;
    public $email, $message;

    public function __construct() {
        parent::__construct();
        if (isset($_POST['email_newsletter'])) {
            $this->email = filter_var(strtolower($_POST['email_newsletter']), FILTER_VALIDATE_EMAIL);
            $error = false;

            if (!$this->email) {
                $this->message = '<div class="container">
                <div class="alert alert-danger" role="alert">
                    Your input "' . $this->email . '" not a valid email. Sorry!
                  </div>
                </div>';
                $error = true;
            }

            if (!$error and ! $this->email_unique()) {
                $error = true;
            }

            if (!$error) {
                $this->add_email_to_list();
            }
        } else {
            $this->message = '';
        }
    }

    public function add_email_to_list() {
        $sql = "INSERT INTO `mail_list` (
        `id` ,
        `email` ,
        `first_name` ,
        `last_name` ,
        `timestamp_joined` ,
        `active_permission` ,
        `timestamp_left`
        )
        VALUES (
        NULL ,  
        '".mysqli_escape_string($this->cn,$this->email)."',  
        '',  
        '', 
        CURRENT_TIMESTAMP ,  
        '1',  
        '0000-00-00 00:00:00'
        )";
        if($this->email_unique()){
        $result = mysqli_query($this->cn, $sql);
        $this->message = '<div class="container">
                <div class="alert alert-success" role="alert">
                    Your email "' . $this->email . '" has been added to our mail list. Thank you!
                  </div>
                </div>';
        }
    }
    
        public function deactivate() {
        $sql = "UPDATE `mail_list` SET `active_permission` = '0', `timestamp_left` = CURRENT_TIMESTAMP
                WHERE `email` = '" . mysqli_escape_string($this->cn,$this->email) . "'";
        $result = mysqli_query($this->cn, $sql);
        $this->message = '<div class="container">
                <div class="alert alert-success" role="alert">
                    Your email "' . $this->email . '" has been added to our mail list. Thank you!
                  </div>
                </div>';
    }

    private function email_unique() {
        $sql = "SELECT * FROM `mail_list` WHERE `email` = '" . mysqli_escape_string($this->cn,$this->email) . "'";
        $result = mysqli_query($this->cn, $sql);
        $num = mysqli_num_rows($result);
        if ($num > 0) {
            $this->message = '<div class="container">
                <div class="alert alert-warning" role="alert">
                    Your email "' . $this->email . '" is already on our mail list. Thank you!
                  </div>
                </div>';
            return false;
        } else {
            return true;
        }
    }

}
