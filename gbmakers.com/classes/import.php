<?php

/*
 * To reimport truncate 4 tables: product_group, products, product_variations, product_images
 */
require("db.php");

class import extends db {

    // connection variables
    private $appID, $compatabilityLevel;
    // singular object attributes
    public $product_group, $description, $ending_time, $location, $category, $category_leaf_id, $title, $single_item_quantity,
            $status, $country, $condition, $are_variations, $single_item_price, $returns_accepted, $brand,
            $shipping_options, $collection_only;
    // array object attributes
    public $images, $specifics, $variations;
    // ebay user
    public $feedback_score, $feedback_percent, $ebay_user;
    // all returned data from ebay
    public $listingData, $rawShippingData, $is_fixed_price, $listing_type, $dispatch_days;

    public function __construct($convert = false) {
        parent::__construct();
        $this->path = "../import/file.csv";
        $this->compatabilityLevel = 717;
        $this->appID = "patricks-gbmakers-PRD-d60bef6c5-be5686bb";
        if ($convert)
            $this->convert_file($this->path);

        $this->listingData = $this->import_product();

        $this->description = $this->listingData["Item"]["Description"];
        $this->ending_time = $this->listingData["Item"]["EndTime"];
        $this->location = $this->listingData["Item"]["Location"];
        $this->category = $this->listingData["Item"]["PrimaryCategoryName"];
        $this->category_leaf_id = $this->listingData["Item"]["PrimaryCategoryID"];
        $this->title = $this->listingData["Item"]["Title"];
        $this->status = $this->listingData["Item"]["ListingStatus"];
        $this->country = $this->listingData["Item"]["Country"];
        if (!isset($this->listingData["Item"]["ConditionDisplayName"]))
            $this->listingData["Item"]["ConditionDisplayName"] = "";
        $this->condition = $this->listingData["Item"]["ConditionDisplayName"];
        $this->listing_type = $this->listingData["Item"]["ListingType"];
        $this->is_fixed_price = $this->listing_type == "FixedPriceItem" ? 1 : 0;

        $this->ebay_user = $this->listingData["Item"]["Seller"]["UserID"];
        $this->feedback_percent = $this->listingData["Item"]["Seller"]["PositiveFeedbackPercent"];
        $this->feedback_score = $this->listingData["Item"]["Seller"]["FeedbackScore"];
        $this->collection_only = 0;

        $this->set_images();
        $this->set_specs();
        $this->set_vars();
        $this->set_return_policy();

        $this->save_product_group();
        $this->save_images();
        $this->save_variations();
        //echo "<script>location.reload();</script>";
    }

    private function save_product_group() {
        $sql = "INSERT INTO `product_group` 
                (
                `title`, 
                `short_description`, 
                `long_description`,
                `display`, 
                `ebay_listing`
                ) 
                VALUES 
                (
                '" . mysqli_real_escape_string($this->cn, $this->title) . "', 
                '" . mysqli_real_escape_string($this->cn, $this->title) . "',
                '" . mysqli_real_escape_string($this->cn, $this->description) . "', 
                '1', 
                '" . mysqli_real_escape_string($this->cn, $this->product_id) . "'
                )";
        $result = mysqli_query($this->cn, $sql);
        $this->product_group = mysqli_insert_id($this->cn);
    }

    public function save_images() {
        $count = 1;
        $title_pre = str_replace(" ", "-", $this->title);
        $title_pre = $this->product_group."-".str_replace("&", "", $title_pre);
        foreach ($this->images AS $image) {
            file_put_contents("../htdocs/img/products/" . $title_pre . "_" . $count . ".jpg", fopen($image, 'r'));
            $sql = "INSERT INTO `product_images` (`group`, `sku`, `link`, `position`)
                    VALUES(
                    '$this->product_group', '', '/img/products/" . $title_pre . "_" . $count . ".jpg', '$count'
                    )";
            $result = mysqli_query($this->cn, $sql);
            $count++;
        }
    }

    public function save_variations() {
        if ($this->are_variations) {
            foreach ($this->variations AS $var) {
                $sql = "
                    INSERT INTO `products` (
                    `sku`, `price`, `title`, `description`, `group`
                    ) VALUES (
                    '" . $var['SKU'] . "',
                    '" . $var['price'] . "', 
                    '" . $this->title . "',
                    '" . mysqli_real_escape_string($this->cn, $this->description) . "',
                    '" . $this->product_group . "'
                    )";
                mysqli_query($this->cn, $sql);
                $product = mysqli_insert_id($this->cn);
                foreach ($var["specifics"] AS $key => $val) {
                    $sql = "
                        INSERT INTO `product_variations`
                        (`product`, `product_group`, `var_name`, `var_value`)
                        VALUES
                        ('" . $product . "', '" . $this->product_group . "', '" . $key . "', '" . $val . "')
                        ";
                    mysqli_query($this->cn, $sql);
                }
            }
        } else {
            
        }
    }

    // used to process raw file from ebay
    private function convert_file($path) {
        $new_file = "";
        $row = 1;
        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                $row++;
                if ($data[1] != "" && $data[1] != "ItemID")
                    $new_file .= $data[1] . "\n";
            }
            fclose($handle);
        }
        file_put_contents($path, $new_file);
    }

    private function import_product() {
        $file = file_get_contents($this->path);
        $lines = explode("\n", $file);
        $this->product_id = $lines[0];
        if($this->product_id == "")
            exit;
        unset($lines[0]);
        $new_file = implode("\n", $lines);
        file_put_contents($this->path, $new_file);


        $url = "http://open.api.ebay.com/shopping?callname=GetSingleItem&responseencoding=XML&"
                . "appid=$this->appID&version=$this->compatabilityLevel&IncludeSelector=Details,Variations,Description,ItemSpecifics,ShippingCosts,DispatchTimeMax,&&ItemID=$this->product_id";
        $responseDoc = file_get_contents($url);
        $response = new SimpleXMLElement($responseDoc);
        $response = json_decode(json_encode($response), TRUE);
        if ((!isset($response["Ack"])) || ($response["Ack"] != "Success" && $response["Ack"] != "Warning")) {
            echo "error with ebay's response ";
            print_r($response);
            exit;
        }
        return $response;
    }

    private function set_images() {
        $this->images = array();
        if (is_array($this->listingData["Item"]["PictureURL"])) {
            foreach ($this->listingData["Item"]["PictureURL"] AS $image)
                $this->images[] = $image;
        } else
            $this->images[] = $this->listingData["Item"]["PictureURL"];
    }

    private function set_specs() {
        $this->specifics = array();
        if (is_array($this->listingData)) {
            foreach ($this->listingData["Item"]["ItemSpecifics"]["NameValueList"] AS $spec) {
                $ignore = array("Return postage will be paid by",
                    "After receiving the item, your buyer should cancel");
                if (isset($spec["Name"])) {
                    if ($spec["Name"] == "Returns Accepted")
                        $this->returns_accepted = $spec["Value"] = "ReturnsNotAccepted" ? 0 : 1;
                    else if ($spec["Name"] == 'Brand')
                        $this->brand = $spec["Value"];
                    else if (!in_array($spec["Name"], $ignore))
                        $this->specifics[$spec["Name"]] = $spec["Value"];
                }
            }
        }
    }

    private function set_vars() {
        if (isset($this->listingData["Item"]["Variations"])) {
            $this->variations = array();

            // if there is only one variation
            if (isset($this->listingData["Item"]["Variations"]["Variation"]["StartPrice"])) {
                $variation = $this->listingData["Item"]["Variations"]["Variation"];
                $this->are_variations = false;
                $var["price"] = $variation["StartPrice"];
                $var["quantity"] = $variation["Quantity"];
                $var["sold"] = $variation["SellingStatus"]["QuantitySold"];
                if (isset($variation["SKU"]))
                    $var["SKU"] = $variation["SKU"];
                if (isset($variation["VariationSpecifics"]["NameValueList"][0]) && is_array($variation["VariationSpecifics"]["NameValueList"][0])) {
                    $specifics = $variation["VariationSpecifics"]["NameValueList"];
                    foreach ($specifics AS $spec)
                        $var["specifics"][$spec["Name"]] = $spec["Value"];
                } else
                    $var["specifics"][$variation["VariationSpecifics"]["NameValueList"]["Name"]] = $variation["VariationSpecifics"]["NameValueList"]["Value"];
                $this->variations[] = $var;
            } else {
                $this->are_variations = true;
                // multiple variations
                foreach ($this->listingData["Item"]["Variations"]["Variation"] AS $variation) {
                    $var["price"] = $variation["StartPrice"];
                    $var["quantity"] = $variation["Quantity"];
                    $var["sold"] = $variation["SellingStatus"]["QuantitySold"];
                    if (isset($variation["SKU"]))
                        $var["SKU"] = $variation["SKU"];
                    $specifics = $variation["VariationSpecifics"]["NameValueList"];
                    if (isset($specifics["Name"]))
                        $specifics = array($specifics);
                    foreach ($specifics AS $spec)
                        $var["specifics"][$spec["Name"]] = $spec["Value"];
                    $this->variations[] = $var;
                }
            }
        }
        else {
            // no variations - yes there can be 1 variation 'with variations' in ebay
            $this->are_variations = false;
            $this->single_item_price = $this->listingData["Item"]["CurrentPrice"];
            $this->single_item_quantity = $this->listingData["Item"]["Quantity"];
        }
    }

    private function set_return_policy() {
        $policy = $this->listingData['Item']['ReturnPolicy'];

        if (!empty($policy) && $policy['ReturnsAccepted'] == 'Returns Accepted') {
            $this->returns_accepted = 1;
            $this->return_days = $policy['ReturnsWithin'];
            $this->return_payer = $policy['ShippingCostPaidBy'];
        } else {
            $this->returns_accepted = 0;
            $this->return_days = $this->return_payer = NULL;
        }
    }

}
