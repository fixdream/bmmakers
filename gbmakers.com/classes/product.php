<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require("db.php");

class product extends db {

    public $cn, $db;
    public $id, $sku, $price, $title, $description, $group, $group_title, $group_description, $group_long_description, $category, $sub_category,
            $image1, $image2, $image3, $image4, $variations, $products, $data_layer, $variation_values;

    public function __construct($product = 0, $product_page = 0) {
        parent::__construct();

        if ($product != 0) {
            $this->load_product($product, $product_page);
        }
    }

    public function load_product($product, $product_page = 0) {
        if ($product_page == 0) {
            $sql = "SELECT * FROM `product_with_group` WHERE `id` = '" . $this->cleanInput($product) . "';";
            $result = mysqli_query($this->cn, $sql);
            $row = mysqli_fetch_all($result, MYSQLI_ASSOC);
            $product = $row[0];
            $this->id = $product['id'];
            $this->sku = $product['sku'];
            $this->price = $product['price'];
            $this->title = $product['title'];
            $this->description = $product['description'];
            $this->group = $product['group'];
            $this->group_title = $product['group_title'];
            $this->group_description = $product['group_description'];
            $this->group_long_description = $product['group_long_description'];
            $this->category = $product['category'];
            $this->sub_category = $product['sub_category'];

            $sql = "SELECT * FROM `product_images` WHERE `group` = '" . $this->group . "';"; //echo $sql;exit;
            $result = mysqli_query($this->cn, $sql);
            $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

            // images
            foreach ($rows AS $row) {
                switch ($row["position"]) {
                    case 1:
                        $this->image1 = $row["link"];
                        break;
                    case 2:
                        $this->image2 = $row["link"];
                        break;
                    case 3:
                        $this->image3 = $row["link"];
                        break;
                    case 4:
                        $this->image4 = $row["link"];
                        break;
                }
            }
        } else {// just product page requirements
            // Get details
            $sql = "SELECT * FROM `product_group` WHERE `id` = '" . $this->cleanInput($product) . "';";


            $result = mysqli_query($this->cn, $sql);
            $row = mysqli_fetch_all($result, MYSQLI_ASSOC);
            $product = $row[0];
            $this->id = $product['id'];
            $this->title = $product['title'];
            $this->group_description = $product['short_description'];
            $this->group_long_description = $product['long_description'];
            $this->category = $product['category'];
            $this->sub_category = $product['sub_category'];

            // Get images
            $sql = "SELECT * FROM `product_images` WHERE `group` = '" . $this->id . "';";
            $result = mysqli_query($this->cn, $sql);
            $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);

            // images
            foreach ($rows AS $row) {
                switch ($row["position"]) {
                    case 1:
                        $this->image1 = $row["link"];
                        break;
                    case 2:
                        $this->image2 = $row["link"];
                        break;
                    case 3:
                        $this->image3 = $row["link"];
                        break;
                    case 4:
                        $this->image4 = $row["link"];
                        break;
                }
            }

            if ($this->image2 == "") {
                $this->image2 = $this->image1;
            }
            if ($this->image3 == "") {
                $this->image3 = $this->image1;
            }
            if ($this->image4 == "") {
                $this->image4 = $this->image1;
            }

            // Get variations
            $sql = "SELECT * FROM `product_variations` WHERE `product_group` = '" . $this->id . "';";
            $result = mysqli_query($this->cn, $sql);
            $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
            // variations - just the variation names
            $this->variations = [];
            foreach ($rows AS $row) {
                if ((!in_array($row["var_name"], $this->variations)) and $row["var_name"] != "Ply Thickness") {
                    $this->variations[] = $row["var_name"];
                }
                if ((!in_array($row["var_name"], $this->variations)) and $row["var_name"] != "Ply Thickness") {
                    $this->variations[] = $row["var_name"];
                }
            }
            // variation values
            $this->variation_values = array();
            foreach($this->variations AS $var_name){
                $values = array();
                foreach($rows AS $row){
                    if($var_name == $row['var_name'])
                        $values[] = $row['var_value'];
                }
                $values = array_unique($values);
                $this->variation_values[$var_name] = $values;
            }

            // Get products
            $sql = "SELECT * FROM `products` WHERE `group` = '" . $this->id . "' ORDER BY `price` ASC;";
            $result = mysqli_query($this->cn, $sql);
            $this->products = mysqli_fetch_all($result, MYSQLI_ASSOC); //print_r($this->products);
        }
    }

    public function create_product_data_layer() {
        // Get variations
        $sql = "SELECT * FROM `product_variations` WHERE `product_group` = '" . $this->id . "';";
        $result = mysqli_query($this->cn, $sql);
        $rows = mysqli_fetch_all($result, MYSQLI_ASSOC);
        // variations 
        $variations = [];
        foreach ($rows AS $row)
                $variations[] = $row;
        $this->data_layer = '<script>var products =';
        $this->data_layer .= json_encode($variations);
        $this->data_layer .= ';</script>';
    }

    public function get_category_products($cat, $subcat = "") {
        if ($subcat == "") {
            $sql = "SELECT * FROM product_group pg left join `product_images` pi ON pg.id = pi.group
                WHERE pg.`category` = '" . mysqli_escape_string($this->cn, $cat) . "'
                AND `display` = 1  group by pg.id;";
        } else {
            $sql = "SELECT * FROM product_group pg left join `product_images` pi ON pg.id = pi.group
                WHERE pg.`category` = '" . mysqli_escape_string($this->cn, $cat) . "' AND pg.sub_category = '" . mysqli_escape_string($this->cn, $subcat) . "'
        AND `display` = 1  group by pg.id;";
        }
        $result = mysqli_query($this->cn, $sql);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function search_products($search) {

        $sql = "SELECT * FROM product_group pg left join `product_images` pi ON pg.id = pi.group
                WHERE pg.`title` LIKE '%" . mysqli_escape_string($this->cn, $search) . "%' AND `display` = 1 group by pg.id;";
        $result = mysqli_query($this->cn, $sql);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function get_product_list($list) {

        $sql = "SELECT * FROM product_group pg left join `product_images` pi ON pg.id = pi.group
                WHERE ";
        $or = "";
        foreach ($list AS $id) {
            $sql .= $or . "`pg`.`id` = " . $id;
            $or = " or ";
        }
        $sql .= " AND `display` = 1  group by pg.id;";
        $result = mysqli_query($this->cn, $sql);
        return mysqli_fetch_all($result, MYSQLI_ASSOC);
    }

    public function save_product() {
        // group
        $sql = "UPDATE  `product_group` SET  
                `title` =  '" . $this->group_title . "',
                `short_description` =  '" . $this->group_description . "',
                `long_description` =  '" . $this->group_long_description . "',
                `category` =  '" . $this->category . "',
                `sub_category` =  '" . $this->sub_category . "' 
                 WHERE `id` = " . $this->group . " LIMIT 1 ;";
        $result = mysqli_query($this->cn, $sql);

        // product
        $sql = "UPDATE  `products` SET  
                `sku` =  '" . $this->sku . "',
                `price` =  '" . $this->price . "',
                `title` =  '" . $this->title . "',
                `description` =  '" . $this->description . "' 
                WHERE  `id` = " . $this->id . " LIMIT 1;";
        $result = mysqli_query($this->cn, $sql);

        // image 1
        $sql = "UPDATE  `product_images` SET  
                `link` =  '" . $this->image1 . "'
                WHERE `sku` = '" . $this->sku . "' AND
                `position` = 1 
                LIMIT 1;
                ";
        $result = mysqli_query($this->cn, $sql);

        // image 2
        $sql = "UPDATE  `product_images` SET  
                `link` =  '" . $this->image2 . "'
                WHERE `sku` = '" . $this->sku . "' AND
                `position` = 2 
                LIMIT 1;
                ";
        $result = mysqli_query($this->cn, $sql);

        // image 3
        $sql = "UPDATE  `product_images` SET  
                `link` =  '" . $this->image3 . "'
                WHERE `sku` = '" . $this->sku . "' AND
                `position` = 3 
                LIMIT 1;
                ";
        $result = mysqli_query($this->cn, $sql);

        // image 4
        $sql = "UPDATE  `product_images` SET  
                `link` =  '" . $this->image4 . "'
                WHERE `sku` = '" . $this->sku . "' AND
                `position` = 4 
                LIMIT 1;
                ";
        $result = mysqli_query($this->cn, $sql);
    }

    public function create_product($sku, $group = 0) {
        $this->sku = $sku;

        if ($group == 0) {
            $sql = "INSERT INTO  `product_group` (
                    `id`
                    )
                    VALUES (
                    NULL
                    );";
            $result = mysqli_query($this->cn, $sql);
            $this->group = mysqli_insert_id($this->cn);
        } else {
            $this->group = $group;
        }
        $sql = "INSERT INTO  `products` (
                `id`,
                `sku`,
                `group`
                )
                VALUES (
                NULL,
                '" . $this->sku . "',
                '" . $this->group . "'
                );";
        echo $sql;
        $result = mysqli_query($this->cn, $sql);
        $this->id = mysqli_insert_id($this->cn);

        $count = 1;
        while ($count <= 4) {
            $sql = "INSERT INTO  `product_images` (
                `group` ,
                `sku` ,
                `link` ,
                `position`
                )
                VALUES (
                '" . $this->group . "',  '" . $this->sku . "',  '', " . $count . "
                );
                ";
            $result = mysqli_query($this->cn, $sql);
            $count++;
        }
    }

}
