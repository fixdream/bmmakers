<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once("../classes/apiCaller.php");
$apiCaller = new apiCaller;

if (isset($_POST['update_products'])) {
    $products = json_encode($apiCaller->get_products());
    $product_file = fopen("product_file.json", "w") or die("Unable to open file!");
    fwrite($product_file, $products);
    fclose($product_file);
}

if (isset($_POST['send_order'])) {
//sending orders 
//with prices put what you will pay me
    $products = array();
    
    $total = 0;
    if ($_POST['product1'] != "0") {
        $all = explode(".", $_POST['product1']);
        $products[] = array("product_id" => $all[0], "quantity" => $_POST['quantity1'], "title" => $all[1], "unit_price" => $all[2]);
        $total = $total + ($all[2] * $_POST['quantity1']);
    } else {
        echo "you didnt give any products";
    }

    if ($_POST['product2'] != "0") {
        $all = explode(".", $_POST['product2']);
        $products[] = array("product_id" => $all[0], "quantity" => $_POST['quantity2'], "title" => $all[1], "unit_price" => $all[2]);
        $total = $total + ($all[2] * $_POST['quantity1']);
    }    

    if ($_POST['product3'] != "0") {
        $all = explode(".", $_POST['product3']);
        $products[] = array("product_id" => $all[0], "quantity" => $_POST['quantity3'], "title" => $all[1], "unit_price" => $all[2]);
        $total = $total + ($all[2] * $_POST['quantity1']);
    }       
    
    if ($_POST['product4'] != "0") {
        $all = explode(".", $_POST['product4']);
        $products[] = array("product_id" => $all[0], "quantity" => $_POST['quantity4'], "title" => $all[1], "unit_price" => $all[2]);
        $total = $total + ($all[2] * $_POST['quantity1']);
    }      
 
    if ($_POST['product5'] != "0") {
        $all = explode(".", $_POST['product5']);
        $products[] = array("product_id" => $all[0], "quantity" => $_POST['quantity5'], "title" => $all[1], "unit_price" => $all[2]);
        $total = $total + ($all[2] * $_POST['quantity1']);
    }     
    
    $amount_paid = $total;
    $transaction_id = "_";
    $order_date = date("Y-m-d");
    $order_time = date("H:i:s");
    $address = str_replace("\n", "<br>", $_POST['address']);
    $postcode = $_POST['postcode'];
    $phone = "_";
    $email = "_";
    $name = $_POST['name'];
    $sales_id = "_";

echo "<h1>Your Order ID is:".$apiCaller->send_order($transaction_id, $amount_paid, $order_date, $order_time, $address, $postcode, $phone, $email, $name, $sales_id, $products)."</h1>"; // returns order number
}

if (isset($_POST['get_status'])) {
    // getting the status of an order 
    $params = array("id" => $_POST['get_status']);
    print_r($apiCaller->get_order_status($params));
}
?>
<h1>Get The Status of an order</h1>
<form action="" method="POST">
    <input type="text" name="get_status"/>
    <input type="submit" value="Get Status"/>
</form>

<br>

<h1>Update Product List</h1>
<form action="" method="POST">
    <input type="hidden" name="update_products" value="1"/>
    <input type="submit" value="Update"/>
</form>

<br>

<?php
$products_list = json_decode(file_get_contents("product_file.json"), true);
?>


<h1>Place Order</h1>
<form action="" method="POST">

    <label>Name: </label>
    <input type="input" name="name"/><br>

    <label>Postcode: </label>
    <input type="input" name="postcode"/><br>

    <label>Address: (use new lines for each line)</label><br>
    <textarea type="input" name="address" style="width:400px;height:100px;"></textarea><br>    


    <select name="product1">
        <option value="0">Select Product 1</option>
        <?php
        foreach ($products_list AS $product) {
            echo "<option value='" . $product['sku'] . "." . $product['title'] . "." . $product['price'] . "'>SKU: " . $product['sku'] . " " . $product['title'] . "</option>";
        }
        ?>    
    </select>
    <label>product 1 Quantity: </label> 
    <input type="text" name="quantity1"><br>





    <select name="product2">
        <option value="0">Select Product 2</option>
        <?php
        foreach ($products_list AS $product) {
            echo "<option value='" . $product['sku'] . "." . $product['title'] . "." . $product['price'] . "'>SKU: " . $product['sku'] . " " . $product['title'] . "</option>";
        }
        ?>    
    </select>
    <label>product 2 Quantity: </label> 
    <input type="text" name="quantity2"><br>





    <select name="product3">
        <option value="0">Select Product 3</option>
        <?php
        foreach ($products_list AS $product) {
            echo "<option value='" . $product['sku'] . "." . $product['title'] . "." . $product['price'] . "'>SKU: " . $product['sku'] . " " . $product['title'] . "</option>";
        }
        ?>    
    </select>
    <label>product 3 Quantity: </label> 
    <input type="text" name="quantity3"><br>




    <select name="product4">
        <option value="0">Select Product 4</option>
        <?php
        foreach ($products_list AS $product) {
            echo "<option value='" . $product['sku'] . "." . $product['title'] . "." . $product['price'] . "'>SKU: " . $product['sku'] . " " . $product['title'] . "</option>";
        }
        ?>    
    </select>
    <label>product 4 Quantity: </label> 
    <input type="text" name="quantity4"><br>




    <select name="product5">
        <option value="0">Select Product 5</option>
        <?php
        foreach ($products_list AS $product) {
            echo "<option value='" . $product['sku'] . "." . $product['title'] . "." . $product['price'] . "'>SKU: " . $product['sku'] . " " . $product['title'] . "</option>";
        }
        ?>    
    </select>
    <label>product 5 Quantity: </label> 
    <input type="text" name="quantity5"><br>



    <input type="hidden" name="send_order" value="1"/>
    <input type="submit" value="Send Order"/>
</form>

<br>

<h1>Find Product</h1>
<?php
foreach ($products_list AS $product) {
    if(!(isset($product['full_ebay_price']))){
        $product['full_ebay_price'] = $product['price'];
    }
    echo "<img src='" . $product['image_url'] . "' width='100px'/><a href='" . $product['ViewItemURL'] . "'>SKU: " . $product['sku'] . " " . $product['title'] . 
            "</a> <b>Price: &pound;" . $product['price'] . " GBmakers Ebay Price: &pound;" . $product['full_ebay_price'] . " Difference &pound;" . number_format($product['full_ebay_price'] - $product['price'],2) . "</b><br>";
}
