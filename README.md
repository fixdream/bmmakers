# README #


### Birmingham Makers - Manufacturing Order Management ###

* This is the repository for the birmingam makers information system
* Version 1.0

### How do I get set up? ###

* The system is designed to be distributed and so you can take any of the 3 elements and put them anywhere
* The Three Elements Being:
1. Datalayer - The repository consisting of a MySQL db and PHP API as middleware for the rest of the application
2. Interface - PHP driven website interace for the user to interact with the system
3. Analysis - Python element used to analysise the data

To install the Interface or the datalayer you will need to run on an environment with at least PHP 5.3

To install the database the installation file is in /datalayer/install/db/db.sql MYSQL version 5.5 is required


* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact