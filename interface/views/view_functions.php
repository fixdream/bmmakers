<?php

ini_set('max_execution_time', 300);

class view_functions {

    public $graph_url;

    public function __construct() {
        require '../settings.php';
        $this->graph_url = $analysis_image_hosting;
    }

    public function get_addresses_from_data($data) {
        if (is_array($data)) {
            $out = $last = "<br><br><div class='row'>";
            // this loop loops through all of the orders grouping them up with each orders
            foreach ($data AS $product) {
                if ($last != $product['id']) {
                    $out .= '
                        <div class="col-md-4">
                            <div class="thumbnail">
                                ' . $product['address'] . '<br><br>
                            </div>
                        </div>';
                }
            }
            $last = $product['id'];
            $out .= "</div>";
        }
        return $out;
    }

    public function get_orders_from_data($data, $undispatch = 0, $dispatch = 1) {
        include("../settings.php");
        if (is_array($data)) {
            $address = "<br>";
            $out = "";
            $last = $total = $count = $order_count = 0;
            // this loop loops through all of the orders grouping them up with each orders
            foreach ($data AS $product) {
                if ($last != $product['id']) {
                    $out .= $address . "<div class='jumbotron'>";
                    $out .= "<h2>Amount Paid: &pound;" . number_format($product["amount_paid"], 2) . "</h2>";
                    $out .= '
                        <div class="float-right">
                            <form action="address_label.php" method="POST">
                                <input type="hidden" name="address" value="' . $product["address"] . '">
                                <input type="hidden" name="sale" value="' . $product["id"] . '">
                                <button type="submit" class="btn btn-primary">Print Postage Label</button>
                            </form><br>';
                    if ($dispatch) {
                        $out .= '
                            <form action="" method="POST">
                                <input type="hidden" name="dispatch" value="' . $product['id'] . '">
                                <button type="submit" class="btn btn-default">Mark As Dispatched</button>
                            </form><br>
                    ';
                    }
                    if ($undispatch) {
                        $out .= '
                            <form action="" method="POST">
                                <input type="hidden" name="undispatch" value="' . $product['id'] . '">
                                <button type="submit" class="btn btn-default">Mark As Undispatched</button>
                            </form><br>
                    ';
                    }
                    $address_lines = explode("<br>", $product["address"]);
                    $out .= '
                            <div class="panel panel-default">
                            <img src="myhermes.jpg" alt="myhermes logo">
                            <form action="" method="POST">
                                <input type="hidden" name="myhermes" value="1"><br>
                                <input type="hidden" name="Address_line_1" value="';
                    if (isset($address_lines[1])) {
                        $out .= $address_lines[1];
                    } $out .= '">
                                <input type="hidden" name="Address_line_2" value="';
                    if (isset($address_lines[2])) {
                        $out .= $address_lines[2];
                    } $out .= '">
                                <input type="hidden" name="Address_line_3" value="';
                    if (isset($address_lines[3])) {
                        $out .= $address_lines[3];
                    } $out .= '">
                                <input type="hidden" name="Address_line_4" value="';
                    if (isset($address_lines[4])) {
                        $out .= $address_lines[4];
                    } $out .= '">
                                <input type="hidden" name="Postcode" value="' . $product['postcode'] . '">
                                <input type="hidden" name="First_Name" value="' . $product['name'] . '">
                                <label for="weight_' . $product['id'] . '">Weight (kg)</weight><br>
                                <input type="text" class=""form-control name="weight" value="0.9" id="weight_' . $product['id'] . '"><br><br>
                                <button type="submit" class="btn btn-primary">Add To MyHermes CSV File</button>
                            </form></div><br>
                        ';
                    $edit_link = "<a href='sale_edit.php?id=" . $product['id'] . "'>Edit</a></b>";
                    $out .= '<p><strong>Order Number: ' . $product['id'] . '</strong></p>' . $edit_link . '</div><br>';


                    $out .= "Time: <b>" . $product["date"] . " " . $product["time"] . "</b> Shop: <b>" . $product["sales_platform"] . " (" . $product["shop_id"] . ")</b><br>";
                    $total = $total + $product["amount_paid"];
                }
                $notes = "";
                if ($product["notes"] != "") {
                    $notes = "<br><br><h4>Notes:</h4><p style='color:red;'>" . $product["notes"] . "</p>";
                }
                $address = "<br>Address:<br>" . stripcslashes($product["address"]) . $notes . "</div>";
                $out .= "<h5>" . $product['title'] . "</h5>";
                if ($product['image_url'] != "" && file_get_contents($product['image_url'])) {
                    $image = $product['image_url'];
                    $suffix = explode(".",$image);//echo "../../datalayer/product_images/".$product["group"].".".end($suffix);exit;
                    if($api == "dev" && file_get_contents("http://localhost/bmmakers/datalayer/product_images/".$product["group"].".".end($suffix))){
                        $image = "http://localhost/bmmakers/datalayer/product_images/".$product["group"].".".end($suffix);
                    }
                } else {
                    $image = 'https://i.ebayimg.com/images/g/14wAAOSwLsBZW~q~/s-l300.jpg';
                }
                $out .= "<a href='http://www.ebay.co.uk/itm/" . $product['other_identifier'] . "'><img src='" . $image . "' width='100' height='100' /></a>";
                $out .= "<h5>Quantity " . $product['quantity'] . "</h5>";
                $count = $count + $product['quantity'];
                $out .= "<a href='http://www.ebay.co.uk/itm/" . $product['other_identifier'] . "'><b>" . $product['other_identifier'] . "</b></a>";
                if ($last != $product['id']) {
                    $order_count++;
                }
                $last = $product['id'];
            }
            $out .= $address . "</div></div></div></div>";
        }
        $return["orders"] = $out;
        $return["count"] = "<h5>" . $order_count . " Orders (" . $count . " Products) </h5>";
        $return["total"] = "<br><h5>Total &pound;" . $total . "</h5>";
        return $return;
    }

    public function edit_sale($data) {
        if (is_array($data)) {
            $address = "<br>";
            $out = "<div style='display:block;'><br>";
            $out .= '<div style="float:left;">
                    <form action="" method="POST">
                        <input type="hidden" name="undispatch" value="' . $_GET['id'] . '">
                        <button type="submit" class="btn btn-primary" >Mark As Undispatched</button>
                    </form></div> ';
            $out .= '<div style="float:left;margin-left:10px;">
                    <form action="" method="POST">
                        <input type="hidden" name="dispatch" value="' . $_GET['id'] . '">
                        <button type="submit" class="btn btn-default">Mark As Dispatched</button>
                    </form></div> ';
            $out .= '<div style="float:left;margin-left:10px;">
                    <form action="" method="POST">
                        <input type="hidden" name="ebay_dispatch" value="' . $_GET['id'] . '">
                        <button type="submit" class="btn btn-warning" >Mark As eBay Dispatched</button>
                    </form></div>';
            $out .= "</div><br>";
            $out .= "<form action='' method='POST'>";
            $last = $total = $count = $order_count = 0;
            // this loop loops through all of the orders grouping them up with each orders
            foreach ($data AS $product) {
                if ($last != $product['id']) {
                    $out .= $address . "<div class='jumbotron'><div class='float-right'>";

                    $out .= '<p><strong>Order Number: ' . $product['id'] . '</strong></p><br>';
                    $out .= '<p><strong>Dispatched: ' . $product['dispatched'] . '</strong></p><br>';
                    $out .= '<p><strong>eBay Dispatched: ' . $product['ebay_dispatched'] . '</strong></p></div><br>';
                    $out .= "<h2>Amount Paid: &pound;" . number_format($product["amount_paid"], 2) . "</h2>";



                    $out .= "Time: <b>" . $product["date"] . " " . $product["time"] . "</b> Shop: <b>" . $product["sales_platform"] . " (" . $product["shop_id"] . ")</b><br>";
                    $total = $total + $product["amount_paid"];
                }
                $notes = "<h4>Notes:</h4><p><textarea name='notes' class='form-control'>" . $product["notes"] . "</textarea></p>";
                $address = "<br>Address:<br><textarea name='address' class='form-control'>" . stripcslashes($product["address"]) . "</textarea>" . $notes . "<input type='submit' class='form-control' value='change'/></div>";
                $out .= "<h5>" . $product['title'] . "</h5>";
                if ($product['image_url'] != "" && file_get_contents($product['image_url'])) {
                    $image = $product['image_url'];
                } else {
                    $image = 'https://i.ebayimg.com/images/g/14wAAOSwLsBZW~q~/s-l300.jpg';
                }
                $out .= "<a href='http://www.ebay.co.uk/itm/" . $product['other_identifier'] . "'><img src='" . $image . "' width='100' height='100' /></a>";
                $out .= "<h5>Quantity " . $product['quantity'] . "</h5>";
                $count = $count + $product['quantity'];
                $out .= "<a href='http://www.ebay.co.uk/itm/" . $product['other_identifier'] . "'><b>" . $product['other_identifier'] . "</b></a>";
                if ($last != $product['id']) {
                    $order_count++;
                }
                $last = $product['id'];
            }
            $out .= $address . "</form></div></div></div></div>";
        }
        $return["orders"] = $out;
        return $return;
    }

    public function part_to_product_view($data) {
        if (!is_array($data)) {
            return '<br><div class="alert alert-success" role="alert">No Products Without Parts!</div>';
        }
        $out = "
        <table class='table table-bordered table-responsive-sm'>
            <tr>
                <td>edit</td>
                <td>product_id</td>
                <td>product_title</td>
                <td>EAN</td>
                <td>SKU</td>
                <td>price</td>
                <td>quantity</td>
                <td>sold</td>
                <td>watchers</td>
                <td>group</td>
                <td>ebay_shop</td>
                <td>part_quantity</td>
                <td>part_id</td>
                <td>part_name</td>
                <td>material_thickness</td>
                <td>process</td>
                <td>material_id</td>
            </tr>
            ";
        foreach ($data AS $row) {
            $out .= "<tr>";
            $out .= "   
           <td><a href='add_part.php?product=" . $row['product_id'] . "'>edit</a></td>
           <td>" . $row['product_id'] . "</td>
            <td>" . $row['product_title'] . "</td>
            <td>" . $row['EAN'] . "</td>
            <td>" . $row['SKU'] . "</td>
            <td>" . $row['price'] . "</td>
            <td>" . $row['quantity'] . "</td>
            <td>" . $row['sold'] . "</td>
            <td>" . $row['watchers'] . "</td>       
            <td>" . $row['group'] . "</td>
            <td>" . $row['ebay_shop'] . "</td>
            <td>" . $row['part_quantity'] . "</td>
            <td>" . $row['part_id'] . "</td>
            <td>" . $row['part_name'] . "</td>
            <td>" . $row['material_thickness'] . "</td>
            <td>" . $row['process'] . "</td>
            <td>" . $row['material_id'] . "</td>";
            $out .= "</tr>";
        }
        $out .= "</table>";
        return $out;
    }

    function productToPartForm($product, $parts) {
        $output = "";
        $output .= '
    <h4>Create a Part</h4>
    <table class="table table-bordered table-responsive-sm">
        <tr>
            <td>Name</td>
            <td>Material</td>
            <td>Material Thickness</td>
            <td>Process</td>
            <td>Material ID</td>
            <td></td>
        </tr>
        <tr>
            <td><form action="" method="POST"><input name="name" type="text" /><input type="hidden" name="part_create" value="1"/></td>
            <td><input name="material" type="text" /></td>
            <td><input name="thickness" type="text" /></td>
            <td><input name="process" type="text" /></td>
            <td><input name="material_id" type="text" /></td>
            <td><input type="submit" value="Create Part"/></form></td>
        </tr>
    </table>        
    ';

        $output .= '
            <h4>Add a part to a product</h4>
            <table class="table table-bordered table-responsive-sm">
                <tr>
                    <th>Part</th>
                    <th>Quantity</th>
                    <th></th>
                </tr>
                <tr>
                    <td>
                        <form action="add_part.php?product=' . $product . '" method="POST">
                        <select name="existing_part">';
        foreach ($parts AS $row) {
            $output .= '<option value="' . $row['id'] . '">' . $row['name'] . '</option>\n';
        }
        $output .= ' 
                        </select>
                    </td>
                    <td>
                        <input type="text" name="quantity"  id="quantity" value="1"/></td>
                        <input type="hidden" name="product" value="' . $product . '"/>
                    <td>
                        <input type="submit" value="Add Part to Product"/>
                        </form>
                    </td>
                </tr>
            </table>';

        return $output;
    }

    public function makeHeader($type, $content, $class = "") {
        return "<h" . $type . " class='" . $class . "'>" . $content . "</h" . $type . ">\n";
    }

    public function partsTable($parts) {
        $return = "<table class='table table-bordered table-responsive-sm'><tr><th>ID</th><th>Name</th></tr>";
        if (is_array($parts)) {
            foreach ($parts AS $part) {
                $return .= "<tr><td>" . $part['part_id'] . "</td><td>" . $part['name'] . "</td></tr>";
            }
        } else {
            $return .= "<tr><td></td><td>NONE</td></tr>";
        }
        $return .= "</table>";
        return $return;
    }

    public function kmeans_data($data, $k) {
        $return = "";
        $count = 1;
        if (is_array($data)) {
              while ($count <= $k) {
                echo "<h1>K ".$count." Members</h1>";
                foreach ($data AS $row) {
                    if($row['group'] == ($count - 1)){
                        //Array ( [part1] => 139 [part2] => 138 [group] => 0 [part1_name] => A5 Birch 9mm [part2_name] => A5 Birch 6mm ) 
                        echo "<p>".$row['part1_name']." <b>and</b> ".$row['part1_name']."</p>";
                    }
                }
                  $count++;
              }
        }
        return $return;
    }

    public function stockTable($stock) {
        $return = "<table class='table table-bordered table-responsive-sm'><tr><th>Part ID</th><th>Name</th><th>Quantity</th><th></th></tr>";
        foreach ($stock AS $part) {
            $return .= "<tr><td>" . $part['part_id'] . "</td><td>" . $part['name'] . "</td><td>
                <form action='' method='POST'>
                    <input type='hidden' value='" . $part["stock_id"] . "' name='stock_id' />
                    <input type='text' value='" . $part['quantity'] . "' name='quantity' class='form-control input-sm'/></td><td>
                    <input type='submit' class='form-control' value='Change Qunatity'>
                    </form>
                    </td></tr>";
        }
        $return .= "</table>";
        return $return;
    }

    public function predictionTable($predictions) {
        $return = "<table class='table table-bordered table-responsive-sm'><tr><th>Part ID</th><th>Name</th><th>Quantity Requied For The Next Month</th></tr>";
        foreach ($predictions AS $part) {
            $return .= "<tr><td>" . $part['part_id'] . "</td><td>" . $part['name'] . "</td>
                    <td>" . $part['quantity'] . "</td></tr>";
        }
        $return .= "</table>";
        return $return;
    }

    public function cutTable($predictions) {
        $return = "<table class='table table-bordered table-responsive-sm'><tr><th>Part ID</th><th>Name</th><th>Stock Quantity</th><th>Predicted Requirement</th>
                 <th>Quantity For Orders</th><th>Required Quanity</th>
                 </tr>";
        foreach ($predictions AS $part) {
            if ($part['required_quantity'] > 0) {
                $return .= "<tr><td>" . $part['part_number'] . "</td><td>" . $part['name'] . "</td>
                    <td><form action='' method='POST'>
                    <input type='hidden' value='" . $part["part_number"] . "' name='part_id' />
                    <input type='text' value='" . $part['stock'] . "' name='quantity' class='form-control input-sm'/>
                    <input type='submit' class='form-control' value='Update Stock'>
                    </form></td>
                    <td>" . $part['predicted_quantity'] . "</td>
                    <td>" . $part['quantity_required_for_orders'] . "</td>
                    <td>" . $part['required_quantity'] . "</td>
                    </tr>";
            }
        }
        $return .= "</table>";
        return $return;
    }

    public function cutTableNoPredict($requirements) {
        $return = "<table class='table table-bordered table-responsive-sm'><tr><th>Part ID</th><th>Name</th><th>Stock Quantity</th>
                 <th>Quantity For Orders</th><th>Required Quanity</th>
                 </tr>";
        foreach ($requirements AS $part) {
            if ($part['required_quantity'] > 0) {
                $return .= "<tr><td>" . $part['part_number'] . "</td><td>" . $part['name'] . "</td>
                    <td><form action='' method='POST'>
                    <input type='hidden' value='" . $part["part_number"] . "' name='part_id' />
                    <input type='text' value='" . $part['stock'] . "' name='quantity' class='form-control input-sm'/>
                    <input type='submit' class='form-control' value='Update Stock'>
                    </form></td>
                    <td>" . $part['quantity_required_for_orders'] . "</td>
                    <td>" . $part['required_quantity'] . "</td>
                    </tr>";
            }
        }
        $return .= "</table>";
        return $return;
    }
    
    public function get_graph($graph_name, $alt = "") {
        return "<div class='container'><div class='row'><div class='col-md-2 col-sm-0'></div><div class='col-md-8 col-sm-12'><img class='img-responsive col-xs-12' src='" . $this->graph_url . $graph_name . ".png?makeNew=" . time() . "' alt='" . $alt . "'/></div><div class='col-md-2 col-sm-0'></div></div>";
    }

    public function low_ebay_stock_product_view($products) {
        $out = "<table class='table table-bordered table-responsive-sm'>
                <tr>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Stock</th>
                    <th>Shop</th>
                </tr>";
        foreach ($products AS $product) {
            //print_r($product);
            $out .= "
                <tr>
                    <td><a href='" . $product['ViewItemURLForNaturalSearch'] . "' target='_blank'><img src='" . $product["image_url"] . "' width='75'/></a></td>
                    <td><a href='" . $product['ViewItemURLForNaturalSearch'] . "' target='_blank'>" . $product["title"] . "</a><br><b>Product ID: " . $product["id"] . " </b><br><b>SKU: " . $product["id"] . "</b></td>
                    <td><a href='" . $product['ViewItemURLForNaturalSearch'] . "' target='_blank'>" . $product["quantity"] . "</a></td>
                 <td>" . $product['ebay_shop'] . "</td>       
                </tr>";
            //break;
        }
        $out .= "</table>";
        return $out;
    }

    public function home_page($data) {

        $avg = $data["avg_sale_price"]["avg"];
        $avg_day = $data["avaerage_day"]["avg"];
        $avg_week = $data["avaerage_week"]["avg"];
        $avg_month = $data["avaerage_month"]["avg"];
        $to_dispatch_count = $data["to_dispatch"]["count"];
        $to_dispatch_total_amount = $data["to_dispatch"]["total_value"];
        $total_sales_count = $data["total_sales"]["sales_count"];
        $total_sales_total = $data["total_sales"]["sales_total"];

        $week_sales_total = $data["last_7_days"]["last_7_days_total_value"];
        $week_sales_count = $data["last_7_days"]["count"];

        $month_sales_total = $data["last_31_days"]["last_31_days_total_value"];
        $month_sales_count = $data["last_31_days"]["count"];

        $year_sales_total = $data["last_365_days"]["last_365_days_total_value"];
        $year_sales_count = $data["last_365_days"]["count"];

        $today_total = 0;
        $today_count = 0;
        if (gmdate("Y-m-d") == $data["daily_sales"][0]["date"]) {
            $today_total = $data["daily_sales"][0]["day_total"];
            $today_count = $data["daily_sales"][0]["orders"];
        }

        //print_r($data["daily_sales"][0]);

        $out = "";
        $out .= "<div style='text-align:center'>";
        $out .= "<br><h4>Birmingham Makers Limited <br>Manufacturing and Sales Manager</h4>";
        $out .= "</div>";

        $out .= "<h5>Current Orders</h5>";
        $out .= "<p>" . $to_dispatch_count . " Orders to dispatch total value:  &pound;" . $to_dispatch_total_amount . "</p>";

        $out .= "<h5>Today</h5>";
        $out .= "<p>" . $today_count . " Sales today totaling: &pound;" . $today_total . "</p>";

        $out .= "<h5>Last 7 Days</h5>";
        $out .= "<p>" . $week_sales_count . " Sales in the last 7 days totaling: &pound;" . $week_sales_total . "</p>";
        $out .= "
            <table class='table table-responsive-sm'>
                <tr>
                    <th></th>
                    <th>" . $data["daily_sales"][0]["date"] . "</th>
                    <th>" . $data["daily_sales"][1]["date"] . "</th>
                    <th>" . $data["daily_sales"][2]["date"] . "</th>
                    <th>" . $data["daily_sales"][3]["date"] . "</th>
                    <th>" . $data["daily_sales"][4]["date"] . "</th>
                    <th>" . $data["daily_sales"][5]["date"] . "</th>
                    <th>" . $data["daily_sales"][6]["date"] . "</th>
                </tr>
                <tr>
                    <th>Day Total</th>
                    <td>&pound;" . $data["daily_sales"][0]["day_total"] . "</td>
                    <td>&pound;" . $data["daily_sales"][1]["day_total"] . "</td>
                    <td>&pound;" . $data["daily_sales"][2]["day_total"] . "</td>
                    <td>&pound;" . $data["daily_sales"][3]["day_total"] . "</td>
                    <td>&pound;" . $data["daily_sales"][4]["day_total"] . "</td>
                    <td>&pound;" . $data["daily_sales"][5]["day_total"] . "</td>
                    <td>&pound;" . $data["daily_sales"][6]["day_total"] . "</td>
                </tr>
                <tr>
                    <th>Sale Count</th>
                    <td>" . $data["daily_sales"][0]["orders"] . "</td>
                    <td>" . $data["daily_sales"][1]["orders"] . "</td>
                    <td>" . $data["daily_sales"][2]["orders"] . "</td>
                    <td>" . $data["daily_sales"][3]["orders"] . "</td>
                    <td>" . $data["daily_sales"][4]["orders"] . "</td>
                    <td>" . $data["daily_sales"][5]["orders"] . "</td>
                    <td>" . $data["daily_sales"][6]["orders"] . "</td>
                </tr>
                <tr>
                    <th>Average Value</th>
                    <td>&pound;" . number_format($data["daily_sales"][0]["day_total"] / $data["daily_sales"][0]["orders"], 2) . "</td>
                    <td>&pound;" . number_format($data["daily_sales"][1]["day_total"] / $data["daily_sales"][1]["orders"], 2) . "</td>
                    <td>&pound;" . number_format($data["daily_sales"][2]["day_total"] / $data["daily_sales"][2]["orders"], 2) . "</td>
                    <td>&pound;" . number_format($data["daily_sales"][3]["day_total"] / $data["daily_sales"][3]["orders"], 2) . "</td>
                    <td>&pound;" . number_format($data["daily_sales"][4]["day_total"] / $data["daily_sales"][4]["orders"], 2) . "</td>
                    <td>&pound;" . number_format($data["daily_sales"][5]["day_total"] / $data["daily_sales"][5]["orders"], 2) . "</td>
                    <td>&pound;" . number_format($data["daily_sales"][6]["day_total"] / $data["daily_sales"][6]["orders"], 2) . "</td>
                </tr>
            </table>
            ";

        $out .= "<h5>Last 31 Days</h5>";
        $out .= "<p>" . $month_sales_count . " Sales in the last 31 days totaling: &pound;" . $month_sales_total . "</p>";
        $out .= "<div class='container'><div class='row'><div class='col-md-2 col-sm-0'></div><div class='col-md-8 col-sm-12'><img class='img-responsive' src='" . $this->graph_url . "last_31_days_sales.png?makeNew=" . time() . "' /></div><div class='col-md-2 col-sm-0'></div></div>";

        $out .= "<h5>Last 365 Days</h5>";
        $out .= "<p>" . $year_sales_count . " Sales in the last 31 days totaling: &pound;" . $year_sales_total . "</p>";

        $out .= "<h5>Averages</h5>";
        $out .= "<p>Average Sale Price: &pound;" . $avg . "</p>";
        $out .= "<p>Average Day Sales: &pound;" . $avg_day . "</p>";
        $out .= "<p>Average Week Sales: &pound;" . $avg_week . "</p>";
        $out .= "<p>Average Month Sales: &pound;" . $avg_month . "</p>";

        $out .= "<h5>System Totals</h5>";
        $out .= "<p>" . $total_sales_count . " Sales recorded on this System: &pound;" . $total_sales_total . "<p>";

        return $out;
    }

}
