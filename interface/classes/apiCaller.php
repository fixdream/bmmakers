<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of apiCaller
 *
 * @author Paddy
 */
class apiCaller {

    private $key, $account, $url;

    public function __construct() {
        require("../settings.php");
        $this->key = $key;
        $this->account = $account;
        $this->salt = $salt;
        if ($api == "live") {
            $this->url = $URL['live'];
        } else {
            $this->url = $URL['dev'];
        }
    }

// parmams must be an array
    public function make_GET_request($type, $params = array()) {
        $request = $this->url . "request=";
        $params["time"] = time();
        $h = $this->key . $params["time"];
        $params["hash"] = sha1($params["time"] . $this->key . $this->salt);
        $params["account"] = $this->account;
        $params["type"] = $type;
        $request = $request . json_encode($params);
        $response = file_get_contents($request);
        $raw = $response;
        if (json_decode($response, true)) {
            $response = json_decode($response, true);
        } else {
            echo "The JSON was not valid, the error code was number: ".json_last_error()." message: ".json_last_error_msg();
            exit();
        }
        if ($response["error"] != "none") {
            echo "<h3>There was an error with the API response </h3><br><b>Error:</b><br><b><p>" . $response["error"] . "</p></b><p>Full response:</p>";
            print_r($raw);
            exit;
        }
        return $response['response'];
    }

// parmams must be an array
    public function make_request($type, $params = array()) {
        $params["time"] = time();
        $params["hash"] = sha1($params["time"] . $this->key . $this->salt);
        $params["account"] = $this->account;
        $params["type"] = $type;
        $request = array("request" => json_encode($params));
        $headers = array(
            'http' => array(
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'Connection' => 'keep-alive',
                'content' => http_build_query($request)
            )
        );
        $context = stream_context_create($headers);
        $raw = file_get_contents($this->url, false, $context);
        
        if (json_decode($raw, true)) {
            $response = json_decode($raw, true);
        } else {
            echo "The JSON was not valid, the error code was number: <b>".json_last_error()."</b> message: <b>".json_last_error_msg()."</b>";
            echo "<br>\nRaw Response:<br>\n ".$raw;
            exit();
        }        
        if ($response["error"] != "none") {
            echo "<h3>There was an error with the API response </h3><br><b>Error:</b><br><b><p>" . $response["error"] . "</p></b><p>Full response:</p>";
            print_r($raw);
            exit;
        }
        return $response['response'];
    }

}
