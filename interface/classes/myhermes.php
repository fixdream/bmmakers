<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class myhermes {

    public function append_csv($data) {
        $new_line = "\n".str_replace(",", "", $data['Address_line_1']).",";
        $new_line .= str_replace(",", "", $data['Address_line_2']).",";
        $new_line .= str_replace(",", "", $data['Address_line_3']).",";
        $new_line .= str_replace(",", "", $data['Address_line_4']).",";
        $new_line .= str_replace(",", "", $data['Postcode']).",";
        $new_line .= str_replace(",", "", strtoupper($data['First_Name'])).",";
        $new_line .= ",";//lastname
        $new_line .= ",";//email
        $new_line .= str_replace(",", "", $data['weight']).",";
        $new_line .= ",";//compensation
        $new_line .= "n,";//signiture
        $new_line .= ",";//reference
        $new_line .= "Wooden Goods,";//contents
        $new_line .= "25,";//value
        $new_line .= ",";//phone
        $new_line .= ",";//safe place
        $new_line .= ",\n";//instructions
        
        $csvfile = fopen("../csv/myhermes_file.csv", "a") or die("fopen not working! check the ini");
        fwrite($csvfile, $new_line);
        fclose($csvfile);
        return true;
        }

    public function write_new_csv() {
        $myhermes_template_csv_contents = "Address_line_1,Address_line_2,Address_line_3,Address_line_4,Postcode,First_name,Last_name,Email,Weight(Kg),Compensation(£),Signature(y/n),Reference,Contents,Parcel_value(£),Delivery_phone,Delivery_safe_place,Delivery_instructions\n";
        $csvfile = fopen("../csv/myhermes_file.csv", "w") or die("fopen not working! check the ini");
        fwrite($csvfile, $myhermes_template_csv_contents);
        fclose($csvfile);
        return true;
    }

}
