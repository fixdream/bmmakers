<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of security
 *
 * @author Paddy
 */
class security {
    private $account, $password;
    
    public function __construct($bypass = false) {
        if (!(isset($_SESSION['logged_in'])) and !($bypass)){
            header("Location: login.php");
            exit;
        }
        require_once("../settings.php");
        $this->account = $userName;
        $this->password = $password;
    }
    
    public function login($post){
        $clean = $this->cleanArray($post);
        if($clean['pass'] == $this->password && strtoupper($clean['user']) == $this->account){
            echo "in";
            $_SESSION["logged_in"] = 1;
            header("Location: ./home.php");
        }else{
            return false;
        }
    }    
    
    public function logout(){
        session_unset();
        session_destroy();
    }
    
    private function cleanArray($array){
        foreach($array AS $key => $val){
            $clean[$key] = filter_var(strip_tags($val), FILTER_SANITIZE_STRING);
        }
        return $clean;
    }
}
