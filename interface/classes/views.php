<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of views
 *
 * @author Paddy
 */
class views {

    public $title, $header, $footer;

    public function __construct($page_title) {
        $this->get_header($page_title);
        $this->get_footer();

        $this->title = $page_title;
    }

    public function get_header($title) {
        $header = file_get_contents("../views/header.html");
        $header = str_replace("[[title]]", $title, $header);
        $this->header = $header;
    }

    public function login_header() {
        $header = file_get_contents("../views/login_header.html");
        $header = str_replace("[[title]]", $this->title, $header);
        return $header;
    }

    public function get_footer() {
        $footer = file_get_contents("../views/footer.html");
        $this->footer = $footer;
    }

    public function get_element($element) {
        return file_get_contents("../views/" . $element . ".html");
    }

    public function alert($message, $type = "danger") {
        $out = file_get_contents("../views/alert.html");
        $out = str_replace("[[message]]", $message, $out);
        $out = str_replace("[[type]]", $type, $out);
        return $out;
    }

}
