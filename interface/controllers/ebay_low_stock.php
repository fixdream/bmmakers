<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("eBay Low Stock");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;
echo "<br><h5>Low eBay Stock List</h5>";



$data = $api->make_request("get_ebay_low_stock");
echo $vf->low_ebay_stock_product_view($data);

echo $view->footer;
