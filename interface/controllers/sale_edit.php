<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Edit Sale");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;
echo "<br><h5>Edit Sale</h5>";


if (isset($_POST['notes'])) {
    $params = array("id" => $_GET['id'], "notes" => $_POST['notes'], "address" => $_POST['address']);
    $data = $api->make_request("edit_sale", $params);
} 

if (isset($_POST['undispatch'])) {
    $params = array("sale" => $_POST['undispatch']);
    $data = $api->make_request("undispatch", $params);
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You marked order: ' . $_POST['undispatch'] . ' as undispathced!</strong>
    </div>';
}

if (isset($_POST['ebay_dispatch'])) {
    $params = array("sale" => $_POST['ebay_dispatch']);
    $data = $api->make_request("mark_order_as_ebay_dispatched", $params);
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You marked order: ' . $_POST['dispatch'] . ' as dispatched!</strong>
    </div>';
}

if (isset($_POST['dispatch'])) {
    $params = array("sale" => $_POST['dispatch']);
    $data = $api->make_request("mark_order_as_dispatched", $params);
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You marked order: ' . $_POST['dispatch'] . ' as dispatched!</strong>
    </div>';
}

$params = array("id" => $_GET['id']);
$data = $api->make_request("get_sale", $params);
if (is_array($data)) {
    $sale = $vf->edit_sale($data);
    echo $sale['orders'];
} else {
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>No Orders matching this ID</strong>
    </div>';
}


echo $view->footer;
