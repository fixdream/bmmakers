<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Cut List");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;

// update the quantity of a single part
if(isset($_POST['quantity'])){
    $params = array("part_id" => $_POST['part_id'], "quantity" => $_POST['quantity']);
    echo '<div class="alert alert-success" role="alert">
    <strong>'.$api->make_request("set_stock_with_part_id", $params).'</strong>
    </div>';
}


if (isset($_GET['machine']) && $_GET['machine'] == "honeycomb_laser") {
    //Honeycomb
    echo "<br><h3>Laser Cutter with Honeycomb Bed Cut List</h3>";
    $params = array("machine" => "honeycomb_laser");
    $cut_list = $api->make_request("get_cut_list_with_machine", $params);
    echo $vf->cutTable($cut_list);
} elseif (isset($_GET['machine']) && $_GET['machine'] == "table_saw") {
    //tables saw
    echo "<br><h3>Table Saw Cut List</h3>";
    $params = array("machine" => "table_saw");
    $cut_list = $api->make_request("get_cut_list_with_machine", $params);
    echo $vf->cutTable($cut_list);
} else {
//Blades
    echo "<br><h3>Laser Cutter with Blade Bed Cut List</h3>";
    $params = array("machine" => "bladed_laser");
    $cut_list = $api->make_request("get_cut_list_with_machine", $params);
    echo $vf->cutTable($cut_list);
}


echo $view->footer;
