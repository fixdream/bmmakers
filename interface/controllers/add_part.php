<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Add Part");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;

if (!isset($_GET["product"])) {
    echo "You have not given a product ID";
    exit;
}

// part creation 
if (isset($_POST["part_create"])) {
    $params = array("name" => $_POST["name"], "material" => $_POST["material"], "material_thickness" => $_POST["thickness"], "process" => $_POST["process"], "material_id" => $_POST["material_id"]);
    $data = $api->make_request("create_part", $params);
}

// product to part assign creation 
if (isset($_POST["existing_part"])) {
    //[existing_part] => 0 [quantity] => 2 [product] => 34633 
    $params = array("existing_part" => $_POST["existing_part"], "product" => $_POST["product"], "quantity" => $_POST["quantity"]);
    $data = $api->make_request("add_part_to_product", $params);
    echo "<br>" . $data;
    header("Location: /bmManager/interface/controllers/no_part_to_product.php");
}



// Page Title
$params = array("id" => $_GET["product"]);
$product = $api->make_request("get_product", $params);
$title = $product["title"];
echo $vf->makeHeader(3, "Add Part To Product:".$title);


// parts add forms
$parts = $api->make_request("get_parts");
$product = intval($_GET["product"]);
echo $vf->productToPartForm($product, $parts);

// Existing parts attached to the product
$params = array("id" => $_GET["product"]);
$parts_for_product = $api->make_request("get_parts_for_product", $params);
echo $vf->makeHeader(3, "Existing Parts added to product:");
echo $vf->partsTable($parts_for_product);


$product = $api->make_request("product_to_ebay_link", $params);
echo "<a href='https://www.ebay.co.uk/itm/".$product["ebay_id"]."'>eBay link to the product</a><br><br>";

echo file_get_contents("../views/material_id_table.html");

echo $view->footer;
