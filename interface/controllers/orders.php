<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Orders");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;
echo "<br><h5>Orders From The Last Week</h5>";

if (isset($_POST['dispatch'])) {
    $params = array("sale" => $_POST['dispatch']);
    $data = $api->make_request("mark_order_as_ebay_dispatched", $params);
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You marked order: ' . $_POST['dispatch'] . ' as dispatched!</strong>
    </div>';
}

if (isset($_POST['undispatch'])) {
    $params = array("sale" => $_POST['undispatch']);
    $data = $api->make_request("undispatch", $params);
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You marked order: ' . $_POST['undispatch'] . ' as undispathced!</strong>
    </div>';
}

if (isset($_POST['myhermes'])) {
    require_once("../classes/myhermes.php");
    $myhermes = new myhermes;
    if ($myhermes->append_csv($_POST)) {
        echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You an order to the MyHermes CSV!</strong>
    </div>';
    }
}


if (isset($_GET['range']) && $_GET['range'] == "2_months") {
    $data = $api->make_request("get_last_two_months_orders");
} elseif (isset($_GET['range']) && $_GET['range'] == "2_weeks") {
    $data = $api->make_request("get_last_two_weeks_orders");
} else {//get_the_last_weeks_orders
    $data = $api->make_request("get_the_last_weeks_orders");
}

if (is_array($data)) {
    $orders = $vf->get_orders_from_data($data, 1, 0);
    echo $orders["total"] . $orders["count"];
    echo $orders["orders"];
} else {
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>All Dispatched!</strong>
    </div>';
}

echo $view->footer;
