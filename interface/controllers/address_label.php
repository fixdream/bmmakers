<?php

require('../classes/fpdf/PDF_Label.php');
require_once("../classes/apiCaller.php");
$api = new apiCaller;

$params = array("sale" => $_POST['sale']);
$data = $api->make_request("mark_order_as_dispatched", $params);

$pdf = new PDF_Label('29mmBrother'); 
$pdf->AddPage("L");
$pdf->SetFont('Times'); 
$pdf->SetFontSize(10);
$address = str_replace("<br>", "\n", $_POST['address']);
$address = str_replace("\n\n", "\n", $address);
$label = $address;
$label_array = explode("\n",$label);
$count = count($label_array);
switch($count):
    case 5:
       $pdf->SetFontSize(16);
    break;
    case 6:
       $pdf->SetFontSize(14);
    break;
    case 7:
       $pdf->SetFontSize(12);
    break;
endswitch;

$pdf->Add_Label($label);

$pdf->Output();