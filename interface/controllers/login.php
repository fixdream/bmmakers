<?php
ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security(true);


require_once("../classes/views.php");
$view = new views("Login Page");

require("../settings.php");
require("../system_messages/".$lang.".php");


echo $view->login_header();

if(isset($_POST['form'])){
    if(!($sec->login($_POST))){
        
        echo $view->alert($message['login_fail'],"danger");
    }
}

echo $view->get_element("login_form");
echo $view->footer;

//print_r($api->make_request("average_sale_price"));
