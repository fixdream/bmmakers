<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Unsorted Cut List");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;

// update the quantity of a single part
if(isset($_POST['quantity'])){
    $params = array("part_id" => $_POST['part_id'], "quantity" => $_POST['quantity']);
    echo '<div class="alert alert-success" role="alert">
    <strong>'.$api->make_request("set_stock_with_part_id", $params).'</strong>
    </div>';
}

//get parts to cut
echo "<br><h5>Mixed Machine Cut List</h5>";
$cut_list = $api->make_request("get_cut_list_with_predictions");
echo $vf->cutTable($cut_list);


echo $view->footer;
