<?php
ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Sales");

echo $view->header;


$data = $api->make_request("get_sales");
print_r($data);
echo $view->footer;