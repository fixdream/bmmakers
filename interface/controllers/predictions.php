<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Predictions");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;



//get parts in stock
$predictions = $api->make_request("get_predictions");
echo "<br><h5>Predictions - Based on Last Month's Sales</h5>";
echo $vf->predictionTable($predictions);







echo $view->footer;
