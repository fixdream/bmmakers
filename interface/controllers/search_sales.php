<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Search Sales");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;
echo "<br><h5>Search Sales</h5>";

if (isset($_POST['myhermes'])) {
    require_once("../classes/myhermes.php");
    $myhermes = new myhermes;
    if ($myhermes->append_csv($_POST)) {
        echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You an order to the MyHermes CSV!</strong>
    </div>';
    }
}

$search = "";
if (isset($_POST['search'])) {
    $search = $_POST['search'];
}

echo "<br>
      <div>
        <form action='' method='POST' classs='form-control'>
            <label for='field'>Search Category</label>
            <select type='hidden' name='field' id='field' classs='form-control'/>
                <option value='address'>Address</option>
                <option value='id'>ID</option>
                <option value='name'>Name</option>
                <option value='date'>Date (2020-12-30)</option>
                <option value='transaction_id'>eBay Transaction ID</option>
                <option value='email'>Email</option>
                <option value='shop_id'>Shop ID</option>
            </select> 
            <label for='field'>Search</label> 
            <input type='text' name='search' id='search' classs='form-control' value='".$search."' />
            <button type='submit' class='btn btn-primary btn-block'>Search</button>
        </form>
      </div>";




if (isset($_POST['search'])) {
    echo '<br><h5>Search Results For: "'.$_POST['search'].'" In: ',$_POST['field'].'</h5>';
    $params = array("field" => $_POST['field'], "search" => $_POST['search']);
    $data = $api->make_request("sale_search",$params);
    if (is_array($data)) {
        $orders = $vf->get_orders_from_data($data);
        echo $orders["orders"];
    } else {
        echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>No Results!</strong>
    </div>';
    }
}






echo $view->footer;
