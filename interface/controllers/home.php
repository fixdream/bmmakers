<?php
ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Birmingham Makers Limited");

echo $view->header;

require_once("../views/view_functions.php");
$vf = new view_functions;

$data = $api->make_request("home_page_data");
echo $vf->home_page($data);



echo $view->footer;