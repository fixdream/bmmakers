<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("To Dispatch List");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;
echo "<br><h5>Full 'To Dispatch' List</h5>";

if (isset($_POST['dispatch'])) {
    $params = array("sale" => $_POST['dispatch']);
    $data = $api->make_request("mark_order_as_dispatched", $params);
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You marked order: ' . $_POST['dispatch'] . ' as dispatched!</strong>
    </div>';
}

if (isset($_POST['myhermes'])) {
    require_once("../classes/myhermes.php");
    $myhermes = new myhermes;
    if ($myhermes->append_csv($_POST)) {
        echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You an order to the MyHermes CSV!</strong>
    </div>';
    }
}

if (isset($_POST['myhermes_wipe'])) {
    require_once("../classes/myhermes.php");
    $myhermes = new myhermes;
    if ($myhermes->write_new_csv()) {
        echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You wiped the MyHermes CSV clean</strong>
    </div>';
    }
}

echo "<br>
      <div>
        <form action='' method='POST' >
            <input type='hidden' name='myhermes_wipe' value='1'/>
            <button type='submit' class='btn btn-danger btn-block'>Clear MyHermes File</button>
        </form>
      </div>";

$data = $api->make_request("get_to_dispatch_with_items");
if (is_array($data)) {
    $orders = $vf->get_orders_from_data($data);
    echo $orders["total"] . $orders["count"];
    echo $orders["orders"];
} else {
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>All Dispatched!</strong>
    </div>';
}


echo $view->footer;
