<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Daily Sales");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;

$data = $api->make_request("get_daily_sales");
foreach($data AS $row){
    print_r($row);echo "<br>";
}
echo $view->footer;
