<?php
ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security(true);
$sec->logout();

require_once("../classes/views.php");
$view = new views("Logout Page");

echo $view->login_header();
echo $view->get_element("logoutMessage");
echo $view->footer;


//print_r($api->make_request("average_sale_price"));
