<?php
ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Daily Sales Graph");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;

echo $view->get_element("graph_form");

if (isset($_GET['month'])) {
    $both = explode(".",$_GET['month']);
    $month = $both[1];
    $year = $_GET['year'];
    $month_word = $both[0];;

    echo "<br><h5>Daily Sales Chart for " . $month_word . " " . $year . "</h5>";
    $graph_name = $year . "_" . $month . "_Daily_Sales";
    echo "<div class='container'>" . $vf->get_graph($graph_name) . "</div>";

    echo "<h5>Daily Sales Volume Chart for " . $month_word . " " . $year . "</h5>";
    $graph_name2 = $graph_name . "_Volume";
    echo "<div class='container'>" . $vf->get_graph($graph_name2) . "</div>";


    echo "<h5>Daily Sales Regression Chart for " . $month_word . " " . $year . "</h5>";
    $graph_name3 = $graph_name . "_Regression";
    echo "<div class='container'>" . $vf->get_graph($graph_name3) . "</div>";
}

echo $view->footer;
