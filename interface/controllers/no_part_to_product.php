<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("No Parts to Products");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;



$data = $api->make_request("get_product_to_part_data_where_none");
echo "<br><h5>Products With No Associated Parts</h5>";

echo $vf->part_to_product_view($data);

echo $view->footer;
