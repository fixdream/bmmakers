<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Monthly Sales Graph");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;

$start_year = 2017;

echo "<br><h5>K-Means Multi Product/Part Order Analysis</h5>";
$graph_name = "kmean_part_sales";
echo "<div class='container'>" . $vf->get_graph($graph_name) . "</div>";

//get_kmeans_multipart_groups
$data = $api->make_request("get_kmeans_multipart_groups");
echo $vf->kmeans_data($data, 2);

