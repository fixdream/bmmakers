<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Stock");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;
echo "<br><h5>Stock List</h5>";

echo $view->get_element("stock_zero_form");

// update the stock with all new parts
if (isset($_POST['update'])) {
    echo $data = $api->make_request("make_stock_record_for_all_parts");
} else {
    echo $view->get_element("stock_parts_update");
}


// update the quantity of a single part
if (isset($_POST['quantity'])) {
    $params = array("stock_id" => $_POST['stock_id'], "quantity" => $_POST['quantity']);
    echo '<div class="alert alert-success" role="alert">
    <strong>' . $api->make_request("set_stock", $params) . '</strong>
    </div>';
}

// zero stock
if (isset($_POST['zero'])) {
    $api->make_request("zero_stock");
    echo '<div class="alert alert-success" role="alert">
    <strong>You Have Zero\'d The Stock</strong>
    </div>';
}

//get parts in stock
$stock = $api->make_request("get_stock_levels");
echo $vf->stockTable($stock);







echo $view->footer;
