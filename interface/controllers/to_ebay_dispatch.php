<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("eBay To Dispatch");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;
echo "<br><h5>To eBay Dispatch List - Shop ".$_GET['shop']."</h5>";

if (isset($_POST['dispatch'])) {
    $params = array("sale" => $_POST['dispatch']);
    $data = $api->make_request("mark_order_as_ebay_dispatched", $params);
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You marked order: ' . $_POST['dispatch'] . ' as dispatched!</strong>
    </div>';
}

if (isset($_POST['undispatch'])) {
    $params = array("sale" => $_POST['undispatch']);
    $data = $api->make_request("undispatch", $params);
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You marked order: ' . $_POST['undispatch'] . ' as undispathced!</strong>
    </div>';
}

if (isset($_POST['myhermes'])) {
    require_once("../classes/myhermes.php");
    $myhermes = new myhermes;
    if ($myhermes->append_csv($_POST)) {
        echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You an order to the MyHermes CSV!</strong>
    </div>';
    }
}

if (isset($_POST['clear_ebay_dispatched'])) {
    if (isset($_POST['shop'])) {
        $params = array("shop" => $_POST['shop']);
        $data = $api->make_request("clear_ebay_dispatched", $params);
    } else {
        $data = $api->make_request("clear_ebay_dispatched");
    }

    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>You wiped the eBay Dispatch List</strong>
    </div>';
}

echo "<br>
      <div>
        <form action='' method='POST' >
            <input type='hidden' name='clear_ebay_dispatched' value='1'/>
            <button type='submit' class='btn btn-danger btn-block'>Clear eBay dispatch list</button>
        </form>
      </div>";

if (isset($_GET['shop'])) {
    $params = array("shop" => $_GET['shop']);
    $data = $api->make_request("get_to_ebay_dispatch_with_items", $params);
} else {
    $data = $api->make_request("get_to_ebay_dispatch_with_items");
}


if (is_array($data)) {
    $orders = $vf->get_orders_from_data($data, 1);
    echo $orders["total"] . $orders["count"];
    echo $orders["orders"];
} else {
    echo '
    <br>
    <div class="alert alert-success" role="alert">
    <strong>All Dispatched!</strong>
    </div>';
}

echo $view->footer;
