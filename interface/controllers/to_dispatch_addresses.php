<?php

/*  � 2013 eBay Inc., All Rights Reserved */
/* Licensed under CDDL 1.0 -  http://opensource.org/licenses/cddl1.php */
//$response = simplexml_import_dom($responseDoc);


require('../classes/fpdf/PDF_Label.php');
//L7163
//3422
//J8165  ~~~
$pdf = new PDF_Label('J8165');

$pdf->AddPage();

require_once("../classes/apiCaller.php");
$api = new apiCaller;
$data = $api->make_request("get_to_dispatch_with_items");

if (is_array($data)) {
    $last = "";
    $count = 1;
    // this loop loops through all of the orders grouping them up with each orders
    foreach ($data AS $product) {
        if($last != $product['id'] and $count != 1){
            $pdf->Add_Label($label);
        }
        if ($last != $product['id']) {
            $label = str_replace("<p>", "", $product['address']."\n");
            $label = str_replace("</p>", "", $label);
            $label = str_replace("<br>", "\n", $label);
            $label = str_replace("\n\n", "\n", $label);
            $label = strtoupper($label);
        }
        $label .= "\n".$product['title']."\nQuantity".$product['quantity'];  
        $count++;
        $last = $product['id'];
    }
    $pdf->Add_Label($label);
}

//    
//    
//    // site 2 
//    if (isset($orders[2])) {
//        foreach ($orders[2] as $order) {
//            $address = str_replace("<p>", "", $order['address']);
//            $address = str_replace("</p>", "", $address);
//            $address = str_replace("<br>", "\n", $address);
//            $address = strtoupper($address);
//            if ($order['shipping_service'] == "InternationalPriorityShippingUK") {
//                $address = explode("\n",$address);
//                $name = $address[0]."\n\n";
//                $address = $name." #### Internation Shipping Programme (ISP) #### ";
//            }
//            if ($labelFormat == 'J8165' or true) {
//                $address .= "\n\n";
//                foreach ($order['transactions']->Transaction as $transaction) {
//
//                $address .= substr($transaction->Item->Title, 0, 170);
//                $address .= " Quantity " . $transaction->QuantityPurchased."\n";
//                }
//            }
//            $pdf->Add_Label($address);
//        }//end loop
//    }


$pdf->Output();
