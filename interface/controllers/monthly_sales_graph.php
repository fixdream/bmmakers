<?php

ob_start();
session_start();

require_once("../classes/security.php");
$sec = new security;

require_once("../classes/apiCaller.php");
$api = new apiCaller;

require_once("../classes/views.php");
$view = new views("Monthly Sales Graph");

require_once("../views/view_functions.php");
$vf = new view_functions;

echo $view->header;

$start_year = 2017;
for ($count = 8; $count >= 0; $count--) {
    $year = $start_year + $count;
    $current_year = date("Y");
    if($year <= $current_year) {
        echo "<br><h5>Monthly Sales Chart for " . $year . "</h5>";
        $graph_name = $year . "_Monthly_Sales";
        echo "<div class='container'>" . $vf->get_graph($graph_name) . "</div>";
        $graph_name = $year . "_Monthly_Sales_Regression";
        echo "<div class='container'>" . $vf->get_graph($graph_name) . "</div>";
        $graph_name = $year . "_Monthly_Sales_Volume";
        echo "<div class='container'>" . $vf->get_graph($graph_name) . "</div>";
    }
}
